.include "man/entity.h.s"
.include "cfg.h.s"
.include "helper/sound.h.s"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Updates all entities with jump component
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; AF, BC, HL
;; ----------------------------------------------------
sys_jump_update::
    ld hl, #sys_jump_updateOneEntity
    xor a
    ld b, a
    set #COMPONENT_H_JUMP_BIT, b
    call man_entity_forallMatchingEntities

    ;; Player Jump code
    call man_entity_getPlayer
    res #COMPONENT_H_JUMP_BIT, ENT_COMPONENTS_H(ix)
	ld a, #JUMP_STATUS_QUIET+1
	cp ENT_JUMP_STATUS(ix)
	ret nz
		helper_soundm_playSFX_0 SFX_JUMP
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Performs the jump actions on one entity
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; AF, BC, HL
;; ----------------------------------------------------
sys_jump_updateOneEntity::
	ld a, ENT_JUMP_STATUS(ix)
	
	;; If jump button is pressed and ENT_JUMP_STATUS isn't JUMP_STATUS_MAX, increments
	;; ENT_JUMP_STATUS and sets ENT_VY to JUMP_VELOCITY
    cp #JUMP_STATUS_MAX
    ret z
		ld ENT_VY(ix), #JUMP_VELOCITY
		inc ENT_JUMP_STATUS(ix)
    ret

