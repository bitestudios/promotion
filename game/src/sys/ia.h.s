.include "lib/macros.h.s"

.globl _boss1_s0
.globl _boss2_s0
.globl _wandering_minion_0
.globl _crazy_minion_0
.globl _boomerang_ia_0
.globl _bird_s0
.globl _boss4_s0
.globl _boss3_s0
.globl _minion_counter
.globl _shooting_minion_0
.globl _bird_counter

.globl sys_ia_update

_no_state = 0x0000

OffsetInit
DefineOffset S_ACTION_L, 1
DefineOffset S_ACTION_H, 1
DefineOffset S_TRANSITION_L, 1
DefineOffset S_TRANSITION_H, 1
DefineOffset S_WAIT_CICLES, 1

NO_MINION = 0XFFFF


;; MACROS
.macro DefineState _stateName, _actionFunction, _transitionFunction, _wait
_stateName::    
    .dw _actionFunction
    .dw _transitionFunction
    .db #_wait
.endm 

.macro MoveToActionFunction __name, __position
__name::
	ld a, #__position
    ld ENT_IA_TARGET_X(ix), a

    call sys_ia_moveTo
    ret
.endm

.macro MoveToTransitionFunction __name, __default_next_state, __other_transitions_function
__name::
    ld hl, #__other_transitions_function
    ld bc, #__default_next_state
    call _move_to_transition
    ret
.endm

.macro MoveToState __state_name, __position, __default_next_state, __other_transitions_function, __wait_cicles
    MoveToActionFunction __state_name'_move, __position
    MoveToTransitionFunction __state_name'_transition, __default_next_state, __other_transitions_function
    DefineState __state_name, __state_name'_move, __state_name'_transition, __wait_cicles
.endm

.macro ShootTransition __name, __default_next_state, __other_transitions_function
__name::
    call __other_transitions_function  ;; In case there are other states to jump

    ;; Change state
    ld hl, #__default_next_state
    ld a, S_WAIT_CICLES(iy)
    call _transition_happens
    ret
.endm

.macro ShootState __state_name, __default_next_state, __other_transitions_function, __wait_cicles
; __state_name'_move::
; 	set COMPONENT_L_SHOOTING_BIT, ENT_COMPONENTS_L(ix)
;     ret
ShootTransition __state_name'_transition, __default_next_state, __other_transitions_function
DefineState __state_name, _shoot_general, __state_name'_transition, __wait_cicles
.endm

.macro ShootToPlayerState __state_name, __default_next_state, __other_transitions_function, __wait_cicles
; __state_name'_move::
; 	set COMPONENT_L_SHOOTING_BIT, ENT_COMPONENTS_L(ix)
;     ret
ShootTransition __state_name'_transition, __default_next_state, __other_transitions_function
DefineState __state_name, _shoot_to_player, __state_name'_transition, __wait_cicles
.endm

.macro ShootRightState __state_name, __default_next_state, __other_transitions_function, __wait_cicles
; __state_name'_move::
;     ld ENT_FACING_DIR(ix), #0
; 	set COMPONENT_L_SHOOTING_BIT, ENT_COMPONENTS_L(ix)
;     ret
ShootTransition __state_name'_transition, __default_next_state, __other_transitions_function
DefineState __state_name, _shoot_right, __state_name'_transition, __wait_cicles
.endm

.macro ShootLeftState __state_name, __default_next_state, __other_transitions_function, __wait_cicles
; __state_name'_move::
;     ld ENT_FACING_DIR(ix), #1
; 	set COMPONENT_L_SHOOTING_BIT, ENT_COMPONENTS_L(ix)
;     ret
ShootTransition __state_name'_transition, __default_next_state, __other_transitions_function
DefineState __state_name, _shoot_left, __state_name'_transition, __wait_cicles
.endm

; .macro WanderStateActionFunction __name
; __name::
;     call sys_ia_wander
;     ret
; .endm

.macro WanderTransition __name, __other_transitions_function
__name::
    call __other_transitions_function  ;; In case there are other states to jump
    ret
.endm

.macro WanderState __state_name, __other_transitions_function, __wait_cicles
    ; WanderStateActionFunction __state_name'_move
    WanderTransition __state_name'_transition, __other_transitions_function
    DefineState __state_name, sys_ia_wander, __state_name'_transition, __wait_cicles
.endm

; .macro MoveToPlayerActionFunction __name
; __name::
;     call _move_to_player
;     ret
; .endm

.macro MoveToPlayerState __state_name, __default_next_state, __other_transitions_function, __wait_cicles
    ; MoveToPlayerActionFunction __state_name'_move
    MoveToTransitionFunction __state_name'_transition, __default_next_state, __other_transitions_function
    DefineState __state_name, _move_to_player, __state_name'_transition, __wait_cicles
.endm

; .macro MoveToPlayerXYActionFunction __name
; __name::
;     call _move_to_playerXY
;     ret
; .endm

.macro MoveToPlayerXYState __state_name, __default_next_state, __other_transitions_function, __wait_cicles
    ; MoveToPlayerXYActionFunction __state_name'_move
    MoveToTransitionFunction __state_name'_transition, __default_next_state, __other_transitions_function
    DefineState __state_name, _move_to_playerXY, __state_name'_transition, __wait_cicles
.endm

.macro MoveToRandomXYState __state_name, __default_next_state, __other_transitions_function, __wait_cicles
    MoveToTransitionFunction __state_name'_transition, __default_next_state, __other_transitions_function
    DefineState __state_name, _move_to_randomXY, __state_name'_transition, __wait_cicles
.endm

; .macro MoveToBossActionFunction __name
; __name::
;     call _move_to_bossXY
;     ret
; .endm


.macro MoveToBossState __state_name, __default_next_state, __other_transitions_function, __wait_cicles
    ; MoveToBossActionFunction __state_name'_move
    MoveToTransitionFunction __state_name'_transition, __default_next_state, __other_transitions_function
    DefineState __state_name, _move_to_bossXY, __state_name'_transition, __wait_cicles
.endm

.macro AutoDestroyActionFunction __name
__name::
    ld a, ENT_HEALTH(ix)        ;; Set damage to kill entity
    ld ENT_DAMAGE(ix), a
    ret
.endm

.macro AutoDestroyState __state_name, __other_transitions_function
    AutoDestroyActionFunction __state_name'_move
    WanderTransition __state_name'_transition, __other_transitions_function
    DefineState __state_name, __state_name'_move, __state_name'_transition, #25
.endm

; .macro JumpToPlayerActionFunction __name
; __name::
;     call _jump_move
;     ret
; .endm

.macro JumpToPlayerState __state_name, __default_next_state, __other_transitions_function, __wait_cicles
    ; JumpToPlayerActionFunction __state_name'_move
    MoveToTransitionFunction __state_name'_transition, __default_next_state, __other_transitions_function
    DefineState __state_name, _jump_move, __state_name'_transition, __wait_cicles
.endm

.macro JumpState __state_name, __default_next_state, __other_transitions_function, __wait_cicles
    ; JumpToPlayerActionFunction __state_name'_move
    MoveToTransitionFunction __state_name'_transition, __default_next_state, __other_transitions_function
    DefineState __state_name, _jump_steady, __state_name'_transition, __wait_cicles
.endm

.macro SpawnActionFunction __name, __enemy1, __enemy2
__name::
    ld hl, #__enemy1
    ld de, #__enemy2
    call _prepare_spawn
    ; ld b, #THIRD_BOSS_SPAWN_POINT_1_X
    ; ld c, #THIRD_BOSS_SPAWN_POINT_1_y
    ; call _spawn_entity
    ; ld hl, #__enemy2
    ; ld b, #THIRD_BOSS_SPAWN_POINT_2_X
    ; ld c, #THIRD_BOSS_SPAWN_POINT_2_y
    ; call _spawn_entity
    ret
.endm

.macro SpawnState __state_name, __enemy1, __enemy2, __default_next_state, __other_transitions_function, __wait_cicles
    SpawnActionFunction __state_name'_move, __enemy1, __enemy2
    ShootTransition __state_name'_transition, __default_next_state, __other_transitions_function
    DefineState __state_name, __state_name'_move, __state_name'_transition, __wait_cicles
.endm

.macro WaitActionFunction __name, __times
__name::
    ld b, #__times
    call _wait_cycles_B_times
    ret
.endm

.macro WaitTransition __name, __default_next_state, __other_transitions_function
__name::
    call __other_transitions_function
    ld hl, #__default_next_state
    call _wait_transition
    ret
.endm

.macro WaitState __state_name, __times, __default_next_state, __other_transitions_function, __wait_cicles
    WaitActionFunction __state_name'_move, __times
    WaitTransition __state_name'_transition, __default_next_state, __other_transitions_function
    DefineState __state_name, __state_name'_move, __state_name'_transition, __wait_cicles
.endm

.macro DoNothingState __state_name, __other_transitions_function, __wait_cicles
    DefineState __state_name, _no_other_transitions, __other_transitions_function, __wait_cicles
.endm