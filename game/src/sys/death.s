.include "man/entity.h.s"
.include "man/game.h.s"
.include "man/entityTables.h.s"
.include "helper/sound.h.s"
.include "lib/opmacros.h.s"
.include "man/game.h.s"
.include "sys/animation.h.s"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Apply death animation to all entities
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, A, HL, BC
;; ----------------------------------------------------
sys_death_update::
    ld hl, #sys_death_forEachEntity
    ld a, #COMPONENT_L_DEATH
    ld b, #0
    call man_entity_forallMatchingEntities
    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Performs the actions for one entity
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity to die
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, A, HL, BC
;; ----------------------------------------------------
sys_death_forEachEntity::
    ;; Deactivate all systems except physics and render
    xor a
    or #COMPONENT_L_ANIMATION | COMPONENT_L_RENDER | COMPONENT_L_DEATH | COMPONENT_L_PHYSICS
    ld ENT_COMPONENTS_L(ix), a
    ld a, ENT_COMPONENTS_H(ix)
    and #COMPONENT_H_TILE_COLLISION             ;; Set the tile colision component only if the entity had it
    ld ENT_COMPONENTS_H(ix), a

    ;; Set death animation
    ld a, ENT_ANIMATION_ID(ix)
    cp #0xFF
    jr z, _already_set

    ld a, ENT_DEATH_ANIMATION_H(ix)
    ld h, a                 ;; HL = Animation
    ld a, ENT_DEATH_ANIMATION_L(ix)
    ld l, a
    ld b, #0xFF             ;; B = Animation ID
    call sys_animation_changeAnimation

    ;; Reset animation array
    ld ENT_ANIMATION_ARRAY_H(ix), #0
    ld ENT_ANIMATION_ARRAY_L(ix), #0

    ;; Stop blanking
    res #RENDER_BLANK_BIT, ENT_RENDER_FLAGS(ix)

    ;; Set x aceleration to 0
    ld ENT_ACCELERATION_X(ix), #0

	ld hl, #entity_table_onDeathStart
	call man_entity_entityTableWLookup
	call_hl

	ld h, ENT_POINTS_H(ix)
	ld l, ENT_POINTS_L(ix)
	ld bc, (game_score)
	add hl, bc
	ld (game_score), hl

_already_set:
    ;; Check death counter
    ld a, ENT_DEATH_COUNTER(ix)
    or a
    jr nz, _decrement_return
	

	ld hl, #entity_table_onDeath
	call man_entity_entityTableWLookup
	call_hl


    ;; If counter is 0 delete entity
    push ix                         ;; Load ix onto hl
    pop hl
    call man_entity_markForDelete
    ret

_decrement_return:
    dec a
    ld ENT_DEATH_COUNTER(ix), a
    ret
