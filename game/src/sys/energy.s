.include "cfg.h.s"
.include "man/game.h.s"

sys_energy_cyclesCounter::
	.db 5

sys_energy_update::
	ld a, (sys_energy_cyclesCounter)
	dec a
	ld (sys_energy_cyclesCounter), a
	ret nz
	
	ld a, #ENERGY_REGENERATION_TIME
	ld (sys_energy_cyclesCounter), a

	ld a, (man_game_playerEnergy)

	cp #MAX_ENERGY
	ret z

	inc a
	ld (man_game_playerEnergy), a

	ret

