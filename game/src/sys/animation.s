.include "man/entity.h.s"
.include "assets/assets.h.s"
.include "cpctelera.h.s"
.include "animation.h.s"

;;CHAIR BOSS (BOSS 1)
;; Standing animation right
DefineAnimation _animation_boss1_standing_right, #_boss1_00, 7
DefineStep #_boss1_01, 7
EndDefine AR_STANDING_RIGHT

;; Standing shooting animation right
DefineAnimation _animation_boss1_shoot_standing_right, #_boss1_04, 7
EndDefine AR_SHOOTING_STANDING_RIGHT

;; Standing animation left
DefineAnimation _animation_boss1_standing_left, #_boss1_08, 7
DefineStep #_boss1_07, 7
EndDefine AR_STANDING_LEFT

;; Standing shooting animation left
DefineAnimation _animation_boss1_shooting_standing_left, #_boss1_09, 7
EndDefine AR_SHOOTING_STANDING_LEFT

;; Walk right animation
DefineAnimation _animation_boss1_walk_right, #_boss1_00, 6
DefineStep #_boss1_02, 6
DefineStep #_boss1_03, 6
EndDefine AR_WALK_RIGHT

;; Walk shooting right animation
DefineAnimation _animation_boss1_shooting_walk_right, #_boss1_04, 6
EndDefine AR_SHOOTING_WALK_RIGHT

;; Walk left animation
DefineAnimation _animation_boss1_walk_left, #_boss1_08, 6
DefineStep #_boss1_06, 6
DefineStep #_boss1_10, 6
EndDefine AR_WALK_LEFT

;; Walk shooting left animation
DefineAnimation _animation_boss1_shooting_walk_left, #_boss1_09, 6
EndDefine AR_SHOOTING_WALK_LEFT

;; Jumping up right
DefineAnimation _animation_boss1_up_right, #_boss1_00, 7
EndDefine AR_UP_RIGHT

;; Jumping shooting up right
DefineAnimation _animation_boss1_shooting_up_right, #_boss1_04, 7
EndDefine AR_SHOOTING_UP_RIGHT

;; Falling down right
DefineAnimation _animation_boss1_down_right, #_boss1_00, 3
EndDefine AR_DOWN_RIGHT

;; Falling shooting down right
DefineAnimation _animation_boss1_shooting_down_right, #_boss1_04, 3
EndDefine AR_SHOOTING_DOWN_RIGHT

;; Jumping up left
DefineAnimation _animation_boss1_up_left, #_boss1_08, 7
EndDefine AR_UP_LEFT

;; Jumping shooting up left
DefineAnimation _animation_boss1_shooting_up_left, #_boss1_09, 7
EndDefine AR_SHOOTING_UP_LEFT

;; Falling down left
DefineAnimation _animation_boss1_down_left, #_boss1_08, 3
EndDefine AR_DOWN_LEFT

;; Falling shooting down left
DefineAnimation _animation_boss1_shooting_down_left, #_boss1_09, 3
EndDefine AR_SHOOTING_DOWN_LEFT

DefineAnimation _animation_boss1_death, #_boss1_00, 4
DefineStep #_boss1_08, 4
DefineStep #_boss1_00, 4
DefineStep #_boss1_08, 4
DefineStep #_boss1_00, 4
DefineStep #_boss1_08, 4
DefineStep #_boss1_00, 4
DefineStep #_boss1_08, 4
DefineStep #_boss1_00, 4
DefineStep #_boss1_08, 4
DefineStep #_boss1_05, 100
EndDefine DEATH_BOSS1



DefineAnimationArray _boss1_animation_array



;;BOSS 2
;; Standing animation right
DefineAnimation _animation_boss2_standing_right, #_boss2_0, 7
DefineStep #_boss2_1, 7
EndDefine AR_STANDING_RIGHT

;; Standing shooting animation right
DefineAnimation _animation_boss2_shoot_standing_right, #_boss2_4, 7
EndDefine AR_SHOOTING_STANDING_RIGHT

;; Standing animation left
DefineAnimation _animation_boss2_standing_left, #_boss2_3, 7
DefineStep #_boss2_2, 7
EndDefine AR_STANDING_LEFT

;; Standing shooting animation left
DefineAnimation _animation_boss2_shooting_standing_left, #_boss2_8, 7
EndDefine AR_SHOOTING_STANDING_LEFT

;; Walk right animation
DefineAnimation _animation_boss2_walk_right, #_boss2_0, 6
; DefineStep #_boss2_0, 6
; DefineStep #_boss2_3, 6
; DefineStep #_boss2_0, 6
EndDefine AR_WALK_RIGHT

;; Walk shooting right animation
DefineAnimation _animation_boss2_shooting_walk_right, #_boss2_4, 6
EndDefine AR_SHOOTING_WALK_RIGHT

;; Walk left animation
DefineAnimation _animation_boss2_walk_left, #_boss2_3, 6
; DefineStep #_boss2_12, 6
; DefineStep #_boss2_9, 6
; DefineStep #_boss2_12, 6
EndDefine AR_WALK_LEFT

;; Walk shooting left animation
DefineAnimation _animation_boss2_shooting_walk_left, #_boss2_8, 6
EndDefine AR_SHOOTING_WALK_LEFT

;; Jumping up right
DefineAnimation _animation_boss2_up_right, #_boss2_5, 7
EndDefine AR_UP_RIGHT

;; Jumping shooting up right
DefineAnimation _animation_boss2_shooting_up_right, #_boss2_4, 7
EndDefine AR_SHOOTING_UP_RIGHT

;; Falling down right
DefineAnimation _animation_boss2_down_right, #_boss2_5, 3
EndDefine AR_DOWN_RIGHT

;; Falling shooting down right
DefineAnimation _animation_boss2_shooting_down_right, #_boss2_4, 3
EndDefine AR_SHOOTING_DOWN_RIGHT

;; Jumping up left
DefineAnimation _animation_boss2_up_left, #_boss2_7, 7
EndDefine AR_UP_LEFT

;; Jumping shooting up left
DefineAnimation _animation_boss2_shooting_up_left, #_boss2_8, 7
EndDefine AR_SHOOTING_UP_LEFT

;; Falling down left
DefineAnimation _animation_boss2_down_left, #_boss2_7, 3
EndDefine AR_DOWN_LEFT

;; Falling shooting down left
DefineAnimation _animation_boss2_shooting_down_left, #_boss2_8, 3
EndDefine AR_SHOOTING_DOWN_LEFT

DefineAnimation _animation_boss2_death, #_boss2_0, 4
DefineStep #_boss2_3, 4
DefineStep #_boss2_0, 4
DefineStep #_boss2_3, 4
DefineStep #_boss2_0, 4
DefineStep #_boss2_3, 4
DefineStep #_boss2_0, 4
DefineStep #_boss2_3, 4
DefineStep #_boss2_0, 4
DefineStep #_boss2_3, 4
DefineStep #_boss2_6, 100
EndDefine DEATH_BOSS2

DefineAnimationArray _boss2_animation_array


;; BOSS 3 (DOES NOT USES AN ANIMATION ARRAY)

;; Animation doing nothing
DefineAnimation _animation_boss3_doing_nothing, #_boss3_0, 255
EndDefine NAR_DOING_NOTHING

;; Animation spawning
DefineAnimation _animation_boss3_spawning, #_boss3_1, 10
DefineStep #_boss3_2, 10
EndDefine NAR_SPAWNING

;; Death animation
DefineAnimation _animation_boss3_death, #_boss3_1, 4
DefineStep #_boss3_2, 4
DefineStep #_boss3_1, 4
DefineStep #_boss3_2, 4
DefineStep #_boss3_1, 4
DefineStep #_boss3_2, 4
DefineStep #_boss3_1, 4
DefineStep #_boss3_2, 4
DefineStep #_boss3_1, 4
DefineStep #_boss3_2, 4
DefineStep #_boss3_3, 100
EndDefine DEATH_BOSS3


;;BOSS 4
;; Standing animation right
DefineAnimation _animation_boss4_standing_right, #_boss4_00, 7
DefineStep #_boss4_01, 7
EndDefine AR_STANDING_RIGHT

;; Standing shooting animation right
DefineAnimation _animation_boss4_shoot_standing_right, #_boss4_04, 7
EndDefine AR_SHOOTING_STANDING_RIGHT

;; Standing animation left
DefineAnimation _animation_boss4_standing_left, #_boss4_12, 7
DefineStep #_boss4_11, 7
EndDefine AR_STANDING_LEFT

;; Standing shooting animation left
DefineAnimation _animation_boss4_shooting_standing_left, #_boss4_08, 7
EndDefine AR_SHOOTING_STANDING_LEFT

;; Walk right animation
DefineAnimation _animation_boss4_walk_right, #_boss4_02, 6
DefineStep #_boss4_00, 6
DefineStep #_boss4_03, 6
DefineStep #_boss4_00, 6
EndDefine AR_WALK_RIGHT

;; Walk shooting right animation
DefineAnimation _animation_boss4_shooting_walk_right, #_boss4_04, 6
EndDefine AR_SHOOTING_WALK_RIGHT

;; Walk left animation
DefineAnimation _animation_boss4_walk_left, #_boss4_10, 6
DefineStep #_boss4_12, 6
DefineStep #_boss4_09, 6
DefineStep #_boss4_12, 6
EndDefine AR_WALK_LEFT

;; Walk shooting left animation
DefineAnimation _animation_boss4_shooting_walk_left, #_boss4_08, 6
EndDefine AR_SHOOTING_WALK_LEFT

;; Jumping up right
DefineAnimation _animation_boss4_up_right, #_boss4_05, 7
EndDefine AR_UP_RIGHT

;; Jumping shooting up right
DefineAnimation _animation_boss4_shooting_up_right, #_boss4_04, 7
EndDefine AR_SHOOTING_UP_RIGHT

;; Falling down right
DefineAnimation _animation_boss4_down_right, #_boss4_05, 3
EndDefine AR_DOWN_RIGHT

;; Falling shooting down right
DefineAnimation _animation_boss4_shooting_down_right, #_boss4_04, 7
EndDefine AR_SHOOTING_DOWN_RIGHT

;; Jumping up left
DefineAnimation _animation_boss4_up_left, #_boss4_07, 7
EndDefine AR_UP_LEFT

;; Jumping shooting up left
DefineAnimation _animation_boss4_shooting_up_left, #_boss4_08, 7
EndDefine AR_SHOOTING_UP_LEFT

;; Falling down left
DefineAnimation _animation_boss4_down_left, #_boss4_07, 3
EndDefine AR_DOWN_LEFT

;; Falling shooting down left
DefineAnimation _animation_boss4_shooting_down_left, #_boss4_08, 7
EndDefine AR_SHOOTING_DOWN_LEFT

DefineAnimation _animation_boss4_death, #_boss4_00, 4
DefineStep #_boss4_12, 4
DefineStep #_boss4_00, 4
DefineStep #_boss4_12, 4
DefineStep #_boss4_00, 4
DefineStep #_boss4_12, 4
DefineStep #_boss4_00, 4
DefineStep #_boss4_12, 4
DefineStep #_boss4_00, 4
DefineStep #_boss4_12, 4
DefineStep #_boss4_06, 100
EndDefine DEATH_BOSS4

DefineAnimationArray _boss4_animation_array


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;BIRD MINION
;;

;; Standing animation right
DefineAnimation _animation_bird_standing_right, #_sprite_bird_0, 7
DefineStep #_sprite_bird_1, 7
EndDefine AR_STANDING_RIGHT

;; Standing shooting animation right
DefineAnimation _animation_bird_shoot_standing_right, #_sprite_bird_1, 7
EndDefine AR_SHOOTING_STANDING_RIGHT

;; Standing animation left
DefineAnimation _animation_bird_standing_left, #_sprite_bird_4, 7
DefineStep #_sprite_bird_3, 7
EndDefine AR_STANDING_LEFT

;; Standing shooting animation left
DefineAnimation _animation_bird_shooting_standing_left, #_sprite_bird_3, 7
EndDefine AR_SHOOTING_STANDING_LEFT

;; Walk right animation
DefineAnimation _animation_bird_walk_right, #_sprite_bird_0, 6
DefineStep #_sprite_bird_1, 6
EndDefine AR_WALK_RIGHT

;; Walk shooting right animation
DefineAnimation _animation_bird_shooting_walk_right, #_sprite_bird_1, 6
EndDefine AR_SHOOTING_WALK_RIGHT

;; Walk left animation
DefineAnimation _animation_bird_walk_left, #_sprite_bird_4, 6
DefineStep #_sprite_bird_3, 6
EndDefine AR_WALK_LEFT

;; Walk shooting left animation
DefineAnimation _animation_bird_shooting_walk_left, #_sprite_bird_3, 6
EndDefine AR_SHOOTING_WALK_LEFT

;; Jumping up right
DefineAnimation _animation_bird_up_right, #_sprite_bird_0, 7
DefineStep #_sprite_bird_1, 6
EndDefine AR_UP_RIGHT

;; Jumping shooting up right
DefineAnimation _animation_bird_shooting_up_right, #_sprite_bird_1, 7
EndDefine AR_SHOOTING_UP_RIGHT

;; Falling down right
DefineAnimation _animation_bird_down_right, #_sprite_bird_0, 3
DefineStep #_sprite_bird_1, 6
EndDefine AR_DOWN_RIGHT

;; Falling shooting down right
DefineAnimation _animation_bird_shooting_down_right, #_sprite_bird_1, 3
EndDefine AR_SHOOTING_DOWN_RIGHT

;; Jumping up left
DefineAnimation _animation_bird_up_left, #_sprite_bird_4, 7
DefineStep #_sprite_bird_3, 6
EndDefine AR_UP_LEFT

;; Jumping shooting up left
DefineAnimation _animation_bird_shooting_up_left, #_sprite_bird_3, 7
EndDefine AR_SHOOTING_UP_LEFT

;; Falling down left
DefineAnimation _animation_bird_down_left, #_sprite_bird_4, 3
DefineStep #_sprite_bird_3, 6
EndDefine AR_DOWN_LEFT

;; Falling shooting down left
DefineAnimation _animation_bird_shooting_down_left, #_sprite_bird_3, 3
EndDefine AR_SHOOTING_DOWN_LEFT

DefineAnimation _animation_bird_death, #_sprite_bird_2, 10
EndDefine DEATH_BIRD

DefineAnimationArray _bird_animation_array




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;BOLD MINION
;;
;

;; Standing animation right
DefineAnimation _bold_standing_right, #_sprite_minions_00, 8
DefineStep #_sprite_minions_01, 8
EndDefine AR_STANDING_RIGHT

;; Shooting standing right
DefineAnimation _bold_shooting_standing_right, #_sprite_minions_00, 4
EndDefine AR_SHOOTING_STANDING_RIGHT

;; Standing animation left
DefineAnimation _bold_standing_left, #_sprite_minions_07, 8
DefineStep #_sprite_minions_06, 8
EndDefine AR_STANDING_LEFT

;; Shooting standing left
DefineAnimation _bold_shooting_standing_left, #_sprite_minions_09, 4
EndDefine AR_SHOOTING_STANDING_LEFT

;; Walk right animation
DefineAnimation _bold_walk_right, #_sprite_minions_02, 3
DefineStep #_sprite_minions_00, 3
DefineStep #_sprite_minions_03, 3
DefineStep #_sprite_minions_00, 3
EndDefine AR_WALK_RIGHT

;; Walk shooting right animation
DefineAnimation _bold_shooting_walk_right, #_sprite_minions_02, 3
DefineStep #_sprite_minions_00, 3
DefineStep #_sprite_minions_03, 3
DefineStep #_sprite_minions_00, 3
EndDefine AR_SHOOTING_WALK_RIGHT

;; Walk left animation
DefineAnimation _bold_walk_left, #_sprite_minions_05, 3
DefineStep #_sprite_minions_07, 3
DefineStep #_sprite_minions_04, 3
DefineStep #_sprite_minions_07, 3
EndDefine AR_WALK_LEFT

;; Walk shooting left animation
DefineAnimation _bold_shooting_walk_left, #_sprite_minions_05, 3
DefineStep #_sprite_minions_07, 3
DefineStep #_sprite_minions_04, 3
DefineStep #_sprite_minions_07, 3
EndDefine AR_SHOOTING_WALK_LEFT

;; Jumping up right
DefineAnimation _bold_up_right, #_sprite_minions_02, 4
EndDefine AR_UP_RIGHT

;; Jumping shooting up right
DefineAnimation _bold_shooting_up_right, #_sprite_minions_02, 4
EndDefine AR_SHOOTING_UP_RIGHT

;; Falling down right
DefineAnimation _bold_down_right, #_sprite_minions_03, 4
EndDefine AR_DOWN_RIGHT

;; Falling shooting down right
DefineAnimation _animation_bold_down_right, #_sprite_minions_03, 4
EndDefine AR_SHOOTING_DOWN_RIGHT

;; Jumping up left
DefineAnimation _bold_up_left, #_sprite_minions_05, 4
EndDefine AR_UP_LEFT

;; Jumping shooting up left
DefineAnimation _bold_shooting_up_left, #_sprite_minions_05, 4
EndDefine AR_SHOOTING_UP_LEFT

;; Falling down left
DefineAnimation _bold_down_left, #_sprite_minions_04, 2
EndDefine AR_DOWN_LEFT

;; Falling shooting down left
DefineAnimation _bold_shooting_down_left, #_sprite_minions_04, 4
EndDefine AR_SHOOTING_DOWN_LEFT

;; Death animation
DefineAnimation _bold_death_animation, #_sprite_minions_00, 2
DefineStep #_sprite_minions_07, 2
DefineStep #_sprite_minions_00, 2
DefineStep #_sprite_minions_07, 2
DefineStep #_sprite_minions_00, 2
DefineStep #_sprite_minions_07, 2
DefineStep #_sprite_player_14, 50

DefineAnimationArray _bold_animation_array



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;BOY MINION
;;
;

;; Standing animation right
DefineAnimation _boy_standing_right, #_sprite_minions_08, 8
DefineStep #_sprite_minions_09, 8
EndDefine AR_STANDING_RIGHT

;; Shooting standing right
DefineAnimation _boy_shooting_standing_right, #_sprite_minions_08, 4
EndDefine AR_SHOOTING_STANDING_RIGHT

;; Standing animation left
DefineAnimation _boy_standing_left, #_sprite_minions_15, 8
DefineStep #_sprite_minions_14, 8
EndDefine AR_STANDING_LEFT

;; Shooting standing left
DefineAnimation _boy_shooting_standing_left, #_sprite_minions_15, 4
EndDefine AR_SHOOTING_STANDING_LEFT

;; Walk right animation
DefineAnimation _boy_walk_right, #_sprite_minions_10, 3
DefineStep #_sprite_minions_08, 3
DefineStep #_sprite_minions_11, 3
DefineStep #_sprite_minions_08, 3
EndDefine AR_WALK_RIGHT

;; Walk shooting right animation
DefineAnimation _boy_shooting_walk_right, #_sprite_minions_10, 3
DefineStep #_sprite_minions_08, 3
DefineStep #_sprite_minions_11, 3
DefineStep #_sprite_minions_08, 3
EndDefine AR_SHOOTING_WALK_RIGHT

;; Walk left animation
DefineAnimation _boy_walk_left, #_sprite_minions_13, 3
DefineStep #_sprite_minions_15, 3
DefineStep #_sprite_minions_12, 3
DefineStep #_sprite_minions_15, 3
EndDefine AR_WALK_LEFT

;; Walk shooting left animation
DefineAnimation _boy_shooting_walk_left, #_sprite_minions_13, 3
DefineStep #_sprite_minions_15, 3
DefineStep #_sprite_minions_12, 3
DefineStep #_sprite_minions_15, 3
EndDefine AR_SHOOTING_WALK_LEFT

;; Jumping up right
DefineAnimation _boy_up_right, #_sprite_minions_10, 4
EndDefine AR_UP_RIGHT

;; Jumping shooting up right
DefineAnimation _boy_shooting_up_right, #_sprite_minions_10, 4
EndDefine AR_SHOOTING_UP_RIGHT

;; Falling down right
DefineAnimation _boy_down_right, #_sprite_minions_11, 4
EndDefine AR_DOWN_RIGHT

;; Falling shooting down right
DefineAnimation _animation_boy_down_right, #_sprite_minions_11, 4
EndDefine AR_SHOOTING_DOWN_RIGHT

;; Jumping up left
DefineAnimation _boy_up_left, #_sprite_minions_13, 4
EndDefine AR_UP_LEFT

;; Jumping shooting up left
DefineAnimation _boy_shooting_up_left, #_sprite_minions_13, 4
EndDefine AR_SHOOTING_UP_LEFT

;; Falling down left
DefineAnimation _boy_down_left, #_sprite_minions_12, 2
EndDefine AR_DOWN_LEFT

;; Falling shooting down left
DefineAnimation _boy_shooting_down_left, #_sprite_minions_12, 4
EndDefine AR_SHOOTING_DOWN_LEFT

;; Death animation
DefineAnimation _boy_death_animation, #_sprite_minions_08, 2
DefineStep #_sprite_minions_15, 2
DefineStep #_sprite_minions_08, 2
DefineStep #_sprite_minions_15, 2
DefineStep #_sprite_minions_08, 2
DefineStep #_sprite_minions_15, 2
DefineStep #_sprite_player_14, 50

DefineAnimationArray _boy_animation_array



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;GIRL MINION
;;
;

;; Standing animation right
DefineAnimation _girl_standing_right, #_sprite_minions_16, 8
DefineStep #_sprite_minions_17, 8
EndDefine AR_STANDING_RIGHT

;; Shooting standing right
DefineAnimation _girl_shooting_standing_right, #_sprite_minions_16, 4
EndDefine AR_SHOOTING_STANDING_RIGHT

;; Standing animation left
DefineAnimation _girl_standing_left, #_sprite_minions_23, 4
DefineStep #_sprite_minions_22, 4
EndDefine AR_STANDING_LEFT

;; Shooting standing left
DefineAnimation _girl_shooting_standing_left, #_sprite_minions_23, 4
EndDefine AR_SHOOTING_STANDING_LEFT

;; Walk right animation
DefineAnimation _girl_walk_right, #_sprite_minions_18, 3
DefineStep #_sprite_minions_16, 3
DefineStep #_sprite_minions_19, 3
DefineStep #_sprite_minions_16, 3
EndDefine AR_WALK_RIGHT

;; Walk shooting right animation
DefineAnimation _girl_shooting_walk_right, #_sprite_minions_18, 3
DefineStep #_sprite_minions_16, 3
DefineStep #_sprite_minions_19, 3
DefineStep #_sprite_minions_16, 3
EndDefine AR_SHOOTING_WALK_RIGHT

;; Walk left animation
DefineAnimation _girl_walk_left, #_sprite_minions_21, 3
DefineStep #_sprite_minions_23, 3
DefineStep #_sprite_minions_20, 3
DefineStep #_sprite_minions_23, 3
EndDefine AR_WALK_LEFT

;; Walk shooting left animation
DefineAnimation _girl_shooting_walk_left, #_sprite_minions_21, 3
DefineStep #_sprite_minions_23, 3
DefineStep #_sprite_minions_20, 3
DefineStep #_sprite_minions_23, 3
EndDefine AR_SHOOTING_WALK_LEFT

;; Jumping up right
DefineAnimation _girl_up_right, #_sprite_minions_18, 4
EndDefine AR_UP_RIGHT

;; Jumping shooting up right
DefineAnimation _girl_shooting_up_right, #_sprite_minions_18, 4
EndDefine AR_SHOOTING_UP_RIGHT

;; Falling down right
DefineAnimation _girl_down_right, #_sprite_minions_19, 4
EndDefine AR_DOWN_RIGHT

;; Falling shooting down right
DefineAnimation _animation_girl_down_right, #_sprite_minions_19, 4
EndDefine AR_SHOOTING_DOWN_RIGHT

;; Jumping up left
DefineAnimation _girl_up_left, #_sprite_minions_21, 4
EndDefine AR_UP_LEFT

;; Jumping shooting up left
DefineAnimation _girl_shooting_up_left, #_sprite_minions_21, 4
EndDefine AR_SHOOTING_UP_LEFT

;; Falling down left
DefineAnimation _girl_down_left, #_sprite_minions_20, 2
EndDefine AR_DOWN_LEFT

;; Falling shooting down left
DefineAnimation _girl_shooting_down_left, #_sprite_minions_20, 4
EndDefine AR_SHOOTING_DOWN_LEFT

;; Death animation
DefineAnimation _girl_death_animation, #_sprite_minions_16, 2
DefineStep #_sprite_minions_23, 2
DefineStep #_sprite_minions_16, 2
DefineStep #_sprite_minions_23, 2
DefineStep #_sprite_minions_16, 2
DefineStep #_sprite_minions_23, 2
DefineStep #_sprite_player_14, 50

DefineAnimationArray _girl_animation_array


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;RAT MINION
;;
;

;; Standing animation right
DefineAnimation _rat_standing_right, #_sprite_rat_0, 8
EndDefine AR_STANDING_RIGHT

;; Shooting standing right
DefineAnimation _rat_shooting_standing_right, #_sprite_rat_0, 4
EndDefine AR_SHOOTING_STANDING_RIGHT

;; Standing animation left
DefineAnimation _rat_standing_left, #_sprite_rat_4, 4
EndDefine AR_STANDING_LEFT

;; Shooting standing left
DefineAnimation _rat_shooting_standing_left, #_sprite_rat_4, 4
EndDefine AR_SHOOTING_STANDING_LEFT

;; Walk right animation
DefineAnimation _rat_walk_right, #_sprite_rat_0, 3
DefineStep #_sprite_rat_1, 3
EndDefine AR_WALK_RIGHT

;; Walk shooting right animation
DefineAnimation _rat_shooting_walk_right, #_sprite_rat_0, 3
DefineStep #_sprite_rat_1, 3
EndDefine AR_SHOOTING_WALK_RIGHT

;; Walk left animation
DefineAnimation _rat_walk_left, #_sprite_rat_4, 3
DefineStep #_sprite_rat_3, 3
EndDefine AR_WALK_LEFT

;; Walk shooting left animation
DefineAnimation _rat_shooting_walk_left, #_sprite_rat_4, 3
DefineStep #_sprite_rat_3, 3
EndDefine AR_SHOOTING_WALK_LEFT

;; Jumping up right
DefineAnimation _rat_up_right, #_sprite_rat_0, 4
EndDefine AR_UP_RIGHT

;; Jumping shooting up right
DefineAnimation _rat_shooting_up_right, #_sprite_rat_0, 4
EndDefine AR_SHOOTING_UP_RIGHT

;; Falling down right
DefineAnimation _rat_down_right, #_sprite_rat_0, 4
EndDefine AR_DOWN_RIGHT

;; Falling shooting down right
DefineAnimation _animation_rat_down_right, #_sprite_rat_0, 4
EndDefine AR_SHOOTING_DOWN_RIGHT

;; Jumping up left
DefineAnimation _rat_up_left, #_sprite_rat_4, 4
EndDefine AR_UP_LEFT

;; Jumping shooting up left
DefineAnimation _rat_shooting_up_left, #_sprite_rat_4, 4
EndDefine AR_SHOOTING_UP_LEFT

;; Falling down left
DefineAnimation _rat_down_left, #_sprite_rat_4, 2
EndDefine AR_DOWN_LEFT

;; Falling shooting down left
DefineAnimation _rat_shooting_down_left, #_sprite_rat_4, 4
EndDefine AR_SHOOTING_DOWN_LEFT

;; Death animation
DefineAnimation _rat_death_animation, #_sprite_rat_0, 2
DefineStep #_sprite_rat_4, 2
DefineStep #_sprite_rat_0, 2
DefineStep #_sprite_rat_4, 2
DefineStep #_sprite_rat_0, 2
DefineStep #_sprite_rat_4, 2
DefineStep #_sprite_rat_2, 50

DefineAnimationArray _rat_animation_array



; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; ;;CRAZY MINION
; ;;
; ;

; ;; Standing animation right
; DefineAnimation _crazy_standing_right, #_sprite_minions_30, 4
; DefineStep #_sprite_minions_31, 4
; DefineStep #_sprite_minions_32, 4
; DefineStep #_sprite_minions_31, 4
; EndDefine AR_STANDING_RIGHT

; ;; Shooting standing right
; DefineAnimation _crazy_shooting_standing_right, #_sprite_minions_30, 4
; EndDefine AR_SHOOTING_STANDING_RIGHT

; ;; Standing animation left
; DefineAnimation _crazy_standing_left, #_sprite_minions_39, 4
; DefineStep #_sprite_minions_38, 4
; DefineStep #_sprite_minions_37, 4
; DefineStep #_sprite_minions_38, 4
; EndDefine AR_STANDING_LEFT

; ;; Shooting standing left
; DefineAnimation _crazy_shooting_standing_left, #_sprite_minions_39, 4
; EndDefine AR_SHOOTING_STANDING_LEFT

; ;; Walk right animation
; DefineAnimation _crazy_walk_right, #_sprite_minions_33, 3
; DefineStep #_sprite_minions_30, 3
; DefineStep #_sprite_minions_34, 3
; DefineStep #_sprite_minions_30, 3
; EndDefine AR_WALK_RIGHT

; ;; Walk shooting right animation
; DefineAnimation _crazy_shooting_walk_right, #_sprite_minions_33, 3
; DefineStep #_sprite_minions_30, 3
; DefineStep #_sprite_minions_34, 3
; DefineStep #_sprite_minions_30, 3
; EndDefine AR_SHOOTING_WALK_RIGHT

; ;; Walk left animation
; DefineAnimation _crazy_walk_left, #_sprite_minions_36, 3
; DefineStep #_sprite_minions_39, 3
; DefineStep #_sprite_minions_35, 3
; DefineStep #_sprite_minions_39, 3
; EndDefine AR_WALK_LEFT

; ;; Walk shooting left animation
; DefineAnimation _crazy_shooting_walk_left, #_sprite_minions_36, 3
; DefineStep #_sprite_minions_39, 3
; DefineStep #_sprite_minions_35, 3
; DefineStep #_sprite_minions_39, 3
; EndDefine AR_SHOOTING_WALK_LEFT

; ;; Jumping up right
; DefineAnimation _crazy_up_right, #_sprite_minions_33, 4
; EndDefine AR_UP_RIGHT

; ;; Jumping shooting up right
; DefineAnimation _crazy_shooting_up_right, #_sprite_minions_33, 4
; EndDefine AR_SHOOTING_UP_RIGHT

; ;; Falling down right
; DefineAnimation _crazy_down_right, #_sprite_minions_34, 4
; EndDefine AR_DOWN_RIGHT

; ;; Falling shooting down right
; DefineAnimation _animation_crazy_down_right, #_sprite_minions_34, 4
; EndDefine AR_SHOOTING_DOWN_RIGHT

; ;; Jumping up left
; DefineAnimation _crazy_up_left, #_sprite_minions_36, 4
; EndDefine AR_UP_LEFT

; ;; Jumping shooting up left
; DefineAnimation _crazy_shooting_up_left, #_sprite_minions_36, 4
; EndDefine AR_SHOOTING_UP_LEFT

; ;; Falling down left
; DefineAnimation _crazy_down_left, #_sprite_minions_35, 2
; EndDefine AR_DOWN_LEFT

; ;; Falling shooting down left
; DefineAnimation _crazy_shooting_down_left, #_sprite_minions_35, 4
; EndDefine AR_SHOOTING_DOWN_LEFT

; ;; Death animation
; DefineAnimation _crazy_death_animation, #_sprite_minions_30, 2
; DefineStep #_sprite_minions_39, 2
; DefineStep #_sprite_minions_30, 2
; DefineStep #_sprite_minions_39, 2
; DefineStep #_sprite_minions_30, 2
; DefineStep #_sprite_minions_39, 2
; DefineStep #_sprite_player_14, 50

; DefineAnimationArray _crazy_animation_array



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;PLAYER
;;
;

;; Standing animation right
DefineAnimation _animation_standing_right, #_sprite_player_00, 4
DefineStep #_sprite_player_01, 4
DefineStep #_sprite_player_02, 4
DefineStep #_sprite_player_01, 4
EndDefine AR_STANDING_RIGHT

;; Shooting standing right
DefineAnimation _animation_shooting_standing_right, #_sprite_player_06, 4
EndDefine AR_SHOOTING_STANDING_RIGHT

;; Standing animation left
DefineAnimation _animation_standing_left, #_sprite_player_26, 4
DefineStep #_sprite_player_25, 4
DefineStep #_sprite_player_24, 4
DefineStep #_sprite_player_25, 4
EndDefine AR_STANDING_LEFT

;; Shooting standing left
DefineAnimation _animation_shooting_standing_left, #_sprite_player_20, 4
EndDefine AR_SHOOTING_STANDING_LEFT

;; Walk right animation
DefineAnimation _animation_walk_right, #_sprite_player_03, 3
DefineStep #_sprite_player_00, 3
DefineStep #_sprite_player_04, 3
DefineStep #_sprite_player_00, 3
EndDefine AR_WALK_RIGHT

;; Walk shooting right animation
DefineAnimation _animation_shooting_walk_right, #_sprite_player_05, 3
DefineStep #_sprite_player_06, 3
DefineStep #_sprite_player_07, 3
DefineStep #_sprite_player_06, 3
EndDefine AR_SHOOTING_WALK_RIGHT

;; Walk left animation
DefineAnimation _animation_walk_left, #_sprite_player_23, 3
DefineStep #_sprite_player_26, 3
DefineStep #_sprite_player_22, 3
DefineStep #_sprite_player_26, 3
EndDefine AR_WALK_LEFT

;; Walk shooting left animation
DefineAnimation _animation_shooting_walk_left, #_sprite_player_21, 3
DefineStep #_sprite_player_20, 3
DefineStep #_sprite_player_19, 3
DefineStep #_sprite_player_20, 3
EndDefine AR_SHOOTING_WALK_LEFT

;; Jumping up right
DefineAnimation _animation_up_right, #_sprite_player_08, 4
EndDefine AR_UP_RIGHT

;; Jumping shooting up right
DefineAnimation _animation_shooting_up_right, #_sprite_player_10, 4
EndDefine AR_SHOOTING_UP_RIGHT

;; Falling down right
DefineAnimation _animation_down_right, #_sprite_player_09, 4
EndDefine AR_DOWN_RIGHT

;; Falling shooting down right
DefineAnimation _animation_shooting_down_right, #_sprite_player_11, 4
EndDefine AR_SHOOTING_DOWN_RIGHT

;; Jumping up left
DefineAnimation _animation_up_left, #_sprite_player_18, 4
EndDefine AR_UP_LEFT

;; Jumping shooting up left
DefineAnimation _animation_shooting_up_left, #_sprite_player_16, 4
EndDefine AR_SHOOTING_UP_LEFT

;; Falling down left
DefineAnimation _animation_down_left, #_sprite_player_17, 2
EndDefine AR_DOWN_LEFT

;; Falling shooting down left
DefineAnimation _animation_shooting_down_left, #_sprite_player_15, 4
EndDefine AR_SHOOTING_DOWN_LEFT

DefineAnimation _player_death_animation, #_sprite_player_12, 20
DefineStep #_sprite_player_13, 20
DefineStep #_sprite_player_14, 255
EndDefine PLAYER_DEATH

DefineAnimationArray _player_animation_array

DefineAnimation _animation_player_shoot #_sprite_player_shot_0, 3
DefineStep #_sprite_player_shot_1, 3
DefineStep #_sprite_player_shot_2, 3
DefineStep #_sprite_player_shot_3, 3
EndDefine PLAYER_SHOOT


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; INDICATOR ANIMATION
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
DefineAnimation _animation_indicator #_indicator_0, 6
DefineStep #_indicator_1, 6
EndDefine ANIMATION_INDICATOR




ANIM_SPRITE_L = 0
ANIM_SPRITE_H = 1
ANIM_WAIT_CICLES = 2
ANIM_NEXT_STEP_L = 3
ANIM_NEXT_STEP_H = 4

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Updates the animation system
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, A, BC, DE, IY
;; ----------------------------------------------------
sys_animation_update::
    ld a, #COMPONENT_L_ANIMATION
    ld b, #0
    ld hl, #sys_animation_forOneEntity
    call man_entity_forallMatchingEntities
    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Operates all the animation actions for one entity
;; ----------------------------------------------------
;; PARAMS
;; Entity to update
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, A, BC, DE, IY
;; ----------------------------------------------------
sys_animation_forOneEntity::
    ld a, ENT_ANIMATION_ARRAY_H(ix)             ;; Check if empty animation array
    or ENT_ANIMATION_ARRAY_L(ix)
    jp z, _one_loop_animation
    call sys_animation_setEntityAnimation
_one_loop_animation:
    call sys_animation_nextStep
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Sets the current entity animation based on its state
;; ----------------------------------------------------
;; PARAMS
;; IX: Pointer to player Entity
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A, B
;; ----------------------------------------------------
sys_animation_setEntityAnimation::
    ;; Load entity animation array
    ld a, ENT_ANIMATION_ARRAY_L(ix)
    ld__iyl_a
    ld a, ENT_ANIMATION_ARRAY_H(ix)
    ld__iyh_a

    ;; Check y speed
    ld a, ENT_VY(ix)
    or a
    jp nz, _jumping

    ;; Check x speed
    ld a, ENT_VX(ix)
    or a
    jp nz, _walking

_standing:
    ld a, ENT_FACING_DIR(ix)
    or a
    jp z, _standing_right

_standing_left:
    bit #COMPONENT_L_SHOOTING_BIT, ENT_COMPONENTS_L(ix)
    jp nz, _shooting_standing_left

    ld b, #AR_STANDING_LEFT_L
    ld l, AR_STANDING_LEFT_L(iy)
    ld h, AR_STANDING_LEFT_H(iy)
    call sys_animation_changeAnimation
    ret

_shooting_standing_left: ;; TODO
    ld b, #AR_SHOOTING_STANDING_LEFT_L
    ld l, AR_SHOOTING_STANDING_LEFT_L(iy)
    ld h, AR_SHOOTING_STANDING_LEFT_H(iy)
    call sys_animation_changeAnimation
    ret

_standing_right:
    bit #COMPONENT_L_SHOOTING_BIT, ENT_COMPONENTS_L(ix)
    jp nz, _shooting_standing_right

    ld b, #AR_STANDING_RIGHT_L
    ld l, AR_STANDING_RIGHT_L(iy)
    ld h, AR_STANDING_RIGHT_H(iy)
    call sys_animation_changeAnimation
    ret

_shooting_standing_right: ;; TODO
    ld b, #AR_SHOOTING_STANDING_RIGHT_L
    ld l, AR_SHOOTING_STANDING_RIGHT_L(iy)
    ld h, AR_SHOOTING_STANDING_RIGHT_H(iy)
    call sys_animation_changeAnimation
    ret

_walking:
    ld a, ENT_FACING_DIR(ix)
    or a
    jp z, _walking_right

_walking_left:
    bit #COMPONENT_L_SHOOTING_BIT, ENT_COMPONENTS_L(ix)
    jp nz, _shooting_walking_left

    ld b, #AR_WALK_LEFT_L
    ld l, AR_WALK_LEFT_L(iy)
    ld h, AR_WALK_LEFT_H(iy)
    call sys_animation_changeAnimation
    ret

_shooting_walking_left:
    ld b, #AR_SHOOTING_WALK_LEFT_L
    ld l, AR_SHOOTING_WALK_LEFT_L(iy)
    ld h, AR_SHOOTING_WALK_LEFT_H(iy)
    call sys_animation_changeAnimation
    ret

_walking_right:
    bit #COMPONENT_L_SHOOTING_BIT, ENT_COMPONENTS_L(ix)
    jp nz, _shooting_walking_right

    ld b, #AR_WALK_RIGHT_L
    ld l, AR_WALK_RIGHT_L(iy)
    ld h, AR_WALK_RIGHT_H(iy)
    call sys_animation_changeAnimation
    ret

_shooting_walking_right:
    ld b, #AR_SHOOTING_WALK_RIGHT_L
    ld l, AR_SHOOTING_WALK_RIGHT_L(iy)
    ld h, AR_SHOOTING_WALK_RIGHT_H(iy)
    call sys_animation_changeAnimation
    ret

_jumping:
    or a
    jp m, _jumping_up

_jumping_down:
    ld a, ENT_FACING_DIR(ix)
    or a
    jp z, _jumping_down_right

_jumping_down_left:
    bit #COMPONENT_L_SHOOTING_BIT, ENT_COMPONENTS_L(ix)
    jp nz, _shooting_jumping_down_left

    ld b, #AR_DOWN_LEFT_L
    ld l, AR_DOWN_LEFT_L(iy)
    ld h, AR_DOWN_LEFT_H(iy)
    call sys_animation_changeAnimation
    ret

_shooting_jumping_down_left:
    ld b, #AR_SHOOTING_DOWN_LEFT_L
    ld l, AR_SHOOTING_DOWN_LEFT_L(iy)
    ld h, AR_SHOOTING_DOWN_LEFT_H(iy)
    call sys_animation_changeAnimation
    ret

_jumping_down_right:
    bit #COMPONENT_L_SHOOTING_BIT, ENT_COMPONENTS_L(ix)
    jp nz, _shooting_jumping_down_right

    ld b, #AR_DOWN_RIGHT_L
    ld l, AR_DOWN_RIGHT_L(iy)
    ld h, AR_DOWN_RIGHT_H(iy)
    call sys_animation_changeAnimation
    ret

_shooting_jumping_down_right:
    ld b, #AR_SHOOTING_DOWN_RIGHT_L
    ld l, AR_SHOOTING_DOWN_RIGHT_L(iy)
    ld h, AR_SHOOTING_DOWN_RIGHT_H(iy)
    call sys_animation_changeAnimation
    ret

_jumping_up:
    ld a, ENT_FACING_DIR(ix)
    or a
    jp z, _jumping_up_right

_jumping_up_left:
    bit #COMPONENT_L_SHOOTING_BIT, ENT_COMPONENTS_L(ix)
    jp nz, _shooting_jumping_up_left

    ld b, #AR_UP_LEFT_L
    ld l, AR_UP_LEFT_L(iy)
    ld h, AR_UP_LEFT_H(iy)
    call sys_animation_changeAnimation
    ret

_shooting_jumping_up_left:
    ld b, #AR_SHOOTING_UP_LEFT_L
    ld l, AR_SHOOTING_UP_LEFT_L(iy)
    ld h, AR_SHOOTING_UP_LEFT_H(iy)
    call sys_animation_changeAnimation
    ret

_jumping_up_right:
    bit #COMPONENT_L_SHOOTING_BIT, ENT_COMPONENTS_L(ix)
    jp nz, _shooting_jumping_up_right

    ld b, #AR_UP_RIGHT_L
    ld l, AR_UP_RIGHT_L(iy)
    ld h, AR_UP_RIGHT_H(iy)
    call sys_animation_changeAnimation
    ret

_shooting_jumping_up_right:
    ld b, #AR_SHOOTING_UP_RIGHT_L
    ld l, AR_SHOOTING_UP_RIGHT_L(iy)
    ld h, AR_SHOOTING_UP_RIGHT_H(iy)
    call sys_animation_changeAnimation
    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Changes the animation of the entity on IX, with the animation on HL, and the type on A
;; ----------------------------------------------------
;; PARAMS
;; IX: Pointer to entity
;; HL: New animation
;; B : Animation ID
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A
;; ----------------------------------------------------
sys_animation_changeAnimation::
    ;; Check if the current animation is the same
    ld a, ENT_ANIMATION_ID(ix)
    cp b
    ret z

    ;; Save new animation ID
    ld ENT_ANIMATION_ID(ix), b

    ;; Change animation
    ld ENT_CURRENT_ANIMATION_H(ix), h
    ld ENT_CURRENT_ANIMATION_L(ix), l

    ;; Reset wait cycles counter
    xor a
    ld ENT_ANIMATION_COUNTER(ix), a
    ret

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; ;; DESCRIPTION
; ;; Checks if the animation has changed or its the same
; ;; ----------------------------------------------------
; ;; PARAMS
; ;; IX: Pointer to entity
; ;; ----------------------------------------------------
; ;; RETURNS
; ;; Z flag set if true. Reset if false
; ;; ----------------------------------------------------
; ;; DESTROYS
; ;; A
; ;; ----------------------------------------------------
; sys_animation_animationChanged::
;     ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Changes the sprite of the entity on IX to the next animation sprite
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity to change sprite
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IY, A
;; ----------------------------------------------------
sys_animation_nextStep::
    ld a, ENT_ANIMATION_COUNTER(ix)
    or a
    jp nz, _not_zero

    ;; Load current animation pointer to IY
    ld a, ENT_CURRENT_ANIMATION_L(ix)
    ld__iyl_a
    ld a, ENT_CURRENT_ANIMATION_H(ix)
    ld__iyh_a

    ; ;; Set current sprite to last sprite
    ; ld a, ENT_SPRITE_L(ix)
    ; ld ENT_LAST_SPRITE_L(ix), a
    ; ld a, ENT_SPRITE_H(ix)
    ; ld ENT_LAST_SPRITE_H(ix), a

    ;; Load next sprite to the entity
    ld a, ANIM_SPRITE_L(iy)
    ld ENT_SPRITE_L(ix), a
    ld a, ANIM_SPRITE_H(iy)
    ld ENT_SPRITE_H(ix), a

    ;; Load counter
    ld a, ANIM_WAIT_CICLES(iy)
    ld ENT_ANIMATION_COUNTER(ix), a

    ;; Update current animation step
    ld a, ANIM_NEXT_STEP_L(iy)
    ld ENT_CURRENT_ANIMATION_L(ix), a
    ld a, ANIM_NEXT_STEP_H(iy)
    ld ENT_CURRENT_ANIMATION_H(ix), a

    ;; Set animation changed to 1
    ; ld a, #1
    ; ld ENT_ANIMATION_CHANGED(ix), a
    ld a, ENT_RENDER_FLAGS(ix)
    set #RENDER_DRAW_BIT, a
    ld ENT_RENDER_FLAGS(ix), a

    ret

_not_zero:
    dec a
    ld ENT_ANIMATION_COUNTER(ix), a
    ret
