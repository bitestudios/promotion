;;;;;;;;;;;;;;;;;;;;;;;;;
;; PUBLIC FUNCTIONS
;;;;;;;;;;;;;;;;;;;;;;;;;
.globl sys_input_update
.globl sys_input_pauseUpdate
.globl sys_input_titleUpdate
.globl sys_input_waitKeyPressed

.globl button_left
.globl button_right
.globl button_jump
.globl button_pause
.globl button_shoot
.globl button_reset
.globl _key_released

BUFFER_SIZE = 10
ZERO_KEYS_ACTIVATED = #0xFF
