.include "man/entityTables.h.s"
.include "man/sfx.h.s"
.include "sys/physics.h.s"
.include "assets/assets.h.s"
.include "man/game.h.s"
.include "man/entity.h.s"
.include "utils.h.s"
.include "cpctelera.h.s"
.include "sys/collision.h.s"
.include "helper/sound.h.s"
.include "cfg.h.s"




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Updates the collision system
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; 
;; ----------------------------------------------------
sys_collision_updatePrev::
	ld hl, #sys_collision_checkEntitesCollision
	ld a, #COMPONENT_L_COLLISION
    ld b, #0
	call man_entity_compareAllMatchingEntities
	ret





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Updates the collision system
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; 
;; ----------------------------------------------------
sys_collision_update::
	xor a
    ld b, #COMPONENT_H_BOUNDARY_COLLISION
	ld hl, #sys_collision_checkBoundaryCollisions
    call man_entity_forallMatchingEntities
	xor a
    ld b, #COMPONENT_H_TILE_COLLISION
	ld hl, #sys_collision_tileCollision
    call man_entity_forallMatchingEntities

	;;Check if player collides with de floor and previously not
	__playerTileCollision = .+1
	ld a, #0                          ;; Loads on A the information flags
	sra a                             ;; Displaces the bits
	ld (__playerTileCollision), a     ;; Stores the result

	ret z                             ;; If is 0, has not collided this iteration
	call nc, man_sfx_playFall         ;;- Call to play FX if has collided this
									  ;;\_ iteration and not in the previous
    
	ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Checks if 2 entities are colliding
;; ----------------------------------------------------
;; PARAMS
;; IX = Entity 1
;; IY = Entity 2
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; ----------------------------------------------------
sys_collision_checkEntitesCollision::

	;; Checks if Enity 1 is collisionable with entity 2 and vice versa

	ld a, ENT_COLLIDES_WITH(ix)
	and ENT_TYPE_FLAG(iy)
	jr nz, checkCollisions

	ld a, ENT_COLLIDES_WITH(iy)
	and ENT_TYPE_FLAG(ix)
	ret z

	checkCollisions:

	;; X axis case 1
	;;    X1          X1+WIDTH1
	;; IX |-----------|
	;; IY               |-----------|
	;;                  X2          X2+WIDTH2
	;; X1 + WIDTH1 - 1 < X2
	;; X1 + WIDTH1 -1 - X2 < 0
	ld a, ENT_X(ix)
	add ENT_WIDTH(ix)
	dec a
	cp ENT_X(iy)
	ret c

	;; X axis case 2
	;;                  X1          X1+WIDTH1
	;; IX               |-----------|
	;; IY |-----------|
	;;    X2          X2+WIDTH2
	;; X2 + WIDTH2 -1 < X1
	;; X2 + WIDTH2 -1 - X1 < 0
	ld a, ENT_X(iy)
	add ENT_WIDTH(iy)
	dec a
	cp ENT_X(ix)
	ret c

	;; Y axis case 1
	;;    Y1          Y1+HEIGHT1
	;; IX |-----------|
	;; IY               |-----------|
	;;                  Y2          Y2+HEIGHT2
	;; Y1 + HEIGHT1 - 1 < Y2
	;; Y1 + HEIGHT1 -1 - Y2 < 0
	ld a, ENT_Y(ix)
	add ENT_HEIGHT(ix)
	dec a
	cp ENT_Y(iy)
	ret c

	;; Y axis case 2
	;;                  Y1          Y1+HEIGHT1
	;; IX               |-----------|
	;; IY |-----------|
	;;    Y2          Y2+HEIGHT2
	;; Y2 + HEIGHT2 - 1 < Y1
	;; Y2 + HEIGHT2 - 1 - Y1 < 0
	ld a, ENT_Y(iy)
	add ENT_HEIGHT(iy)
	dec a
	cp ENT_Y(ix)
	ret c
	
	ld hl, #entity_table_onCollisionSFX
	call man_entity_entityTableWLookup
	call_hl
	
	push ix   ;; Stores IX on the stack

	push iy   ;;\_ Copy IY to IX
	pop ix    ;;/
	
	ld hl, #entity_table_onCollisionSFX
	call man_entity_entityTableWLookup
	call_hl

	pop ix   ;;- Recovers IX

	ld a, ENT_INFLICTS_DAMAGE_ON(ix)
	and ENT_TYPE_FLAG(iy)
	jr z, __endInflictDamageXToY
	
	ld a, ENT_DAMAGE(iy)
	add ENT_ATTACK(ix)
	ld ENT_DAMAGE(iy), a
	
	__endInflictDamageXToY:
	
	ld a, ENT_INFLICTS_DAMAGE_ON(iy)
	and ENT_TYPE_FLAG(ix)
	jr z, __endInflictDamageYToX

	ld a, ENT_DAMAGE(ix)
	add ENT_ATTACK(iy)
	ld ENT_DAMAGE(ix), a
	
	__endInflictDamageYToX:


	;cpctm_setBorder_asm #HW_RED

	ret




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Checks the boundary collisions (left, right, top,
;; bottom) and restores the entity position. Before,
;; calls the boundary collision behavior of the entity
;; ----------------------------------------------------
;; PARAMS
;; IX = Entity to check
;; ----------------------------------------------------
;; RETURNS
;; E = collision flags
;; ----------------------------------------------------
;; DESTROYS
;; A, D
;; ----------------------------------------------------
sys_collision_checkBoundaryCollisions::
	ld e, #0

	;; Left boundary
	;; X <= SCREEN_START
	;; X - SCREEN_WIDTH >= 0
	ld d, #SCREEN_WIDTH
	ld a, ENT_X(ix)
	cp d
	jr c, endif_leftBoundary
		set BOUNDARY_COLLISION_LEFT_BIT, e
		ld ENT_X(ix), #0   ;; Sets the entity position to the margin left
		call sys_physics_restoreWorldX
		jr endif_rightBoundary
	endif_leftBoundary:


	;; Right boundary (ONLY CHECKS IF LEFT ISN'T TRUE)
	;; X + ENT_WIDTH >= SCREEN_WIDTH
	;; X + ENT_WIDTH - SCREEN_WIDTH >= 0
	add ENT_WIDTH(ix)  ;; a = ENT_X + ENT_WIDTH
	cp d
	jr c, endif_rightBoundary
		set BOUNDARY_COLLISION_RIGHT_BIT, e
		ld a, #SCREEN_WIDTH ;;\
		sub ENT_WIDTH(ix)   ;; - Sets the entity position to the margin right
		ld ENT_X(ix), a     ;;/
		call sys_physics_restoreWorldX
	endif_rightBoundary:


	;; Top boundary
	;; Y >= SCREEN_HEIGHT
	;; Y - SCREEN_HEIGHT >= 0
	ld d, #SCREEN_HEIGHT
	ld a, ENT_Y(ix)
	cp d
	jr c, endif_topBoundary
		set BOUNDARY_COLLISION_TOP_BIT, e
		ld ENT_Y(ix), #0  ;; Sets the entity position to the margin top
		call sys_physics_restoreWorldY
		jr endif_bottomBoundary
	endif_topBoundary:


	;; Bottom boundary (ONLY CHECKS IF TOP ISN'T TRUE)
	;; Y + ENT_HEIGHT >= SCREEN_HEIGHT
	;; Y + ENT_HEIGHT - SCREEN_HEIGHT >= 0
	add ENT_HEIGHT(ix)  ;; a = ENT_Y + ENT_HEIGHT
	cp d
	jr c, endif_bottomBoundary
		set BOUNDARY_COLLISION_BOTTOM_BIT, e
		ld a, #SCREEN_HEIGHT  ;;\
		sub ENT_HEIGHT(ix)    ;; - Sets the entity position to the margin bottom
		ld ENT_Y(ix), a       ;;/
		call sys_physics_restoreWorldY
	endif_bottomBoundary:

	;; If E=0, no collisions, ret
	ld a, e
	or a
	ret z
	
	;; Jumps to the boundary collision behavior of the entity
	ld hl, #man_behaviors_collision_onBoundaryTable
	call man_entity_entityTableWLookup
	jp (hl)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Checks tileset collisions with the player, and acts
;; acordingly
;; WARNING: ASUMES TILEMAP WIDTH OF 40
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity to check collisions
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; AF, BC, HL, DE, IX
;; ----------------------------------------------------
sys_collision_tileCollision::

    ld a, ENT_VY(ix)
    bit #7, a                               ;; Check if going down
    ret nz                                   ;; If going up return
    ;; Calculate the position of the last tile
    ld b, ENT_X(ix) 
    ;;Calculate modulus x
    ld a, b
    modulus TILE_WIDTH_IN_BYTES
    ld (_modulus_x), a

    ld a, ENT_Y(ix)                         ;; A: Y position
    add ENT_HEIGHT(ix)
    ld d, a
    modulus TILE_HEIGHT                       ;; Calculate the modulus of the tile height
    ld (_modulus_y), a                        ;; Save modulus
    ld a, d

    ld de, #game_tilemapCenter                ;; DE: Start of the tile map memory position
    call sys_collision_calculatePosition


    ;;Check feet collisions

    ;; Set left foot flag on c
    ld c, #IA_FLAGS_BITS                ;; Set flags (no collision by default)

    ld a, (hl)
    push hl
    and #TILE_COLLISIONABLE
    jr z, _not_first
        res #IA_LEFT_FOOT_BIT, c
        call _feet_collides
_not_first:
    pop hl
    push bc         ;; Save feet flags

    ld a, ENT_WIDTH(ix)

    shiftr_a TOTAL_TILE_DIVISION_DISPLACEMENTS_X
    or a 
    jr z, _dont_check_next_tile

    ld c, a
    ld b, #0

    ;;Check offset with the modulus
_modulus_x = .+1
    ld a, #0
    or a
    jp nz, _mod_not_zero                ;; If Modulus x is zero
    dec c                               ;; check mimnus 1 tile, because it is not half ocupying tiles
_mod_not_zero:

    add hl, bc


_continue:            ;; Checks next for feet flags in entities with 1 tile width, watching next tile to the right.
    pop bc          ;; Restore feet flags

    ld a, (hl)
    ; push hl
    and #TILE_COLLISIONABLE
    jr z, _not_second
        res #IA_RIGHT_FOOT_BIT, c
        call _feet_collides
        ; jr _dont_check_next_tile
_not_second:
    push bc

_dont_check_next_tile:
    pop bc
    ;; Reset boolean
    xor a
    ld (_tile_collides), a
    
    ;; Reset flags and return
    ld a, ENT_IA_DATA(ix)
    or c
    ld ENT_IA_DATA(ix), a
    ret

_feet_collides:                                ;; HAS TO BE AVOVE _tile_collides
    ld ENT_JUMP_STATUS(ix), #JUMP_STATUS_QUIET ;; Activate the jump. HAS TO BE AVOVE _tile_collides
_modulus_y = .+1
    ld b, #0                                ;; AUTOMODIFIABLE load the y modulus
    
;;    
;;  B: Modulus Y
;; 
_tile_collides = .+1
    ld a, #0                                ;; AUTOMODIFIABLE Boolean to check if already corrected entity
    or a
    ret nz
    
    ld a, ENT_Y(ix)
    sub b
    ld ENT_Y(ix), a                         ;; Set the new position
	call sys_physics_restoreWorldY
    ld ENT_VY(ix), #0                       ;; Set speed y to 0
    ld a, #1
    ld (_tile_collides), a
	
	;; If is the player we set the we have to indicate it
	bit ENTITY_TYPE_FLAG_PLAYER_BIT, ENT_TYPE_FLAG(ix)
	ret z

	ld hl, #__playerTileCollision
	set 1, (hl)
	
	ret




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Calculates tile memory position based on entity coordinates
;; WARNING: ASUMES TILEMAP WIDTH OF 40
;; ----------------------------------------------------
;; PARAMS
;; B: X position
;; A: Y position
;; DE: Starting memory position of the tilemap
;; ----------------------------------------------------
;; RETURNS
;; HL: Memory position of the tile
;; ----------------------------------------------------
;; DESTROYS
;; AF, BC, HL, DE
;; ----------------------------------------------------
sys_collision_calculatePosition::
    push de

    shiftl_a TOTAL_TILE_DIVISION_DISPLACEMENTS_Y

    multiply_a_by_total_tiles_to_hl

    ;; Add the x position
    xor a
    ld d, a                                 ;; Set d to 0*
    ld a, b                                 ;; A = x position   

    shiftl_a TOTAL_TILE_DIVISION_DISPLACEMENTS_X

    ld e, a                                 ;; D was put to 0 before*
    add hl, de                              ;; HL = 40y + x         

    pop bc                                  ;; BC = Memory position of the tilemap
    add hl, bc                              ;; Add the offset
    ret

