.include "cpctelera.h.s"
.include "utils.h.s"
.include "cfg.h.s"
.include "man/game.h.s"
.include "man/entity.h.s"
.include "man/interruption.h.s"
.include "sys/render.h.s"
.include "assets/assets.h.s"
.include "helper/video.h.s"
.include "lib/ui.h.s"
.include "ui/hud.h.s"
.include "lib/macros.h.s"
.include "buffers.h.s"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; RENDER SYSTEM
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;     RENDER_QUEUES      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        ;;
;;- Pointer to the screen ;;
;;                        ;;
;;- Height                ;;
;;                        ;;
;;- Width                 ;;
;;                        ;;
;;- Pointer to the sprite ;;
;;                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        ;;
;;- Pointer to the screen ;;
;;                        ;;
;;- Height                ;;
;;                        ;;
;;- Width                 ;;
;;                        ;;
;;- Pointer to the sprite ;;
;;                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        ;;
;;          ...           ;;
;;                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        ;;
;;        0x0000          ;;
;;                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;


OffsetInit
DefineOffset SYS_RENDER_QUEUE_ELEMENT_SCREEN_L  , 1
DefineOffset SYS_RENDER_QUEUE_ELEMENT_SCREEN_H  , 1
DefineOffset SYS_RENDER_QUEUE_ELEMENT_HEIGHT    , 1
DefineOffset SYS_RENDER_QUEUE_ELEMENT_WIDTH     , 1
DefineOffset SYS_RENDER_QUEUE_ELEMENT_SPRITE_L  , 1
DefineOffset SYS_RENDER_QUEUE_ELEMENT_SPRITE_H  , 1
DefineOffset SIZEOF_SYS_RENDER_QUEUE_ELEMENT    , 0

SIZEOF_SYS_RENDER_QUEUE = 25

sys_render_eraseQueueNextElem::
	.dw sys_render_eraseQueue

sys_render_drawQueueNextElem::
	.dw sys_render_drawQueue

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Clears the erase queue
;; ----------------------------------------------------
;; PARAMS
;; HL = Queue to render
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;;  HL, A, BC, DE
;; ----------------------------------------------------
;; TIME
;; 10 + 143*QUEUE_SIZE
;;----------------------------------------------------
sys_render_renderQueue::
	
	sys_render_eraseQueueLoop:
		ld e, (hl)                             ;;[7]
		inc hl                                 ;;[6]
		ld d, (hl)                             ;;[7]

		ld a, e                                ;;[4]
		or d                                   ;;[4]
		ret z                                  ;;[11/5]

		inc hl                                 ;;[6]
		ld c, (hl)                             ;;[7]
		inc hl                                 ;;[6]
		ld b, (hl)                             ;;[7]
		inc hl                                 ;;[6]
		

		ld a, (hl)                             ;;[7] 
		inc hl                                 ;;[6]
		push hl                                ;;[11]
		ld h, (hl)                             ;;[7]
		ld l, a                                ;;[4]
		
		call cpct_drawSpriteBlended_asm        ;;[17]

		pop hl                                 ;;[10]
		inc hl                                 ;;[6]

	jp sys_render_eraseQueueLoop               ;;[10]


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Clears the erase queue
;; ----------------------------------------------------
;; PARAMS
;; DE = Queue
;; HL = Queue next element
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;;  HL, DE
;; ----------------------------------------------------
sys_render_queueClear::
	ld (hl), e                       ;;\
	inc hl                           ;; |- Sets the begining of the queue as the next element
	ld (hl), d                       ;;/

	ex de, hl                        ;;- Moves the quueue pointer to HL

	ld (hl), #0                      ;;\
	inc hl                           ;; |- Sets the first 2 bytes of the queue to NULL
	ld (hl), #0                      ;;/
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Push the erase data on the queue
;; ----------------------------------------------------
;; PARAMS
;; IX: pointer to the entity
;; HL: nextElement
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;;  AF, HL
;; ----------------------------------------------------
;; TIME = 260
sys_render_queuePush::
	ld d, h
	ld e, l
	ld a, (hl)
	inc hl
	ld h, (hl)
	ld l, a

	
	ld a, ENT_PREV_SCREEN_PTR_L(ix)                 ;;[19]\
	ld (hl), a                                      ;;[7]  |
	inc hl                                          ;;[6]  |_ Stores the screen pointer
	ld a, ENT_PREV_SCREEN_PTR_H(ix)                 ;;[19] |
	ld (hl), a                                      ;;[7]  |
	inc hl                                          ;;[6] /
	
	ld a, ENT_HEIGHT(ix)                            ;;[19]\
	ld (hl), a                                      ;;[7]  |- Stores the entity height
	inc hl                                          ;;[6] /
	
	ld a, ENT_WIDTH(ix)                             ;;[19]\
	ld (hl), a                                      ;;[7]  |- Stores the entity width
	inc hl                                          ;;[6] /
	
	ld a, ENT_LAST_SPRITE_L(ix)                     ;;[19]\
	ld (hl), a                                      ;;[7]  |
	inc hl                                          ;;[6]  |_ Stores the sprite pointer
	ld a, ENT_LAST_SPRITE_H(ix)                     ;;[19] |
	ld (hl), a                                      ;;[7]  |
	inc hl                                          ;;[6] /
	
	ex de, hl
	ld (hl), e                                      ;;[16] - Stores the new next element pointer
	inc hl
	ld (hl), d                                      ;;[16] - Stores the new next element pointer
	ex de ,hl

	ld (hl), #0                                     ;;[10]\
	inc hl                                          ;;[6]  | - Sets the null pointer at the end
	ld (hl), #0                                     ;;[10]/
	
	ret                                             ;;[10]







;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Sets the video mode, border color and pal colors
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;;  AF, BC, DE, HL
;; ----------------------------------------------------
sys_render_init::
	ld c, #0 ;; C = 0 (Mode 0)
	call cpct_setVideoMode_asm
	
	;; SET BORDER
	cpctm_setBorder_asm HW_BLACK

	ld ix, #ui_hud_bg
	call bite_ui_drawComponent
	
	sys_render_actualPalette == .+1
	ld hl, #0x0000
	call man_interruption_setGameplayPalette
	call man_interruption_setHUDPalette

    ;; Draw background
	call man_interruption_waitInterruption4
    call sys_render_renderTiles
	
	call man_interruption_init
	
	ld hl, #_palette_hud
	call man_interruption_setHUDPalette

	ld ix, #ui_hud
	call bite_ui_drawComponent

	ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Renders the entity given
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity pointer
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; AF, BC, DE, HL
;; ----------------------------------------------------
sys_render_renderEntity::
    
    ld a, #RENDER_DRAW|RENDER_BLANK      ;;\
    and ENT_RENDER_FLAGS(ix)             ;; |-If doesn't have the draw or blank flag don't should
	ret z                                ;;/ \do nothing
	
    ;; Check if erase flag activated
    ld a, ENT_RENDER_FLAGS(ix)
    bit #RENDER_ERASE_BIT, a
    jp z, _do_not_erase

    ;; Reset erase bit
    res #RENDER_ERASE_BIT, a
    ld ENT_RENDER_FLAGS(ix), a

	;; Clean previous pos
	ld hl, #sys_render_eraseQueueNextElem
	call sys_render_queuePush

_do_not_erase:
    ; Check if blank flag is reset
    ld a, ENT_RENDER_FLAGS(ix)
    bit #RENDER_BLANK_BIT, a
    ret nz

    ;; Set flags
    set #RENDER_ERASE_BIT, a
    res #RENDER_DRAW_BIT, a         ;; Reset draw flag (already drawed this frame)
    ld ENT_RENDER_FLAGS(ix), a
    
    ;; Render in the new position
	;; b = entity->posY
	ld a, ENT_Y(ix)
	add #SCREEN_OFFSET_Y
	ld b, a
	;; c = entity->posX
	ld a, ENT_X(ix)
	add #SCREEN_OFFSET_X
	ld c, a
	ld de, #0xC000   ;; DE = Pointer to start of the screen
	call cpct_getScreenPtr_asm    ;; Calculate video memory location and return it in HL

	;; Store screen ptr into previous screen to restore it in the next frame
	ld ENT_PREV_SCREEN_PTR_H(ix), h
	ld ENT_PREV_SCREEN_PTR_L(ix), l

    ;; Set last sprite
    ld a, ENT_SPRITE_L(ix)
    ld ENT_LAST_SPRITE_L(ix), a
    ld a, ENT_SPRITE_H(ix)
    ld ENT_LAST_SPRITE_H(ix), a
	
	ld hl, #sys_render_drawQueueNextElem
	call sys_render_queuePush

_do_not_draw:
    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Deletes the sprite (previous sprite) of the entity on IX, on the position of DE
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity pointer
;; DE: Video memory position
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; AF, BC, DE, HL
;; ----------------------------------------------------
sys_render_eraseBlendedSprite::
    ld h, ENT_LAST_SPRITE_H(ix)
    ld l, ENT_LAST_SPRITE_L(ix)
    ld b, ENT_WIDTH(ix)
    ld c, ENT_HEIGHT(ix)
    call cpct_drawSpriteBlended_asm
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Renders all entities
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; All registers
;; ----------------------------------------------------
sys_render_update::

	ld de, #sys_render_eraseQueue
	ld hl, #sys_render_eraseQueueNextElem
	call sys_render_queueClear
	ld de, #sys_render_drawQueue
	ld hl, #sys_render_drawQueueNextElem
	call sys_render_queueClear
	
	ld hl, #sys_render_renderEntity
	ld a, #COMPONENT_L_RENDER
    ld b, #0
	call man_entity_forallMatchingEntities
	
	
	call man_interruption_waitInterruption4
	;cpctm_setBorder_asm HW_GREEN
	ld hl, #sys_render_eraseQueue              ;;[10]
	call sys_render_renderQueue
	ld hl, #sys_render_drawQueue              ;;[10]
	call sys_render_renderQueue
	;cpctm_setBorder_asm HW_BLACK
	
	
	ret


sys_render_updateHUD::
	ld ix, #ui_hudUpdated
	call bite_ui_drawComponent
	ld ix, #ui_hudUpdated
	call bite_ui_componentNode_clearChilds
	ret





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Restores sprite from screen of entity given
;; ----------------------------------------------------
;; PARAMS
;; IX = Pointer to entity
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; HL, AF, DE, BC
;; ----------------------------------------------------
;; DISABLED
;;sys_render_restoreSprite::
;;	;;If the screen ptr H differs from 0 sprite has to be recovered
;;    ld h, ENT_PREV_SCREEN_PTR_H(ix)
;;	xor a
;;	cp h
;;	ret z
;;    ld l, ENT_PREV_SCREEN_PTR_L(ix)
;;    ld d, ENT_SPRITE_H(ix)
;;    ld e, ENT_SPRITE_L(ix)
;;    ld c, ENT_WIDTH(ix)
;;    ld b, ENT_HEIGHT(ix)
;;    call helper_video_swapScreenSprite
;;	ret




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Restores all entity sprites
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; HL, AF, DE, BC, IX
;; ----------------------------------------------------
sys_render_terminate::
;;	call cpct_waitVSYNC_asm
;;	ld hl, #sys_render_restoreSprite
;;	call man_entity_forallEntities
	ret




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Draws the background from the tilemap
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; AF, BC, DE, HL AF’, BC’, DE’, HL’
;; ----------------------------------------------------
sys_render_renderTiles::
	
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; Tilemap top
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ld c, #TILEMAP_TOP_W                    ;; C: Tilemap width
    ld b, #TILEMAP_TOP_H                    ;; B: Tilemap height
    ld de, #TILEMAP_TOP_W                   ;; DE: Tilemap length
    ld hl, #_tile_000                          ;; HL: Tileset pointer
    call cpct_etm_setDrawTilemap4x8_ag_asm     ;; Sets the tileset uset by the tilemap
	
	ld c, #TILEMAP_TOP_X
	ld b, #TILEMAP_TOP_Y
    ld de, #0xC000
	call cpct_getScreenPtr_asm
	
	ld de, #game_tilemapTop
	di
    call cpct_etm_drawTilemap4x8_ag_asm    ;; Draw the tilemap
    ei
	
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; Tilemap margin left
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ld c, #TILEMAP_MARGIN_W                    ;; C: Tilemap width
    ld b, #TILEMAP_MARGIN_H                    ;; B: Tilemap height
    ld de, #TILEMAP_MARGIN_W                   ;; DE: Tilemap length
    ld hl, #_tile_000                          ;; HL: Tileset pointer
    call cpct_etm_setDrawTilemap4x8_ag_asm     ;; Sets the tileset uset by the tilemap
	
	ld c, #TILEMAP_MARGIN_LEFT_X
	ld b, #TILEMAP_MARGIN_LEFT_Y
    ld de, #0xC000
	call cpct_getScreenPtr_asm
	
	ld de, #game_tilemapMarginLeft
	di
    call cpct_etm_drawTilemap4x8_ag_asm    ;; Draw the tilemap
	ei


	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; Tilemap margin left
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ld c, #TILEMAP_MARGIN_W                    ;; C: Tilemap width
    ld b, #TILEMAP_MARGIN_H                    ;; B: Tilemap height
    ld de, #TILEMAP_MARGIN_W                   ;; DE: Tilemap length
    ld hl, #_tile_000                          ;; HL: Tileset pointer
    call cpct_etm_setDrawTilemap4x8_ag_asm     ;; Sets the tileset uset by the tilemap
	
	ld c, #TILEMAP_MARGIN_RIGHT_X
	ld b, #TILEMAP_MARGIN_RIGHT_Y
    ld de, #0xC000
	call cpct_getScreenPtr_asm
	
	ld de, #game_tilemapMarginRight
	di
    call cpct_etm_drawTilemap4x8_ag_asm    ;; Draw the tilemap
	ei
	

	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; Tilemap center
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ld c, #TILEMAP_CENTER_W                    ;; C: Tilemap width
    ld b, #TILEMAP_CENTER_H                    ;; B: Tilemap height
    ld de, #TILEMAP_CENTER_W                   ;; DE: Tilemap length
    ld hl, #_tile_000                          ;; HL: Tileset pointer
    call cpct_etm_setDrawTilemap4x8_ag_asm     ;; Sets the tileset uset by the tilemap
	
	ld c, #TILEMAP_CENTER_X
	ld b, #TILEMAP_CENTER_Y
    ld de, #0xC000
	call cpct_getScreenPtr_asm
	
	ld de, #game_tilemapCenter
	di
    call cpct_etm_drawTilemap4x8_ag_asm    ;; Draw the tilemap
	ei
    ret

