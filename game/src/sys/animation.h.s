.include "lib/macros.h.s"

.globl sys_animation_update
.globl _animation_standing_right
.globl _animation_walk_right
.globl _animation_walk_left
.globl _player_animation_array
.globl _boss2_animation_array
.globl _animation_boss2_standing_right
.globl _player_death_animation
.globl _animation_player_shoot
.globl _animation_indicator
.globl sys_animation_changeAnimation
.globl _bold_animation_array
.globl _bold_standing_right
.globl _bold_death_animation
.globl _boy_animation_array
.globl _boy_death_animation
.globl _boy_standing_right
.globl _girl_standing_right
.globl _girl_animation_array
.globl _girl_death_animation
; .globl _crazy_animation_array
; .globl _crazy_death_animation
; .globl _crazy_standing_right
.globl _bird_animation_array
.globl _animation_bird_standing_right
.globl _animation_bird_death
.globl _animation_boss2_death
.globl _boss1_animation_array
.globl _animation_boss1_death
.globl _animation_boss1_standing_right
.globl _animation_boss4_standing_right
.globl _animation_boss4_death
.globl _boss4_animation_array
.globl _animation_boss3_doing_nothing
.globl _animation_boss3_spawning
.globl _animation_boss3_death
.globl _rat_animation_array
.globl _rat_death_animation
.globl _rat_standing_right

_dont_animate = 0xDEAD

_animation_counter = 0

.macro AnimationID __name
    __name'_ID = _animation_counter
    _animation_counter = _animation_counter + 1
.endm

.macro DefineName __name
    __start = __name
    __field_count = 5
.endm

.macro NextStep
    __field_count = __field_count + 5
.endm

.macro DefineAnimation __name, __sprite, __wait_cycles
DefineName __name
AnimationID __name
__name::
    .dw __sprite
    .db __wait_cycles
.endm


.macro DefineStep __sprite, __wait_cycles
    .dw #__start + __field_count            ;; Last step next step
    .dw __sprite
    .db __wait_cycles
    NextStep
.endm

.macro EndDefine __type
    __pointer_'__type = __start
    .dw #__start
    .macro __type
        .dw #__pointer_'__type
    .endm
.endm

.macro AddType __type
    __type
    .mdelete __type
.endm

.macro DefineAnimationArray __name
    __name::
        AddType AR_WALK_RIGHT
        AddType AR_WALK_LEFT
        AddType AR_UP_RIGHT
        AddType AR_UP_LEFT
        AddType AR_DOWN_RIGHT
        AddType AR_DOWN_LEFT
        AddType AR_STANDING_RIGHT
        AddType AR_STANDING_LEFT
        AddType AR_SHOOTING_STANDING_RIGHT
        AddType AR_SHOOTING_STANDING_LEFT
        AddType AR_SHOOTING_WALK_RIGHT
        AddType AR_SHOOTING_WALK_LEFT
        AddType AR_SHOOTING_UP_RIGHT
        AddType AR_SHOOTING_DOWN_RIGHT
        AddType AR_SHOOTING_UP_LEFT
        AddType AR_SHOOTING_DOWN_LEFT
.endm

OffsetInit
DefineOffset AR_WALK_RIGHT_L, 1
DefineOffset AR_WALK_RIGHT_H, 1
DefineOffset AR_WALK_LEFT_L, 1
DefineOffset AR_WALK_LEFT_H, 1
DefineOffset AR_UP_RIGHT_L, 1
DefineOffset AR_UP_RIGHT_H, 1
DefineOffset AR_UP_LEFT_L, 1
DefineOffset AR_UP_LEFT_H, 1
DefineOffset AR_DOWN_RIGHT_L, 1
DefineOffset AR_DOWN_RIGHT_H, 1
DefineOffset AR_DOWN_LEFT_L, 1
DefineOffset AR_DOWN_LEFT_H, 1
DefineOffset AR_STANDING_RIGHT_L, 1
DefineOffset AR_STANDING_RIGHT_H, 1
DefineOffset AR_STANDING_LEFT_L, 1
DefineOffset AR_STANDING_LEFT_H, 1
DefineOffset AR_SHOOTING_STANDING_RIGHT_L, 1
DefineOffset AR_SHOOTING_STANDING_RIGHT_H, 1
DefineOffset AR_SHOOTING_STANDING_LEFT_L, 1
DefineOffset AR_SHOOTING_STANDING_LEFT_H, 1
DefineOffset AR_SHOOTING_WALK_RIGHT_L, 1
DefineOffset AR_SHOOTING_WALK_RIGHT_H, 1
DefineOffset AR_SHOOTING_WALK_LEFT_L, 1
DefineOffset AR_SHOOTING_WALK_LEFT_H, 1
DefineOffset AR_SHOOTING_UP_RIGHT_L, 1
DefineOffset AR_SHOOTING_UP_RIGHT_H, 1
DefineOffset AR_SHOOTING_DOWN_RIGHT_L, 1
DefineOffset AR_SHOOTING_DOWN_RIGHT_H, 1
DefineOffset AR_SHOOTING_UP_LEFT_L, 1
DefineOffset AR_SHOOTING_UP_LEFT_H, 1
DefineOffset AR_SHOOTING_DOWN_LEFT_L, 1
DefineOffset AR_SHOOTING_DOWN_LEFT_H, 1


_no_animation_array = 0x0000
