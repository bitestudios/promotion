.include "cfg.h.s"
.include "ui/hud.h.s"
.include "lib/ui.h.s"
.include "lib/math.h.s"
.include "lib/opmacros.h.s"
.include "man/entity.h.s"
.include "man/game.h.s"
.include "helper/string.h.s"
.include "cpctelera.h.s"

sys_hud_update::

	call sys_hud_updateTimer

	call sys_hud_updatePlayerHealthBar

	call sys_hud_updatePlayerEnergyBar

	call sys_hud_updateBossHealthBar

	call sys_hud_updateScoreboard
	
	ret


sys_hud_updateBonus::
	call sys_hud_decTimer
	ld a, #MAN_GAME_STATUS_BONUS_COMPLETED
	call z, man_game_setGameStatus
	ld a, (man_game_remainingLives)
	ld b, a
	ld a, #1
	shiftl_a_b
	ld c, a
	ld hl, (game_score)
	add hl, bc
	ld (game_score), hl
	call sys_hud_updateScoreboard
	ret



sys_hud_updateScoreboard::
	
	ld a, (game_scorePrev)
	ld bc, (game_score)

	xor a, c
	jr nz, __updateScoreboardNoEqual
		ld a, (game_scorePrev+1)
		xor a, b
		ret z
	__updateScoreboardNoEqual:

	ld (game_scorePrev), bc
	
	ld hl, #ui_hud_scoreboardDigits
	call bite_ui_componentText_getString
	ld a, #5
	ld bc, (game_score)
	call bite_math_u16ToStr
	
	ld de, #ui_hud_scoreboardDigits
	ld ix, #ui_hudUpdated
	call bite_ui_componentNode_addChild
	
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Pushes the timer to draw decrements
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; Z enabled if timer is zero
;; ----------------------------------------------------
;; DESTROYS
;; All registers
;; ----------------------------------------------------
sys_hud_decTimer::
	ld hl, #ui_hud_timer
	call bite_ui_componentText_getString
	ld bc, (man_game_timer)
	ld a, #3
	call bite_math_u16ToStr
	
	ld de, #ui_hud_timer
	ld ix, #ui_hudUpdated
	call bite_ui_componentNode_addChild
	
	ld hl, (man_game_timer)
	ld a, h
	or l
	ret z

	dec hl
	ld (man_game_timer), hl
	or #1                     ;;- Resets de Z flag
	ret


sys_hud_cyclesCounter::
	.db 1

sys_hud_updateTimer::
	ld a, (man_game_timerStopped)
	or a
	ret nz
	ld a, (sys_hud_cyclesCounter)
	dec a
	ld (sys_hud_cyclesCounter), a
	ret nz
	ld a, #GAMEPLAY_CYCLES
	ld (sys_hud_cyclesCounter), a


	call sys_hud_decTimer
	ret nz
	
	call man_entity_getPlayer
	ld a, #8
	ld ENT_DAMAGE(ix), a
	
	ret
	





sys_hud_updatePlayerHealthBar::

	call man_entity_getPlayer
	ld hl, #ui_hud_healthBarFillPlayer
	ld bc, #UI_COMPONENT_WIDTH
	add hl, bc
	ld a, ENT_HEALTH(ix)
	cp (hl)
	ret z
	ld (hl), a
	
	ld de, #ui_hud_healthBarPlayer
	ld ix, #ui_hudUpdated
	call bite_ui_componentNode_addChild

	ret






sys_hud_updatePlayerEnergyBar::
	ld hl, #ui_hud_energyBarFillPlayer
	ld bc, #UI_COMPONENT_WIDTH
	add hl, bc
	ld a, (man_game_playerEnergy)
	cp (hl)
	ret z
	ld (hl), a
	
	ld de, #ui_hud_energyBarPlayer
	ld ix, #ui_hudUpdated
	call bite_ui_componentNode_addChild

	ret





sys_hud_updateBossHealthBar::
	
	call man_entity_getBoss
	ld hl, #ui_hud_healthBarFillBoss
	ld bc, #UI_COMPONENT_WIDTH
	add hl, bc

	ld a, ENT_HEALTH(iy)          ;;-Gets the boss health

	ld e, a                       ;;\_Stores the health on E adn D also

	ld d, #0xFF                   ;;-Initializes d to -1

	or a                          ;;\_ If health is 0 goes directly to set the width
	jr z, __setWidth              ;;/

	shiftr_a 3                    ;;-Divides a between 8 to get the number of bars

	ld d, a                       ;;\_ Stores the result on D and recovers the previous from E
	ld a, e                       ;;/

	modulus 8                     ;;- Calculates the modulus between 8 to get the bar size

	jr nz, __setWidth             ;;\_ If the modulus is 0 must be setted to 8
		ld a, #8                  ;;/
		dec d
	__setWidth:
	
	inc d                     ;;- If modulus is not 0 increment the number of bars

	cp (hl)
	ret z
	ld (hl), a

	ld a, d

	ld hl, #ui_hud_numberOfBars
	call bite_ui_componentText_getString
	digitToChar
	ld (hl), a
	
	ld de, #ui_hud_healthBarBoss
	ld ix, #ui_hudUpdated
	call bite_ui_componentNode_addChild
	
	ret

