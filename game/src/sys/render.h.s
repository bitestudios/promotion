;;;;;;;;;;;;;;;;;;;;;;;;;
;; PUBLIC FUNCTIONS
;;;;;;;;;;;;;;;;;;;;;;;;;
.globl sys_render_init
.globl sys_render_update
.globl sys_render_updateHUD
.globl sys_render_terminate
.globl sys_render_eraseBlendedSprite
.globl sys_render_actualPalette

