.include "man/entity.h.s"
.include "sys/ia.h.s"
.include "cpctelera.h.s"
.include "assets/assets.h.s"
.include "man/game.h.s"
.include "sys/collision.h.s"
.include "lib/opmacros.h.s"
.include "cfg.h.s"
.include "man/sfx.h.s"
.include "factory/factory.h.s"
.include "sys/animation.h.s"
.include "utils.h.s"
.include "sys/physics.h.s"


;;
;;STATE DIAGRAM FUNCTIONS
;;

;;Left to right, shoot contrary fase
MoveToState _boss1_s0, 5, _boss1_s1, _no_other_transitions, 40
ShootRightState _boss1_s1, _boss1_s2, _no_other_transitions, 20
MoveToState _boss1_s2, 55, _boss1_s3, _no_other_transitions, 40
ShootLeftState _boss1_s3, _boss1_s4, _no_other_transitions, 20

MoveToState _boss1_s4, 5, _boss1_s5, _no_other_transitions, 40
ShootRightState _boss1_s5, _boss1_s6, _no_other_transitions, 20
MoveToState _boss1_s6, 55, _boss1_s7, _no_other_transitions, 40
ShootLeftState _boss1_s7, _boss1_s8, _no_other_transitions, 20

;; Center left right. Shoot outwards
MoveToState _boss1_s8, 20, _boss1_s9, _no_other_transitions, 40
ShootLeftState _boss1_s9, _boss1_s10, _no_other_transitions, 20
MoveToState _boss1_s10, 38, _boss1_s11, _no_other_transitions, 40
ShootRightState _boss1_s11, _boss1_s16, _no_other_transitions, 20

; MoveToState _boss1_s12, 20, _boss1_s13, _no_other_transitions, 40
; ShootLeftState _boss1_s13, _boss1_s14, _no_other_transitions, 20
; MoveToState _boss1_s14, 38, _boss1_s15, _no_other_transitions, 40
; ShootRightState _boss1_s15, _boss1_s16, _no_other_transitions, 20

;; Go to center and shoot left to right
MoveToState _boss1_s16, 27, _boss1_s17, _no_other_transitions, 40
ShootRightState _boss1_s17, _boss1_s18, _no_other_transitions, 20
ShootLeftState _boss1_s18, _boss1_s19, _no_other_transitions, 20
ShootRightState _boss1_s19, _boss1_s20, _no_other_transitions, 20
ShootLeftState _boss1_s20, _boss1_s25, _no_other_transitions, 40
; ShootRightState _boss1_s21, _boss1_s22, _no_other_transitions, 20
; ShootLeftState _boss1_s22, _boss1_s23, _no_other_transitions, 20
; ShootRightState _boss1_s23, _boss1_s24, _no_other_transitions, 20
; ShootLeftState _boss1_s24, _boss1_s25, _no_other_transitions, 40

ShootRightState _boss1_s25, _boss1_s26, _no_other_transitions, 10
ShootLeftState _boss1_s26, _boss1_s27, _no_other_transitions, 10
ShootRightState _boss1_s27, _boss1_s28, _no_other_transitions, 10
ShootLeftState _boss1_s28, _boss1_s29, _no_other_transitions, 10
ShootRightState _boss1_s29, _boss1_s30, _no_other_transitions, 10
ShootLeftState _boss1_s30, _boss1_s1, _no_other_transitions, 40
; ShootRightState _boss1_s31, _boss1_s32, _no_other_transitions, 10
; ShootLeftState _boss1_s32, _boss1_s0, _no_other_transitions, 40



;; SECOND BOSS.
JumpToPlayerState _boss2_s0, _boss2_s1, _no_other_transitions, 40
ShootState _boss2_s1, _boss2_s2, _stop_jumping, 20
JumpToPlayerState _boss2_s2, _boss2_s3, _no_other_transitions, 40
ShootState _boss2_s3, _boss2_s0, _stop_jumping, 20

;; THIRD BOSS
WaitState _boss3_s0, 3, _boss3_s1, _activate_animation_third_boss_first, 0
SpawnState _boss3_s1, bold_minion, NO_MINION, _boss3_s2, _deactivate_animation_third_boss, 0
WaitState _boss3_s2, 5, _boss3_s3, _spawn_bird, 0
WaitState _boss3_s3, 3, _boss3_s4, _activate_animation_third_boss, 0
SpawnState _boss3_s4, boy_minion, NO_MINION, _boss3_s5, _deactivate_animation_third_boss, 0
WaitState _boss3_s5, 5, _boss3_s6, _no_other_transitions, 0
WaitState _boss3_s6, 3, _boss3_s7, _activate_animation_third_boss, 0
SpawnState _boss3_s7, NO_MINION, girl_minion, _boss3_s8, _deactivate_animation_third_boss, 0
WaitState _boss3_s8, 5, _boss3_s9, _spawn_bird, 0
WaitState _boss3_s9, 3, _boss3_s10, _activate_animation_third_boss, 0
SpawnState _boss3_s10, bold_minion, girl_minion, _boss3_s11, _deactivate_animation_third_boss, 0
WaitState _boss3_s11, 5, _boss3_s12, _no_other_transitions, 0
WaitState _boss3_s12, 3, _boss3_s13, _activate_animation_third_boss, 0
SpawnState _boss3_s13, boy_minion, boy_minion, _boss3_s14, _deactivate_animation_third_boss, 0
WaitState _boss3_s14, 5, _boss3_s15, _no_other_transitions, 0
WaitState _boss3_s15, 3, _boss3_s1, _activate_animation_third_boss, 0

;; FOURTH BOSS
MoveToPlayerState _boss4_s0, _boss4_s1, _boss_4_rage, 0
JumpState _boss4_s1, _boss4_s2, _boss_4_rage, 40
ShootToPlayerState _boss4_s2, _boss4_s3, _boss_4_rage, 20

MoveToPlayerState _boss4_s3, _boss4_s4, _boss_4_rage, 0
JumpState _boss4_s4, _boss4_s5, _boss_4_rage, 40
ShootToPlayerState _boss4_s5, _boss4_s6, _boss_4_rage, 20

MoveToState _boss4_s6, 27, _boss4_s7, _no_other_transitions, 40
ShootRightState _boss4_s7, _boss4_s8, _no_other_transitions, 15
ShootLeftState _boss4_s8, _boss4_s0, _no_other_transitions, 40



WanderState _wandering_minion_0, _no_other_transitions, 10

WanderState _crazy_minion_0, _player_on_platform, 1
MoveToPlayerState _crazy_minion_1, _crazy_minion_1, _no_other_transitions, 15

WanderState _shooting_minion_0, _player_on_same_height, 1
ShootToPlayerState _shooting_minion_1, _shooting_minion_2, _no_other_transitions, 0
WaitState _shooting_minion_2, 3, _shooting_minion_0, _no_other_transitions, 0

MoveToPlayerXYState _boomerang_ia_0, _boomerang_ia_1, _no_other_transitions, 1
MoveToBossState _boomerang_ia_1, _boomerang_ia_2, _no_other_transitions, 1
AutoDestroyState _boomerang_ia_2, _no_other_transitions

MoveToRandomXYState _bird_s0, _bird_s1, _no_other_transitions, 20
ShootToPlayerState _bird_s1, _bird_s0, _no_other_transitions, 20
; _wait::
;     ret
; ShootTransition _waitTransition_bs1, _bird_s2, _no_other_transitions
; _bird_s1::
;     .dw #_wait
;     .dw #_waitTransition_bs1
;     .db 64
; ShootTransition _waitTransition_bs2, _bird_s3, _no_other_transitions
; _bird_s2::
;     .dw #_wait
;     .dw #_waitTransition_bs2
;     .db 64
; ShootTransition _waitTransition_bs3, _bird_s0, _no_other_transitions
; _bird_s3::
;     .dw #_wait
;     .dw #_waitTransition_bs3
;     .db 64

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Updates the ia system
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; 
;; ----------------------------------------------------
sys_ia_update::
    ld a, #COMPONENT_L_AI
    ld b, #0
    ld hl, #sys_ia_stateStep
    call man_entity_forallMatchingEntities
    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Performs an action and checks for transitions on the 
;; state diagram.
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with IA to perform the actions
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A, IY
;; ----------------------------------------------------
sys_ia_stateStep::
    ;; Load action and transition functions
    ld a, ENT_IA_STATE_H(ix)
    ld__iyh_a
    ld a, ENT_IA_STATE_L(ix)
    ld__iyl_a

    ld l, S_ACTION_L(iy)
    ld h, S_ACTION_H(iy)
    ld (_action_function), hl
    
    ld l, S_TRANSITION_L(iy)
    ld h, S_TRANSITION_H(iy)
    ld (_transition_function), hl
    
    ;; Check if enought cicles waited
    ld a, ENT_IA_DATA(ix)
    and #IA_COUNTER_BITS
    cp #0
    jp z, _perform_actions

    call _decrease_wait_counter
    ret

_perform_actions:

    ;; Call action and transtion functions
    push iy
    _action_function = .+1
    call #0000                              ;; AUTOMODIFIABLE Action function pointer
    pop iy
    push iy
    _transition_function = .+1
    call #0000                              ;; AUTOMODIFIABLE Transition function pointer
    pop iy
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Performs the move to behaviour for the entity on IX
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with IA to perform the actions
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A, B
;; ----------------------------------------------------
sys_ia_moveTo::
    ;; Compare target to position
    xor a
    ld (_check_if_arrived), a           ;; Reset check arrived field
    ld a, ENT_X(ix)
    sub ENT_IA_TARGET_X(ix)
    ld (_old_sign), a
    jp p, _is_positive
    neg
_is_positive:
    cp #MOVETO_THRESHOLD
    jp nc, _acelerate_to_position ;; >= threshold -> moveto; else -> activate arrive flag
    xor a
    set 0, a
    ld (_check_if_arrived), a
    xor a
    ld ENT_ACCELERATION_X(ix), a
    jr _y_target
_acelerate_to_position:
    ld a, #MOVETO_ACELL
_old_sign = .+1                     ;; AUTOMODIFIABLE Load substraction value to get the direction
    ld b, #0
    bit 7, b
    jp nz, _do_not_negate            ;; IF direction is negative, negate speed
    neg
_do_not_negate:
    ld ENT_ACCELERATION_X(ix), a

_y_target:
    ;; Check if unset
    ld a, ENT_IA_TARGET_Y(ix)
    cp #TARGET_UNSET
    jr nz, _y_target_set

    ;; Check if arrived X
    ld a, (_check_if_arrived)
    or a
    ret z

    ld ENT_IA_ARRIVED(ix), #1
    ret

_y_target_set:
    ;; Compare target to position
    ld a, ENT_Y(ix)
    sub ENT_IA_TARGET_Y(ix)
    ld (_old_sign_y), a
    jp p, _is_positive_y
    neg
_is_positive_y:
    cp #MOVETO_THRESHOLD
    jp nc, _acelerate_to_position_y ;; >= threshold -> moveto; else -> activate arrive flag
    ld a, (_check_if_arrived)
    set 1, a
    ld (_check_if_arrived), a
    xor a
    ld ENT_ACCELERATION_Y(ix), a
    jr _check_if_arrived-1              ;; Cancel the +1
_acelerate_to_position_y:
    ld a, #MOVETO_ACELL
_old_sign_y = .+1                     ;; AUTOMODIFIABLE Load substraction value to get the direction
    ld b, #0
    bit 7, b
    jp nz, _do_not_negate_y            ;; IF direction is negative, negate speed
    neg
_do_not_negate_y:
    ld ENT_ACCELERATION_Y(ix), a

_check_if_arrived = .+1
    ld a, #0x00
    cp #3 ;; 0011
    ret nz
    ld ENT_IA_ARRIVED(ix), #1
    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Moves left or right until it reaches a pit or screen border
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with IA to perform the actions
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A, B
;; ----------------------------------------------------
sys_ia_wander::
    ld a, ENT_FACING_DIR(ix)

    ;; Check if arrived to a border
    ld b, a         ;; Save value
    ld a, ENT_IA_DATA(ix)
    bit #IA_LEFT_FOOT_BIT, a
    jr z, _left_nothing

    ;; Set facing direction to the left
    ld b, #ENTITY_FACING_RIGHT
    jr _wander_movement

    ; ld ENT_VX(ix), #0

_left_nothing:
    bit #IA_RIGHT_FOOT_BIT, a
    jr z, _wander_movement

    ;; Set facing direction to the right
    ld b, #ENTITY_FACING_LEFT

    ; ld ENT_VX(ix), #0

_wander_movement:
    ld a, b                 ;; Restore facing direction value
    or a
    jp nz, _going_left

    ld a, #MOVETO_ACELL
    jr _save_aceleration

_going_left:
    ld a, #MOVETO_ACELL
    neg

_save_aceleration:
    ld ENT_ACCELERATION_X(ix), a

    ;; Reset flag bits
    ld a, ENT_IA_DATA(ix)
    and #IA_COUNTER_BITS
    ld ENT_IA_DATA(ix), a
    ret


    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Function to wait decrease the wait counter
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with IA to perform the actions
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A, BC
;; ----------------------------------------------------
_decrease_wait_counter::
    ;; Decrement wait cicles counter
    ld a, ENT_IA_DATA(ix)
    ld b, a
    and #IA_COUNTER_BITS
    dec a
    ld c, a
    ld a, b
    and #IA_FLAGS_BITS
    or c
    ld ENT_IA_DATA(ix), a
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Function to change the state
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with IA to perform the actions
;; HL: Pointer to the new state
;; A : Counter of wait cicles until next state
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; 
;; ----------------------------------------------------
_transition_happens::
    ;; Reset counter and change state
    ld ENT_IA_DATA(ix), a
    ld ENT_IA_STATE_H(ix), h
    ld ENT_IA_STATE_L(ix), l
ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Function to change the state of a move_to behaviour
;; ----------------------------------------------------
;; PARAMS
;; IY: Animation array
;; IX: Entity with IA to perform the actions
;; HL: Pointer to the oter_transition function
;; BC: Default next state
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; 
;; ----------------------------------------------------
_move_to_transition::
    ;; Check if arrived
    ld a, ENT_IA_ARRIVED(ix)
    or a
    ret z

    ;;Reset arrived flag
    xor a
    ld ENT_IA_ARRIVED(ix), a

    ld (_function_to_call), hl

    ld ENT_IA_TARGET_X(ix), #TARGET_UNSET       ;; Reset target field
    res #COMPONENT_H_JUMP_BIT, ENT_COMPONENTS_H(ix) ;; Reset jump

    push bc
_function_to_call = .+1
    call 0x0000  ;; AUTOMODIFIABLE In case there are other states to jump

    ;; Change state
    ; ld h, b
    ; ld l, c
    pop hl

    ld a, S_WAIT_CICLES(iy)
    call _transition_happens
    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Empty auxiliar function, when there are no other transitions to check
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; 
;; ----------------------------------------------------
_no_other_transitions::
    ret

_minion_counter::
    .db #0


;; Only for use the first time, to reset the minion counters
_activate_animation_third_boss_first::
    xor a
    ld (_minion_counter), a
    ld (_bird_counter), a
    call _activate_animation_third_boss
    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Empty auxiliar function, activates spawning animation for the third boss
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; 
;; ----------------------------------------------------
_activate_animation_third_boss::
    push iy

    ld a, (_minion_counter)
    cp #THIRD_BOSS_MAX_MINIONS
    jr nc, _max_entites_reached

    ;; Reset bird spawned boolean
    xor a
    ld (_bird_spawned), a

    call man_entity_getBoss
    ld hl, #_animation_boss3_spawning
    ld ENT_CURRENT_ANIMATION_H(iy), h
    ld ENT_CURRENT_ANIMATION_L(iy), l
    ld ENT_ANIMATION_COUNTER(iy), #0
    jr _aat_continue

_max_entites_reached:
    xor a
    ld (_waiter_counter), a             ;; Reset counter, until it is posible to spawn

_aat_continue:

    pop iy
    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Empty auxiliar function, deactivates spawning animation for the third boss
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; 
;; ----------------------------------------------------
_deactivate_animation_third_boss::
    push iy

    ;; Change current animation
    call man_entity_getBoss
    ld hl, #_animation_boss3_doing_nothing
    ld ENT_CURRENT_ANIMATION_H(iy), h
    ld ENT_CURRENT_ANIMATION_L(iy), l
    ld ENT_ANIMATION_COUNTER(iy), #0

    pop iy

    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Empty auxiliar function, when there are no other transitions to check
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; 
;; ----------------------------------------------------
_boss_4_rage::
    ld a, ENT_HEALTH(ix)
    cp #FOURTH_BOSS_RAGE_LIFE_VALUE
    jr nc, _has_more_life
    ld a, (_already_raged_boss_4)
    or a
    ret nz

    ;; Boss laugth
    call man_sfx_playLaugh

    ;; Spawn boss 1
    push ix
    call factory_createBoss1
    ld a, h
    ld__ixh_a
    ld a, l
    ld__ixl_a

    ld ENT_X(ix), #FOURTH_BOSS_HELPER_BOSS_SPAWN_X
    ld ENT_Y(ix), #FOURTH_BOSS_HELPER_BOSS_SPAWN_Y
    ld ENT_HEALTH(ix), #FOURTH_BOSS_HELPER_BOSS_HEALTH
    ld ENT_TYPE_FLAG(ix), #ENTITY_TYPE_FLAG_MINION

    ;; Make boss wait whail laughs
    pop ix
    ld ENT_IA_DATA(ix), #FOURTH_BOSS_LAUGHT_WAIT
    ld a, #1
    ld (_already_raged_boss_4), a           ;; Set flag
    ret

_has_more_life:
    xor a                           ;; Reset rage boolean
    ld (_already_raged_boss_4), a
    ret


_already_raged_boss_4:
    .db #0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Empty auxiliar function, points the entity IX to player
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; 
;; ----------------------------------------------------
_shoot_to_player::
    set COMPONENT_L_SHOOTING_BIT, ENT_COMPONENTS_L(ix)  ;; Activate shoot flag
    ;; Set facing direction to the player, so then shoots at that direction
    push ix     ;; change entity pointer to iy to load player on ix
    pop iy
    call man_entity_getPlayer
    ld a, ENT_X(iy)
    sub ENT_X(ix)
    jp m, _negative_value
        ld ENT_FACING_DIR(iy), #1
        push iy
        pop ix
        ret
_negative_value:
        ld ENT_FACING_DIR(iy), #0
    push iy
    pop ix
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Stops jumping, when there are no other transitions to check
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; 
;; ----------------------------------------------------
_stop_jumping::
    res #COMPONENT_H_JUMP_BIT, ENT_COMPONENTS_H(ix)
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; transition for the crazy minion. Transitions if the player is on top of a platform and in the same height
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with this IA
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; 
;; ----------------------------------------------------
_player_on_platform::
    ld a, S_WAIT_CICLES(iy)
    ld (_wait_cicles), a
    push ix
    ; pop iy
    call man_entity_getPlayer
    push ix
    pop iy
    pop ix

    ld a, ENT_X(ix)
    sub ENT_X(iy)
    absA

    cp #CRAZY_DETECTION_RADIOUS         ;; Check if X position is close enought
    ret nc

    ld a, ENT_Y(ix)
    sub ENT_Y(iy)
    absA

    cp #CRAZY_HEIGHT_THRESHOLD
    ret nc

    ld a, ENT_IA_DATA(iy)               ;; Get foot flags of the player
    and #IA_FLAGS_BITS
    ret z                               ;; If player is not on top of a platform do nothing

    ;; Restore entity pointer on IX
    ; push iy
    ; pop ix

    ;; Set IA target unset value and call for transition
    ld ENT_IA_TARGET_X(ix), #TARGET_UNSET
    ld hl, #_crazy_minion_1
_wait_cicles = .+1
    ld a, #0x00                         ;; AUTOMODIFIABLE save load cicles
    call _transition_happens
    ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; transition for the shooting minion. Transitions if the player is at the same height
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with this IA
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; 
;; ----------------------------------------------------
_player_on_same_height::
    ld a, S_WAIT_CICLES(iy)
    ld (_wait_cicles), a
    push ix
    call man_entity_getPlayer
    push ix
    pop iy
    pop ix

    ld a, ENT_Y(ix)
    sub ENT_Y(iy)
    absA

    cp #CRAZY_HEIGHT_THRESHOLD
    ret nc

    ;; Stop the movement of the minion
    ld ENT_VX(ix), #0
    ld ENT_ACCELERATION_X(ix), #0

    ;; Set IA target unset value and call for transition
    ld ENT_IA_TARGET_X(ix), #TARGET_UNSET
    ld hl, #_shooting_minion_1
_wait_cicles = .+1
    ld a, #0x00                         ;; AUTOMODIFIABLE save load cicles
    call _transition_happens
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; spawns a bird for the 3 boss
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with this IA
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; 
;; ----------------------------------------------------
_spawn_bird::
    push ix
    push iy

    ;; Check if already spawned in this state
    ld a, (_bird_spawned)
    cp #1
    jr z, _sb_end

    ;; Check if in rage fase
    call man_entity_getBoss
    ld a, ENT_HEALTH(iy)
    cp #THIRD_BOSS_RAGE_LIFE_VALUE
    jr nc, _sb_end

    ;; Check number of birds on screen
    ld a, (_bird_counter)
    cp #THIRD_BOSS_MAX_BIRDS_ALLOWED
    jr nc, _sb_end

    ;; Set spawned boolean
    ld a, #1
    ld (_bird_spawned), a

    inc a
    ld (_bird_counter), a

    ld hl, #bird_minion
    call man_entity_createEntity
    ld a, h
    ld__ixh_a
    ld a, l
    ld__ixl_a

    ;; Set points
    ld ENT_POINTS_L(ix), #0
    ld ENT_POINTS_L(ix), #POINTS_THIRD_BOSS_MINIONS

    ld ENT_X(ix), #THIRD_BOSS_BIRD_SPAWN_POSITION_X
    call sys_physics_restoreWorldX
    ld ENT_Y(ix), #THIRD_BOSS_BIRD_SPAWN_POSITION_Y
    call sys_physics_restoreWorldY

_sb_end:
    pop iy
    pop ix
    ret

_bird_counter::
    .db 0

_bird_spawned::
    .db 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; moves IX entity to player in X axis
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with this IA
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; 
;; ----------------------------------------------------
_move_to_player::
    ld a, ENT_IA_TARGET_X(ix)
    cp #TARGET_UNSET
    jr nz, _already_set_mtpl
    push ix
    call man_entity_getPlayer
    ld a, ENT_X(ix)
    pop ix
    ld ENT_IA_TARGET_X(ix), a

    ;; Check if valid position A = ENT_IA_TARGET_X
    ld b, a
    ld a, #SCREEN_WIDTH
    sub ENT_WIDTH(ix)
    cp b
    jr nc, _already_set_mtpl
        ld ENT_IA_TARGET_X(ix), a
; _x_ok:


_already_set_mtpl:
    call sys_ia_moveTo
    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; moves IX entity to player in both axis
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with this IA
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; 
;; ----------------------------------------------------
_move_to_playerXY::
    ld a, ENT_IA_TARGET_X(ix)
    cp #TARGET_UNSET
    jr nz, _already_set_mtpl_XY
    push ix

    call man_entity_getPlayer
    ld a, ENT_X(ix)
    ld b, a
    ld a, ENT_Y(ix)

    pop ix
    ld ENT_IA_TARGET_Y(ix), a
    ld a, b
    ld ENT_IA_TARGET_X(ix), a

_already_set_mtpl_XY:
    call sys_ia_moveTo
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; moves IX entity to player
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with this IA
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A, IY, ...
;; ----------------------------------------------------
_move_to_bossXY::
    call man_entity_getBoss
    ld a, ENT_X(iy)
    ld ENT_IA_TARGET_X(ix), a
    ld a, ENT_Y(iy)
    ld ENT_IA_TARGET_Y(ix), a

_already_set_mtbss:
    call sys_ia_moveTo
    ret

X_MAX_NUMBER_MASK = 0b00111111      ;; Max number is 63
Y_MAX_NUMBER_MASK = 0b01111111      ;; Max number is 127

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; moves IX entity to player
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with this IA
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A, IY, ...
;; ----------------------------------------------------
_move_to_randomXY::
    push ix
    call man_entity_getPlayer
    push ix
    pop iy
    pop ix

    ;; Check if already set target
    ld a, ENT_IA_TARGET_X(ix)
    cp #TARGET_UNSET
    jp nz, _mtrxy_already_set

    ;; Get random X target value
    ld l, ENT_X(iy)             ;; Use player position as entropy for the function
    call cpct_getRandom_lcg_u8_asm
    and #X_MAX_NUMBER_MASK      ;; Max number is 63
    sub #SPRITE_BIRD_WIDTH      ;; Substract the width of the bird
    jp p, _already_valid_x
        ld a, #0                ;; If the result is negative, just make it zero
_already_valid_x:
    ld ENT_IA_TARGET_X(ix), a
    
    
    ;; Get random Y target value
    ld l, ENT_Y(iy)             ;; Use player position as entropy for the function
    call cpct_getRandom_lcg_u8_asm
    and #X_MAX_NUMBER_MASK      ;; Max number is 63
    sub #SPRITE_BIRD_HEIGHT + 7 ;; Substract the width of the bird, plus the extra numbers
    jp p, _already_valid_y
        ld a, #0                    ;; If the result is negative, just make it zero
_already_valid_y:
    ld ENT_IA_TARGET_Y(ix), a

_mtrxy_already_set:
    call sys_ia_moveTo
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; makes IX entity to jump, moving in a direction
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with this IA
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A, IY, ...
;; ----------------------------------------------------
_jump_move::
    set #COMPONENT_H_JUMP_BIT, ENT_COMPONENTS_H(ix)    ;; Set the component for the entity to jump

    ;; Check if already set target
    ld a, ENT_IA_TARGET_X(ix)
    cp #TARGET_UNSET
    jp nz, _jm_already_set

    ;; Check relative position with player
    ld a, ENT_X(ix)
    push ix
    call man_entity_getPlayer
    ld b, #SECOND_BOSS_JUMP_MOVEMENT
    cp ENT_X(ix)
    jr c, _player_is_right
        ld a, b
        neg
        ld b, a
    ; jr _set_target
_player_is_right:

; _set_target:
    pop ix
    ;; Set the target
    ld a, ENT_X(ix)
    add b

    ;; Check if its outside of the screen
    cp #SCREEN_WIDTH - SPRITE_BOSS2_WIDTH
    jr c, _everything_correct
    sub b
    sub b

_everything_correct:
    ld ENT_IA_TARGET_X(ix), a

_jm_already_set:
    ;; Call moveto behaviour
    call sys_ia_moveTo
    ret


_shoot_right::
    ld ENT_FACING_DIR(ix), #0
	set COMPONENT_L_SHOOTING_BIT, ENT_COMPONENTS_L(ix)
    ret

_shoot_general::
    set COMPONENT_L_SHOOTING_BIT, ENT_COMPONENTS_L(ix)
    ret

_shoot_left::
    ld ENT_FACING_DIR(ix), #1
	set COMPONENT_L_SHOOTING_BIT, ENT_COMPONENTS_L(ix)
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; makes IX entity to jump. WARNING Only works with 1 entity
;; on screen due to harcoded counter
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with this IA
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A, IY, ...
;; ----------------------------------------------------
_jump_steady::
    set #COMPONENT_H_JUMP_BIT, ENT_COMPONENTS_H(ix)    ;; Set the component for the entity to jump
    
    ld a, (_jump_steady_counter)
    dec a
    ld (_jump_steady_counter), a
    ret nz

    ld ENT_IA_ARRIVED(ix), #1   ;; Set arrived to 1 so we can use move_to transition
    ld a, #FOURTH_BOSS_JUMP_TIME
    ld (_jump_steady_counter), a

    ret

_jump_steady_counter:
    .db #FOURTH_BOSS_JUMP_TIME


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; spawns two entities
;; ----------------------------------------------------
;; PARAMS
;; HL: Entity 1
;; DE: Entity 2
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A,...
;; ----------------------------------------------------
_prepare_spawn::
    push de
    ld b, #THIRD_BOSS_SPAWN_POINT_1_X
    ld c, #THIRD_BOSS_SPAWN_POINT_1_y
    call _spawn_entity
    pop de

    ld h, d
    ld l, e
    ld b, #THIRD_BOSS_SPAWN_POINT_2_X
    ld c, #THIRD_BOSS_SPAWN_POINT_2_y
    call _spawn_entity
    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; spawns an entity (HL) on the position (A,B)
;; ----------------------------------------------------
;; PARAMS
;; HL: Pointer to the entity to spawn
;; B : X spawn position
;; C : Y spawn position
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A,...
;; ----------------------------------------------------
_spawn_entity::
    ld a, h             ;; If H pointer is 0xff, there is no minion to spawn
    cp #0xff
    ret z

    ; ld a, (numEntities)
    ; cp #THIRD_BOSS_MAX_ENTITIES_ALOWED_TO_SPAWN    It should be tested in the state before
    ; ret nc
    ld a, (_minion_counter)             ;; Increment minion counter
    inc a
    ld (_minion_counter), a

    push ix
    push bc
    call man_entity_createEntity
    pop bc

    ld a, h     ;; IX = HL
    ld__ixh_a
    ld a, l
    ld__ixl_a

    ;; Set points
    ld ENT_POINTS_L(ix), #0
    ld ENT_POINTS_L(ix), #POINTS_THIRD_BOSS_MINIONS

    ld a, b
    ld ENT_X(ix), a
    call sys_physics_restoreWorldX

    ld a, c
    ld ENT_Y(ix), a
    call sys_physics_restoreWorldY

    pop ix
    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; waits 64 cycles B times
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with this IA
;; B : wait times
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A, IX, ...
;; ----------------------------------------------------
_wait_cycles_B_times::
    ld a, (_waiter_counter)
    inc a
    ld (_waiter_counter), a
    cp b
    jr nc, _counter_maxed      ;; If counter >= wait times pass to the next state
    ld ENT_IA_DATA(ix), #TOTAL_WAIT_CYCLES_PER_TIME
    ret
_counter_maxed:
    xor a
    ld (_waiter_counter), a
    ret

_waiter_counter::
    .db #0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Transition for the wait state
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with this IA
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A, IX, ...
;; ----------------------------------------------------
_wait_transition::
    ;; Check counter
    ld a, ENT_IA_DATA(ix)
    and #IA_COUNTER_BITS
    ret nz              ;; If is not zero, it has not waited enought

    ;; HL = new state
    ld a, S_WAIT_CICLES(iy)
    call _transition_happens
    ret