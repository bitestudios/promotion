.include "man/entity.h.s"
.include "utils.h.s"
.include "lib/opmacros.h.s"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; HEALTH SYSTEM
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Updates the health of one entity and marks it for 
;; delete if the health is 0.
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity pointer
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A, HL
;; ----------------------------------------------------
sys_health_updateOneEntity::
    ;; Check if it's on invincibility time
    ld a, ENT_INVINCIBLE_COUNTER(ix)
    or a
    jp nz, _invincible

	;; Sub damage taken to health
	ld a, ENT_HEALTH(ix)
	ld c, ENT_DAMAGE(ix)
	sub c
	
	ld b, ENT_MAX_HEALTH(ix)
	clamp_a_min_0_max_b
	ld ENT_HEALTH(ix), a
    ld ENT_DAMAGE(ix), #0   ;; Restore damage taken to 0
	or a
	jr nz, __noZeroHealth

    ;; Activate death component
    set #COMPONENT_L_DEATH_BIT, ENT_COMPONENTS_L(ix)
	ret
	
	__noZeroHealth:


    ;; Reset blank flag
    res #RENDER_BLANK_BIT, ENT_RENDER_FLAGS(ix)

    ;; Set invincible counter if damage is not 0
	xor a
    add c
		ret z
		ret m
	__damaged:

	res #COMPONENT_L_COLLISION_BIT, ENT_COMPONENTS_L(ix)
    ld a, ENT_INVINCIBLE_TIME(ix)       ;; Set invincible counter
    ld ENT_INVINCIBLE_COUNTER(ix), a
	ret

_invincible:
    ;; Decrease counter
    dec a
    ld ENT_INVINCIBLE_COUNTER(ix), a
	jr nz, __invincibleCounterNotZero
		set #COMPONENT_L_COLLISION_BIT, ENT_COMPONENTS_L(ix)
	__invincibleCounterNotZero:

    ld ENT_DAMAGE(ix), #0   ;; Restore damage taken to 0

    ;; Change player sprite flag (blinking effect)
    sra a
    jr c, _blank

    ld a, ENT_RENDER_FLAGS(ix)
    res #RENDER_BLANK_BIT, a
    set #RENDER_DRAW_BIT, a
    ld ENT_RENDER_FLAGS(ix), a

    ret

_blank:
    ld a, ENT_RENDER_FLAGS(ix)
    set #RENDER_BLANK_BIT, a
    ld ENT_RENDER_FLAGS(ix), a

    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Apply physics to all entities
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, A, HL, BC
;; ----------------------------------------------------
sys_health_update::
	ld hl, #sys_health_updateOneEntity
    xor a
    ld b, a
    ld a, #COMPONENT_L_HEALTH
	call man_entity_forallMatchingEntities
	ret


