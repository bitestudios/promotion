.include "lib/opmacros.h.s"

.globl sys_collision_update
.globl sys_collision_updatePrev

;; MACROS

.macro divide_a_by_tile_size __displacements
    shiftr_a __displacements
.endm

.macro multiply_a_by_total_tiles_to_hl
    multiplication_16_tiles
.endm

.macro multiplication_40_tiles
    ;; 40y = 2⁵y + 2³y
    ld l, a                                 ;; Set y value on the low position to be multiplied
    ld h, #0                                ;; Set h value to 0

    shiftl_hl 5                                   ;; (1) Calculate 2⁵y.  DONE: Division gets anulated by multiplication (0ptimization 2)


    ld e, a                                 ;; Load y position on d
    xor a                                   ;; Reset a
    ld d, a                                 ;; Set b to 0

    shiftl_de 3                                   ;; (1) Calculate 2³y.  DONE: Division gets anulated by multiplication (0ptimization 2)

    add hl, de                              ;; HL = 2⁵y + 2³y
.endm

.macro multiplication_20_tiles
    ;; 20y = 2⁴y + 2²y
    ld l, a                                 ;; Set y value on the low position to be multiplied
    ld h, #0                                ;; Set h value to 0

    shiftl_hl 4                                   ;; (1) Calculate 2⁵y.  DONE: Division gets anulated by multiplication (0ptimization 2)


    ld e, a                                 ;; Load y position on d
    xor a                                   ;; Reset a
    ld d, a                                 ;; Set b to 0

    shiftl_de 2                                   ;; (1) Calculate 2³y.  DONE: Division gets anulated by multiplication (0ptimization 2)

    add hl, de                              ;; HL = 2⁵y + 2³y
.endm


.macro multiplication_16_tiles
    ;; 16y = 2^4y
    ld l, a         ;; Set y value on the low position to be multiplied
    ld h, #0        ;; Set h value to 0

    shiftl_hl 4     ;;- (1) Calculate 2⁵y.  DONE: Division gets anulated by multiplication
                    ;;\ (0ptimization 2)
.endm


BOUNDARY_COLLISION_LEFT       = 0b10000000 ;; Put this way so they match with the feet
BOUNDARY_COLLISION_LEFT_BIT   = 7
BOUNDARY_COLLISION_RIGHT      = 0b01000000
BOUNDARY_COLLISION_RIGHT_BIT  = 6
BOUNDARY_COLLISION_TOP        = 0b00100000
BOUNDARY_COLLISION_TOP_BIT    = 5
BOUNDARY_COLLISION_BOTTOM     = 0b00010000
BOUNDARY_COLLISION_BOTTOM_BIT = 4


