.include "man/entity.h.s"
.include "man/entityTables.h.s"
.include "utils.h.s"
.include "lib/opmacros.h.s"
.include "cpctelera.h.s"
.include "cfg.h.s"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; PHYSICS SYSTEM
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Calculates the new velocity value using the
;; acceleration and friction
;; ----------------------------------------------------
;; PARAMS
;; A: Acceleration
;; D: Velocity
;; E: Friction
;; B: Max vel
;; ----------------------------------------------------
;; RETURNS
;; A: New velocity
;; ----------------------------------------------------
;; DESTROYS
;; E, B
;; ----------------------------------------------------
sys_physics_calculateNewVelocity::
	add d                    ;; A(Acceleration) = A(Acceleration) + D(Velocity)
	ret z
	jp m, __negativeVel
	;; If velocity positive
		sub e                ;; A(Velocity) = A(Velocity) - E(Friction)
		clamp_a_min_0_max_b  ;; Clamps A(Velocity) between 0 and B(Max vel)
		ret
	__negativeVel:
	;; If velocity negative
		add e                ;; A(Velocity) = A(Velocity) + E(Friction)

		ld e, a              ;;\
		ld a, b              ;; |
		neg                  ;;  - Turns max vel negative, now is min vel
		ld b, a              ;; |
		ld a, e              ;;/

		clamp_a_min_b_max_0  ;; Clamps A(Velocity) between B(Min vel) and 0
	ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Move the entity given and checks collisions with
;; screen borders
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity pointer
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A, BC, HL
;; ----------------------------------------------------
sys_physics_moveEntity::
	
	ld a, ENT_X(ix)                       ;;\
	ld ENT_PREV_X(ix), a                  ;; \_ Sets the previous positions
	ld a, ENT_Y(ix)                       ;; /
	ld ENT_PREV_Y(ix), a                  ;;/
	
	
	ld hl, #entity_table_maxVelocityX     ;;\_ Gets the max velocity X on a
	call man_entity_entityTableBLookup    ;;/
	
	ld b, a                               ;;\
	ld a, ENT_ACCELERATION_X(ix)          ;; _Sets the needed params to calculate the new velocity
	ld d, ENT_VX(ix)                      ;; |
	ld e, ENT_FRICTION_X(ix)              ;;/

	call sys_physics_calculateNewVelocity

	ld ENT_VX(ix), a                      ;; Stores the x velocity

    or a
	jr z, __endPosX                       ;;- If the x velocity (a) is zero we dont need calculate
	                                      ;;\_ a new position

	;; a = posX + vx
	ld l, ENT_X_WORLD_L(ix)               ;;\_ Stores the X world coordinates on hl
	ld h, ENT_X_WORLD_H(ix)               ;;/
	
	ld_neg_bc_a                           ;;/ Loads A(velocity x) on BC if A is negative BC will be
	                                      ;;\ negative

	add hl, bc                            ;; HL(World coords) = HL(World coords) + BC(Velocity)

	ld ENT_X_WORLD_L(ix), l               ;;\_ Stores the new calculated world coords
	ld ENT_X_WORLD_H(ix), h               ;;/

	shiftr_hl WORLD_SCALE_X_DISP          ;; Convert te world coords into screen coords

	ld ENT_X(ix), l                       ;; Stores the screen coords

	__endPosX:


	ld a, ENT_ACCELERATION_Y(ix)          ;;\
	ld d, ENT_VY(ix)                      ;; |- Sets the needed params to calculate the new 
	ld e, ENT_FRICTION_Y(ix)              ;; |- Y velocity
	ld b, #MAX_VEL_Y                      ;;/

	call sys_physics_calculateNewVelocity

	ld ENT_VY(ix), a                      ;; Stores the x velocity
	
	jr z, __endPosY                       ;;- If the y velocity (a) is zero we dont need calculate
	                                      ;;\_ a new position

	;; a = posY + vy
	ld l, ENT_Y_WORLD_L(ix)               ;;\_ Stores the x world coords on HL
	ld h, ENT_Y_WORLD_H(ix)               ;;/

	ld_neg_bc_a                           ;;/ Loads A(velocity y) on BC if A is negative BC will be
	                                      ;;\ negative

	add hl, bc                            ;; HL(World coords) = HL(World coords) + BC(Velocity)
	
	ld ENT_Y_WORLD_L(ix), l               ;;\_ Stores the new calculated world coords
	ld ENT_Y_WORLD_H(ix), h               ;;/

	shiftr_hl WORLD_SCALE_Y_DISP          ;; Convert te world coords into screen coords

	ld ENT_Y(ix), l                       ;; Stores the screen coords

	__endPosY:
    
	;; Checks if is falling (VY > 0)
	;; If is falling JUMP_STATUS = JUMP_STATUS_MAX
    ld a, ENT_VY(ix)
	or a
;	jr z, _endIsFalling
   jp m, _endIsFalling
		ld ENT_JUMP_STATUS(ix), #JUMP_STATUS_MAX
	_endIsFalling:

    ;; No draws the entity if the previous x and y are equal than the actual
	;; X comparison
	ld a, ENT_PREV_X(ix)
	sub ENT_X(ix)
	jr nz, _hasToRedraw
	;; Y comparison
	ld a, ENT_PREV_Y(ix)
	sub ENT_Y(ix)
	jr nz, _hasToRedraw

    jr _doesNotHasToRedraw
_hasToRedraw:
    ;; Set draw flag
    set #RENDER_DRAW_BIT, ENT_RENDER_FLAGS(ix)
_doesNotHasToRedraw:

    ;; Set facing direction
    ld a, ENT_VX(ix)
    or a
    ret z                       ;; If speed is 0 mantain facing direction
    and #0x80
    jp z, _facing_right
    ld ENT_FACING_DIR(ix), #1
    ret
_facing_right:
    ld ENT_FACING_DIR(ix), a    ;; Set facing direction to 0
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Apply physics to all entities
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, A, HL, BC
;; ----------------------------------------------------
sys_physics_update::
	ld hl, #sys_physics_moveEntity
    xor a
    ld b, a
    or #COMPONENT_L_PHYSICS
	call man_entity_forallMatchingEntities
	ret


sys_physics_restoreWorldX::
	ld l, ENT_X(ix)
	ld h, #0
	shiftl_hl WORLD_SCALE_X_DISP
	ld ENT_X_WORLD_H(ix), h
	ld ENT_X_WORLD_L(ix), l
	ret

sys_physics_restoreWorldY::
	ld l, ENT_Y(ix)
	ld h, #0
	shiftl_hl WORLD_SCALE_Y_DISP
	ld ENT_Y_WORLD_H(ix), h
	ld ENT_Y_WORLD_L(ix), l
	ret

