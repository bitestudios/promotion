.include "cpctelera.h.s"
.include "man/entity.h.s"
.include "utils.h.s"
.include "lib/opmacros.h.s"
.include "assets/assets.h.s"
.include "man/entityTables.h.s"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; SHOOTING SYSTEM
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BEFORESHOOT_TIME = 1
AFTERSHOOT_TIME = 5

COUNTER_BITS = 0b01111111
FASE_BITS = 0b10000000



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Executes the shooting behaviour of one entity
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity pointer
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A, HL, B
;; ----------------------------------------------------
sys_shooting_updateOneEntity::
	;; Check what fase the entity is
    ld a, ENT_SHOOTING_DATA(ix)
    ld b, a
    and #FASE_BITS
    jr nz, _after_shoot

    ;; Check counter
    ld a, b
    and #COUNTER_BITS
    jr z, _change_fase
        
    ;; Decrease and save counter
    call sys_shooting_decreaseCounter
    ret

_change_fase:
    ;; Set after fase and reset counter
    ld a, #AFTERSHOOT_TIME
    set 7, a
    ld ENT_SHOOTING_DATA(ix), a


	ld hl, #man_behaviors_shooting_table
	call man_entity_entityTableWLookup

	jp (hl)

    ret ;; FALTABA AQUI UN RET???!!! NO PORQUE EL BEHAVIOUR HACE RET

_after_shoot:
    ;; Check counter
    ld a, b
    and #COUNTER_BITS
    jr z, _end_shooting

    ;; Decrease and save counter
    call sys_shooting_decreaseCounter
    ret

_end_shooting:
    ld a, #BEFORESHOOT_TIME
    ld ENT_SHOOTING_DATA(ix), a

    res COMPONENT_L_SHOOTING_BIT, ENT_COMPONENTS_L(ix)

    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Decreases and stores the counter on the shoting data
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity
;; A : Counter current value
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A, BC
;; ----------------------------------------------------
sys_shooting_decreaseCounter::
    dec a
    ld c, a
    ld a, b
    and #FASE_BITS          ;; Reset all bits except for the flag one
    or c                    ;; Set all the bits
    ld ENT_SHOOTING_DATA(ix), a
    ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Execute shooting behaviours of the entities
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, A, HL, BC
;; ----------------------------------------------------
sys_shooting_update::
	ld a, #COMPONENT_L_SHOOTING
    ld b, #0
	ld hl, #sys_shooting_updateOneEntity
	call man_entity_forallMatchingEntities
	ret

