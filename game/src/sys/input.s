.include "cpctelera.h.s"
.include "utils.h.s"
.include "man/entity.h.s"
.include "man/game.h.s"
.include "sys/collision.h.s"
.include "assets/assets.h.s"
.include "man/scene.h.s"
.include "scene/pause.h.s"
.include "cfg.h.s"
.include "input.h.s"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; INPUT SYSTEM
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BUTTON_NEXT = #Key_N
BUTTON_HELATH = #Key_H
; BUTTON_LEFT = #Key_O
; BUTTON_RIGHT = #Key_P
; BUTTON_JUMP = #Key_Q
; BUTTON_PAUSE = #Key_Esc
; BUTTON_SHOOT = #Key_Space

button_bind::
    .dw #Key_C

button_left::
    .dw #Key_O

button_right::
    .dw #Key_P

button_jump::
    .dw #Key_Q

button_pause::
    .dw #Key_Del

button_reset::
    .dw #Key_Esc

button_shoot::
    .dw #Key_Space


shoot_previouslyPressed:
    .db 0



.macro sys_input_cheats

	ld hl, #BUTTON_NEXT
	call cpct_isKeyPressed_asm
	jr z, endif_NEXT_pressed
		ld a, #MAN_GAME_STATUS_NEXT_MAP
		call man_game_setGameStatus
	endif_NEXT_pressed:
	
	ld hl, #BUTTON_HELATH
	call cpct_isKeyPressed_asm
	jr z, endif_HEALTH_pressed
		call man_entity_getPlayer
		ld a, #-8
		ld ENT_DAMAGE(ix), a
	endif_HEALTH_pressed:

.endm





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Reads keyboard inputs and updates player entity
;; physics
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, AF, BC, DE, HL
;; ----------------------------------------------------
sys_input_update::
	call man_entity_getPlayer ;; Stores player ptr on ix

    ;; Check if player has to get inputs
    bit #COMPONENT_H_INPUT_BIT, ENT_COMPONENTS_H(ix)
    jp z, _no_player_inputs

	ld ENT_ACCELERATION_X(ix), #0

    ; call sys_input_getKeyPressed


    ;; Shoot
	ld hl, (button_shoot)
	call cpct_isKeyPressed_asm

	jr z, endif_SHOOT_pressed                   ;; If SHOOT button was pressed
	ld a, (shoot_previouslyPressed)             ;; And wasn't previously pressed
	or a
	jr nz, endif_SHOOT_not_pressed
		set COMPONENT_L_SHOOTING_BIT, ENT_COMPONENTS_L(ix)
		ld a, #1
		ld (shoot_previouslyPressed), a
        jr endif_SHOOT_not_pressed
	endif_SHOOT_pressed:
        xor a
        ld (shoot_previouslyPressed), a              ;; reset release key boolean
    endif_SHOOT_not_pressed:

	;; Move left
	ld hl, (button_left)
	call cpct_isKeyPressed_asm
	jr z, endif_LEFT_pressed
		ld ENT_ACCELERATION_X(ix), #-PLAYER_INPUT_ACCELERATION
	endif_LEFT_pressed:

	;; Move right
	ld hl, (button_right)
	call cpct_isKeyPressed_asm
	jr z, endif_RIGHT_pressed
		ld ENT_ACCELERATION_X(ix), #PLAYER_INPUT_ACCELERATION
	endif_RIGHT_pressed:

	;; Jump
	ld hl, (button_jump)
	call cpct_isKeyPressed_asm
	ld a, ENT_JUMP_STATUS(ix)
	
	;; If jump button is pressed and ENT_JUMP_STATUS isn't JUMP_STATUS_MAX, increments
	;; ENT_JUMP_STATUS and sets ENT_VY to JUMP_VELOCITY
	jr z, else_JUMP_pressed
        set #COMPONENT_H_JUMP_BIT, ENT_COMPONENTS_H(ix)
        jr endif_JUMP_pressed

	;; If jump button isn't pressed sets ENT_JUMP_STATUS to JUMP_STATUS_MAX
	else_JUMP_pressed:
		ld a, #JUMP_STATUS_MAX
		ld ENT_JUMP_STATUS(ix), a
	endif_JUMP_pressed:
	
;;	sys_input_cheats

_no_player_inputs:
	;; Checks pause button
	call sys_input_checkPause
	ret nz
	
	call sys_input_checkReset
	ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Reads keyboard inputs and updates player entity
;; physics
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, AF, BC, DE, HL
;; ----------------------------------------------------
sys_input_pauseUpdate::
	call sys_input_checkPause
	ret nz
	call sys_input_checkReset
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Gets the ID of the FIRST key pressed found on the key
;; buffer
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; HL: Key pressed if anyone pressed. If not returns 0
;; ----------------------------------------------------
;; DESTROYS
;; AF, HL
;; ----------------------------------------------------
sys_input_getKeyPressed::
    ld hl, #_cpct_keyboardStatusBuffer
    xor a                           ;; A = 0

_kp_loop:
    cp #BUFFER_SIZE
    jr z, _kp_endLoop               ;; Check counter value. End if its 0
    ld (_size_counter), a

    ld a, (hl)                      ;; Load byte from the buffer
    xor #ZERO_KEYS_ACTIVATED        ;; Inverts bytes
    jr z, _no_key_detected
        ld h, a                     ;; H is the mask
        ld a, (_size_counter)
        ld l, a                     ;; L is the offset
        ; ld (_current_key_pressed), hl
        ret
_no_key_detected:
    inc hl
_size_counter = .+1
    ld a, #0x00                     ;; AUTOMODIFIABLE, A = counter
    inc a
    jr _kp_loop
_kp_endLoop:
    ld hl, #0x00                    ;; Return 0 if no key is pressed
    ld a, #0
    ld (_key_released), a
    ret

; _current_key_pressed::
;     .dw #0x0000


_key_released::
    .db #0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Does not return the key pressed until one is pressed.
;; WARNING: This blocks the execution until done
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; HL: Key pressed
;; ----------------------------------------------------
;; DESTROYS
;; AF, HL
;; ----------------------------------------------------
sys_input_waitKeyPressed::
    call sys_input_getKeyPressed
    ld a, (_key_released)
    or a
    jr nz, sys_input_waitKeyPressed
    xor a
    or h
    or l
    jr z, sys_input_waitKeyPressed
    ld a, #1
    ld (_key_released), a
    ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Reads keyboard inputs and updates player entity
;; physics
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, AF, BC, DE, HL
;; ----------------------------------------------------
sys_input_titleUpdate::
    call sys_input_checkBind
    jp z, _not_bind
        ld a, #MAN_GAME_STATUS_BINDING
		call man_game_setGameStatus
_not_bind:
	call sys_input_checkFire
	ret z
		ld a, #MAN_GAME_STATUS_STARTED
		call man_game_setGameStatus
	ret



sys_input_checkFire::
	ld hl, (button_shoot)
	call cpct_isKeyPressed_asm
	ret

sys_input_checkBind::
	ld hl, (button_bind)
	call cpct_isKeyPressed_asm
	ret


sys_input_checkPause::
	ld hl, (button_pause)
	call cpct_isKeyPressed_asm
	ret z
		ld a, #MAN_GAME_STATUS_PAUSED
		call man_game_setGameStatus
	ret

sys_input_checkReset::
	ld hl, (button_reset)
	call cpct_isKeyPressed_asm
	ret z
		ld a, #MAN_GAME_STATUS_RESETED
		call man_game_setGameStatus
	ret

