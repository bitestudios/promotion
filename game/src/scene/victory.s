.include "cpctelera.h.s"
.include "utils.h.s"
.include "helper/sound.h.s"
.include "lib/print.h.s"
.include "lib/wait.h.s"
.include "lib/ui.h.s"
.include "ui/victory.h.s"
.include "scene/bonus.h.s"
.include "man/scene.h.s"
.include "man/interruption.h.s"
.include "assets/assets.h.s"
.include "cfg.h.s"

waitTime = 12

scene_victory_init::
	call man_interruption_disableMusic
	
	ld ix, #ui_victory
	call bite_ui_drawComponent

    helper_soundm_setSong _MUSIC_WIN_START
	
	call man_interruption_enableMusic
	
	call man_interruption_init

	jp man_scene_sceneInitEnd



scene_victory_start::
	jp man_scene_sceneStartEnd



scene_victory_update::
	ld d, #waitTime
	call bite_wait_waitSeconds
	ld a, #SCENE_PHASE_TERMINATE
	call man_scene_setPhase
	ld hl, #scene_bonus
	call man_scene_push

	ret
	


scene_victory_terminate::
	jp man_scene_sceneTerminateEnd

SetFieldW SCENE_FUNCTION_INIT      scene_victory_init
SetFieldW SCENE_FUNCTION_START     scene_victory_start
SetFieldW SCENE_FUNCTION_UPDATE    scene_victory_update
SetFieldW SCENE_FUNCTION_TERMINATE scene_victory_terminate
DefineScene scene_victory

