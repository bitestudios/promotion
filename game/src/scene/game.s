.include "cpctelera.h.s"
.include "sys/render.h.s"
.include "sys/physics.h.s"
.include "sys/input.h.s"
.include "sys/collision.h.s"
.include "sys/health.h.s"
.include "sys/ia.h.s"
.include "sys/shooting.h.s"
.include "sys/animation.h.s"
.include "sys/hud.h.s"
.include "sys/energy.h.s"
.include "sys/death.h.s"
.include "sys/jump.h.s"
.include "factory/factory.h.s"
.include "man/game.h.s"
.include "man/entity.h.s"
.include "man/scene.h.s"
.include "scene/victory.h.s"
.include "scene/gameover.h.s"
.include "scene/pause.h.s"
.include "scene/title.h.s"
.include "scene/lostlife.h.s"
.include "scene/elevator.h.s"
.include "scene/end.h.s"
.include "man/interruption.h.s"
.include "assets/assets.h.s"
.include "helper/sound.h.s"
.include "man/map.h.s"

scene_game_init::
	call man_entity_clearExceptPlayer
	
	call man_map_loadMap
	
	call sys_render_init
	
	call man_interruption_init

	jp man_scene_sceneInitEnd



scene_game_start::
	call man_interruption_enableMusic

	jp man_scene_sceneStartEnd



scene_game_update::
	call sys_render_update
	call sys_render_updateHUD
	call sys_collision_updatePrev
	call sys_input_update
    call sys_jump_update
    call sys_ia_update
	;cpctm_setBorder_asm HW_BLUE
	call sys_physics_update
	;cpctm_setBorder_asm HW_YELLOW
	call sys_collision_update
	;cpctm_setBorder_asm HW_BLACK
    call sys_animation_update
    call sys_shooting_update
	call sys_health_update
	call sys_energy_update
    call sys_death_update
	call sys_hud_update
	
	call man_game_getGameStatus
	cp #MAN_GAME_STATUS_PAUSED
	jr nz, __notPaused
		ld a, #SCENE_PHASE_START
		call man_scene_setPhase
		ld hl, #scene_pause
		call man_scene_push
		jp __scene_game_resetGameStatus
	__notPaused:

	cp #MAN_GAME_STATUS_RESETED
	jr nz, __notReseted
		call man_scene_clearStack
		ld hl, #scene_title
		call man_scene_push
		jr __scene_game_resetGameStatus
	__notReseted:
	
	cp #MAN_GAME_STATUS_NEXT_MAP
	jr nz, __noNextMap
		call man_map_nextMap
		ld a, #SCENE_PHASE_INIT
		call man_scene_setPhase
		jr __scene_game_resetGameStatus
	__noNextMap:
	
	cp #MAN_GAME_STATUS_NEXT_FLOOR
	jr nz, __noNextFloor
		call man_map_nextMap
		ld a, #SCENE_PHASE_INIT
		call man_scene_setPhase
		ld hl, #scene_elevator
		call man_scene_push
		jr __scene_game_resetGameStatus
	__noNextFloor:
	
	cp #MAN_GAME_STATUS_BOSS_DEFEATED
	jr nz, noBossDefeated
		call man_map_nextMap
		ld a, #SCENE_PHASE_INIT
		call nz, man_scene_setPhase
		jr nz, __setPhase
			ld a, #SCENE_PHASE_TERMINATE
			call man_scene_setPhase
		    ld hl, #scene_end
		    call man_scene_push
		__setPhase:
		ld hl, #scene_victory
		call man_scene_push
		jr __scene_game_resetGameStatus
	noBossDefeated:


	cp #MAN_GAME_STATUS_PLAYER_DEFEATED
	jr nz, noPlayerDefeated
		call man_game_decRemainingLives
		call man_game_checkRemainingLives
		jr nz, __remainingLivesNoZero
			ld a, #SCENE_PHASE_TERMINATE
			call man_scene_setPhase
			ld hl, #scene_gameover
			call man_scene_push
			ld hl, #scene_lostlife
			call man_scene_push
			jr __scene_game_resetGameStatus
		__remainingLivesNoZero:
		ld a, #SCENE_PHASE_INIT
		call man_scene_setPhase
		call man_map_backToCheckpoint
		call man_entity_restorePlayer
		ld hl, #scene_lostlife
		call man_scene_push
		jr __scene_game_resetGameStatus
	noPlayerDefeated:

    call man_entity_deleteMarked
	ret

	__scene_game_resetGameStatus:
	ld a, #MAN_GAME_STATUS_NORMAL
	call man_game_setGameStatus
	ret


scene_game_terminate::
	jp man_scene_sceneTerminateEnd


SetFieldW SCENE_FUNCTION_INIT      scene_game_init
SetFieldW SCENE_FUNCTION_START     scene_game_start
SetFieldW SCENE_FUNCTION_UPDATE    scene_game_update
SetFieldW SCENE_FUNCTION_TERMINATE scene_game_terminate
DefineScene scene_game

