.include "cpctelera.h.s"
.include "utils.h.s"
.include "assets/assets.h.s"
.include "lib/ui.h.s"
.include "helper/sound.h.s"
.include "scene/title.h.s"
.include "man/scene.h.s"
.include "scene/game.h.s"
.include "man/interruption.h.s"
.include "factory/factory.h.s"
.include "man/game.h.s"
.include "man/map.h.s"
.include "man/entity.h.s"
.include "sys/input.h.s"
.include "scene/bind.h.s"
.include "ui/contract.h.s"
.include "cfg.h.s"



scene_titleImage_init::
	jp man_scene_sceneInitEnd


scene_titleImage_start::
	call man_interruption_disableMusic
	
	ld hl, #_palette_title
	call man_interruption_setGameplayPalette
	call man_interruption_setHUDPalette

	cpctm_setBorder_asm HW_BLACK
	
	helper_soundm_setSong _MUSIC_MAIN_THEME_EXTENDED_START

	call man_interruption_enableMusic
	
	jp man_scene_sceneStartEnd



scene_titleImage_update::
	call sys_input_waitKeyPressed
	ld a, #SCENE_PHASE_TERMINATE
	call man_scene_setPhase
	ld hl, #scene_title
	call man_scene_push
	ret


scene_titleImage_terminate::
	jp man_scene_sceneTerminateEnd


SetFieldW SCENE_FUNCTION_INIT      scene_titleImage_init
SetFieldW SCENE_FUNCTION_START     scene_titleImage_start
SetFieldW SCENE_FUNCTION_UPDATE    scene_titleImage_update
SetFieldW SCENE_FUNCTION_TERMINATE scene_titleImage_terminate
DefineScene scene_titleImage


