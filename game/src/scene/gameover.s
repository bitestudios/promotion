.include "cpctelera.h.s"
.include "utils.h.s"
.include "assets/assets.h.s"
.include "helper/keyboard.h.s"
.include "helper/sound.h.s"
.include "lib/wait.h.s"
.include "lib/ui.h.s"
.include "ui/gameover.h.s"
.include "man/scene.h.s"
.include "man/interruption.h.s"
.include "cfg.h.s"

waitTime = 10

waitTimeCounter: .db 0

scene_gameover_init::
	call man_interruption_disableMusic
	
	ld hl, #_palette_hud
	call man_interruption_setGameplayPalette
	call man_interruption_setHUDPalette

	;; SET BORDER
	cpctm_setBorder_asm HW_BLACK

	cpctm_clearScreen_asm #0x00
	
	ld ix, #ui_gameover
	call bite_ui_drawComponent

	ld a, #waitTime
	ld (waitTimeCounter), a

    helper_soundm_setSong _MUSIC_GAMEOVER_START
	
	call man_interruption_enableMusic
	
	jp man_scene_sceneInitEnd




scene_gameover_start::
	jp man_scene_sceneStartEnd



scene_gameover_update::
	ld a, #GAMEPLAY_CYCLES
	call bite_waitCycles
	ld a, (waitTimeCounter)
	dec a
	jr z, counterZero
		ld (waitTimeCounter), a
		ret
	counterZero:
		ld a, #SCENE_PHASE_TERMINATE
		call man_scene_setPhase
	ret


scene_gameover_terminate::
	jp man_scene_sceneTerminateEnd


SetFieldW SCENE_FUNCTION_INIT      scene_gameover_init
SetFieldW SCENE_FUNCTION_START     scene_gameover_start
SetFieldW SCENE_FUNCTION_UPDATE    scene_gameover_update
SetFieldW SCENE_FUNCTION_TERMINATE scene_gameover_terminate
DefineScene scene_gameover

