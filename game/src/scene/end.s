.include "cpctelera.h.s"
.include "utils.h.s"
.include "assets/assets.h.s"
.include "helper/sound.h.s"
.include "sys/input.h.s"
.include "lib/wait.h.s"
.include "lib/ui.h.s"
.include "lib/math.h.s"
.include "ui/end.h.s"
.include "man/game.h.s"
.include "man/scene.h.s"
.include "man/interruption.h.s"
.include "cfg.h.s"


scene_end_init::
	call man_interruption_disableMusic
	
	ld hl, #_palette_default
	call man_interruption_setGameplayPalette
	call man_interruption_setHUDPalette

	;; SET BORDER
	cpctm_setBorder_asm HW_BLACK

	cpctm_clearScreen_asm #0x00
	
	ld bc, (game_score)
	ld hl, #ui_end_scoreboardDigits
	call bite_ui_componentText_getString
	ld a, #5
	ld bc, (game_score)
	call bite_math_u16ToStr
	
	ld ix, #ui_end
	call bite_ui_drawComponent

    helper_soundm_setSong _MUSIC_MAIN_THEME_START
	
	call man_interruption_enableMusic
	
	jp man_scene_sceneInitEnd




scene_end_start::
	jp man_scene_sceneStartEnd



scene_end_update::
	call sys_input_waitKeyPressed
	ld a, #SCENE_PHASE_TERMINATE
	call man_scene_setPhase
	ret


scene_end_terminate::
	jp man_scene_sceneTerminateEnd


SetFieldW SCENE_FUNCTION_INIT      scene_end_init
SetFieldW SCENE_FUNCTION_START     scene_end_start
SetFieldW SCENE_FUNCTION_UPDATE    scene_end_update
SetFieldW SCENE_FUNCTION_TERMINATE scene_end_terminate
DefineScene scene_end

