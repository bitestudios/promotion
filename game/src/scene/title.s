.include "cpctelera.h.s"
.include "utils.h.s"
.include "assets/assets.h.s"
.include "lib/ui.h.s"
.include "helper/sound.h.s"
.include "ui/title.h.s"
.include "man/scene.h.s"
.include "scene/game.h.s"
.include "man/interruption.h.s"
.include "factory/factory.h.s"
.include "helper/keyboard.h.s"
.include "man/game.h.s"
.include "man/map.h.s"
.include "man/entity.h.s"
.include "sys/input.h.s"
.include "scene/bind.h.s"
.include "ui/contract.h.s"
.include "cfg.h.s"



scene_title_init::
	jp man_scene_sceneInitEnd


scene_title_start::
	ld a, #PLAYER_MAX_LIVES
	call man_game_setRemainingLives

	call man_game_initScore

	call man_entity_init
	
	call factory_createPlayer

	call man_map_init

	call man_interruption_disableMusic
	
	ld hl, #_palette_default
	call man_interruption_setGameplayPalette
	call man_interruption_setHUDPalette

	cpctm_setBorder_asm HW_BLACK
	
	cpctm_clearScreen_asm #0x00
	
	ld ix, #ui_title
	; ld ix, #ui_contractscreen
	call bite_ui_drawComponent

	helper_soundm_setSong _MUSIC_MAIN_THEME_START

	call man_interruption_enableMusic

	call helper_keyboard_waitNoKeysPressed
	
	jp man_scene_sceneStartEnd



scene_title_update::
	call sys_input_titleUpdate

	call man_game_getGameStatus
	cp #MAN_GAME_STATUS_STARTED
	jr nz, __notStarted
		ld a, #SCENE_PHASE_START
		call man_scene_setPhase
		ld hl, #scene_game
		call man_scene_push
		ret
	__notStarted:
    cp #MAN_GAME_STATUS_BINDING
    jr nz, __not_binding
        ld a, #SCENE_PHASE_INIT
        call man_scene_setPhase
        ld hl, #scene_bind
        call man_scene_push
__not_binding:
	ret


scene_title_terminate::
	jp man_scene_sceneTerminateEnd


SetFieldW SCENE_FUNCTION_INIT      scene_title_init
SetFieldW SCENE_FUNCTION_START     scene_title_start
SetFieldW SCENE_FUNCTION_UPDATE    scene_title_update
SetFieldW SCENE_FUNCTION_TERMINATE scene_title_terminate
DefineScene scene_title

