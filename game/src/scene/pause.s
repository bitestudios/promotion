.include "cpctelera.h.s"
.include "utils.h.s"
.include "lib/ui.h.s"
.include "ui/hud.h.s"
.include "sys/input.h.s"
.include "sys/render.h.s"
.include "helper/keyboard.h.s"
.include "man/scene.h.s"
.include "man/game.h.s"
.include "man/interruption.h.s"
.include "scene/title.h.s"


scene_pause_init::
	call helper_keyboard_waitNoKeysPressed
	call scene_pause_setPauseButton
	jp man_scene_sceneInitEnd


scene_pause_start::
	call man_interruption_disableMusic
	jp man_scene_sceneStartEnd


scene_pause_update::
	call sys_render_updateHUD
	call sys_input_pauseUpdate
	call man_game_getGameStatus
	
	cp #MAN_GAME_STATUS_PAUSED
	jr nz, __notPaused
		ld a, #SCENE_PHASE_TERMINATE
		call man_scene_setPhase
		call scene_pause_setPlayButton
		ret
	__notPaused:

	cp #MAN_GAME_STATUS_RESETED
	ret nz
		call man_scene_clearStack
		ld hl, #scene_title
		call man_scene_push
		call scene_pause_setPlayButton
	ret


scene_pause_terminate::
	call helper_keyboard_waitNoKeysPressed
	jp man_scene_sceneTerminateEnd



scene_pause_setPauseButton::
	ld ix, #ui_hud_playOrPauseButton
	call bite_ui_componentNode_clearChilds
	ld de, #ui_hud_pauseButton
	call bite_ui_componentNode_addChild
	
	ld ix, #ui_hudUpdated
	ld de, #ui_hud_playOrPauseButton
	call bite_ui_componentNode_addChild
	ret


scene_pause_setPlayButton::
	ld ix, #ui_hud_playOrPauseButton
	call bite_ui_componentNode_clearChilds
	ld de, #ui_hud_playButton
	call bite_ui_componentNode_addChild

	ld ix, #ui_hudUpdated
	ld de, #ui_hud_playOrPauseButton
	call bite_ui_componentNode_addChild
	ret


SetFieldW SCENE_FUNCTION_INIT      scene_pause_init
SetFieldW SCENE_FUNCTION_START     scene_pause_start
SetFieldW SCENE_FUNCTION_UPDATE    scene_pause_update
SetFieldW SCENE_FUNCTION_TERMINATE scene_pause_terminate
DefineScene scene_pause

