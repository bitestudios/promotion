.include "cpctelera.h.s"
.include "utils.h.s"
.include "helper/sound.h.s"
.include "lib/print.h.s"
.include "lib/wait.h.s"
.include "lib/ui.h.s"
.include "sys/hud.h.s"
.include "sys/render.h.s"
.include "man/sfx.h.s"
.include "man/scene.h.s"
.include "man/game.h.s"
.include "man/interruption.h.s"
.include "assets/assets.h.s"
.include "cfg.h.s"


scene_bonus_init::
    halt
    helper_soundm_setSong _MUSIC_SLOT_MACHINE_START
	jp man_scene_sceneInitEnd



scene_bonus_start::
	jp man_scene_sceneStartEnd



scene_bonus_update::
	call sys_hud_updateBonus
	call sys_render_updateHUD
	ld a, #2
	call bite_waitCycles
	call man_game_getGameStatus
	cp #MAN_GAME_STATUS_BONUS_COMPLETED
	ret nz
	ld a, #MAN_GAME_STATUS_NORMAL
	call man_game_setGameStatus
	ld a, #SCENE_PHASE_TERMINATE
	call man_scene_setPhase
	ret


scene_bonus_terminate::
    halt
	helper_soundm_setSong _MUSIC_EMPTY_START
	ld d, #4
	call bite_wait_waitSeconds
	jp man_scene_sceneTerminateEnd


SetFieldW SCENE_FUNCTION_INIT      scene_bonus_init
SetFieldW SCENE_FUNCTION_START     scene_bonus_start
SetFieldW SCENE_FUNCTION_UPDATE    scene_bonus_update
SetFieldW SCENE_FUNCTION_TERMINATE scene_bonus_terminate
DefineScene scene_bonus

