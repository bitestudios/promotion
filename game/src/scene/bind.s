.include "cpctelera.h.s"
.include "utils.h.s"
.include "assets/assets.h.s"
.include "helper/sound.h.s"
.include "ui/title.h.s"
.include "man/scene.h.s"
.include "man/interruption.h.s"
.include "factory/factory.h.s"
.include "man/game.h.s"
.include "man/map.h.s"
.include "man/entity.h.s"
.include "sys/input.h.s"
.include "ui/keys.h.s"
.include "lib/ui.h.s"

scene_bind_init::
	jp man_scene_sceneInitEnd


scene_bind_start::
	cpctm_clearScreen_asm #0x00
	
	ld ix, #ui_bind_left
	call bite_ui_drawComponent

    ld a, #1
    ld (_key_released), a             ;; Avoid seting a key with the C, forcing a key release
	
	jp man_scene_sceneStartEnd



scene_bind_update::
    ;; Bind keys one by one
	call sys_input_waitKeyPressed
    ld (button_left), hl

    cpctm_clearScreen_asm #0x00
	ld ix, #ui_bind_right
	call bite_ui_drawComponent

	call sys_input_waitKeyPressed
    ld (button_right), hl

    cpctm_clearScreen_asm #0x00
	ld ix, #ui_bind_jump
	call bite_ui_drawComponent

	call sys_input_waitKeyPressed
    ld (button_jump), hl

    cpctm_clearScreen_asm #0x00
	ld ix, #ui_bind_fire
	call bite_ui_drawComponent

	call sys_input_waitKeyPressed
    ld (button_shoot), hl

    cpctm_clearScreen_asm #0x00
	ld ix, #ui_bind_pause
	call bite_ui_drawComponent

	call sys_input_waitKeyPressed
    ld (button_pause), hl

    cpctm_clearScreen_asm #0x00
	ld ix, #ui_bind_reset
	call bite_ui_drawComponent

	call sys_input_waitKeyPressed
    ld (button_reset), hl

    ld a, #SCENE_PHASE_TERMINATE
    call man_scene_setPhase

    ; cpctm_clearScreen_asm #0x00         ;; Press any key to continue
	; ld ix, #ui_bind_any
	; call bite_ui_drawComponent

    ; call sys_input_waitKeyPressed       ;; Press any key to continue
	
	ret


scene_bind_terminate::
	jp man_scene_sceneTerminateEnd


SetFieldW SCENE_FUNCTION_INIT      scene_bind_init
SetFieldW SCENE_FUNCTION_START     scene_bind_start
SetFieldW SCENE_FUNCTION_UPDATE    scene_bind_update
SetFieldW SCENE_FUNCTION_TERMINATE scene_bind_terminate
DefineScene scene_bind
