.include "cpctelera.h.s"
.include "utils.h.s"
.include "assets/assets.h.s"
.include "helper/keyboard.h.s"
.include "helper/sound.h.s"
.include "lib/wait.h.s"
.include "lib/ui.h.s"
.include "ui/elevator.h.s"
.include "man/scene.h.s"
.include "man/interruption.h.s"
.include "cfg.h.s"

waitTime = 4

scene_elevator_init::
	ld hl, #_palette_default
	call man_interruption_setGameplayPalette
	call man_interruption_setHUDPalette

	cpctm_clearScreen_asm #0x00
	
	ld ix, #ui_elevator
	call bite_ui_drawComponent

	jp man_scene_sceneInitEnd



scene_elevator_start::
	jp man_scene_sceneStartEnd



scene_elevator_update::
	ld d, #waitTime
	call bite_wait_waitSeconds
	ld a, #SCENE_PHASE_TERMINATE
	call man_scene_setPhase
	ret


scene_elevator_terminate::
	jp man_scene_sceneTerminateEnd


SetFieldW SCENE_FUNCTION_INIT      scene_elevator_init
SetFieldW SCENE_FUNCTION_START     scene_elevator_start
SetFieldW SCENE_FUNCTION_UPDATE    scene_elevator_update
SetFieldW SCENE_FUNCTION_TERMINATE scene_elevator_terminate
DefineScene scene_elevator

