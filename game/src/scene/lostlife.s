.include "cpctelera.h.s"
.include "utils.h.s"
.include "assets/assets.h.s"
.include "helper/string.h.s"
.include "lib/print.h.s"
.include "lib/wait.h.s"
.include "man/scene.h.s"
.include "man/interruption.h.s"
.include "ui/lostlife.h.s"
.include "lib/ui.h.s"
.include "cfg.h.s"

waitTime = 3

waitTimeCounter: .db 0

scene_lostlife_init::
	call man_interruption_disableMusic
	
	ld hl, #_palette_hud
	call man_interruption_setGameplayPalette
	call man_interruption_setHUDPalette

	;; SET BORDER
	cpctm_setBorder_asm HW_BLACK

	cpctm_clearScreen_asm #0x00


	ld a, #waitTime
	ld (waitTimeCounter), a
	
	ld ix, #ui_lostlife
	call bite_ui_drawComponent

	;;call man_interruption_enableMusic
	
	jp man_scene_sceneInitEnd




scene_lostlife_start::
	jp man_scene_sceneStartEnd



scene_lostlife_update::
	ld a, #GAMEPLAY_CYCLES
	call bite_waitCycles
	ld a, (waitTimeCounter)
	dec a
	jr z, counterZero
		ld (waitTimeCounter), a
		ret
	counterZero:
		ld a, #SCENE_PHASE_TERMINATE
		call man_scene_setPhase
	ret


scene_lostlife_terminate::
	jp man_scene_sceneTerminateEnd


SetFieldW SCENE_FUNCTION_INIT      scene_lostlife_init
SetFieldW SCENE_FUNCTION_START     scene_lostlife_start
SetFieldW SCENE_FUNCTION_UPDATE    scene_lostlife_update
SetFieldW SCENE_FUNCTION_TERMINATE scene_lostlife_terminate
DefineScene scene_lostlife

