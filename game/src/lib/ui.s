.include "cpctelera.h.s"
.include "lib/ui.h.s"
.include "lib/print.h.s"
.include "utils.h.s"


bite_ui_drawFunctionsTable = .-2
.dw bite_ui_drawComponentText
.dw bite_ui_drawComponentBox
.dw bite_ui_drawComponentSprite
.dw bite_ui_drawComponentNode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Draws a text component
;; ----------------------------------------------------
;; PARAMS
;; HL: Pointer to the componenet
;; ----------------------------------------------------
;; RETURNS
;; HL: Pointer to the string
;; ----------------------------------------------------
;; DESTROYS
;; BC
;; ----------------------------------------------------
bite_ui_componentText_getString::
	ld bc, #UI_COMPONENT_STRING   ;;\_ Makes HL point to the start of the string
	add hl, bc                    ;;/
	ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Draws a text component
;; ----------------------------------------------------
;; PARAMS
;; IX: Pointer to the componenet
;; DE: Pointer to the screen
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; AF, BC, DE, HL
;; ----------------------------------------------------
bite_ui_drawComponentText::
	ld l, #0x7E                   ;;\_ Sets the blend mode to normal
	call cpct_setBlendMode_asm    ;;/
	
	ld b, UI_COMPONENT_Y(ix)      ;;\
	ld c, UI_COMPONENT_X(ix)      ;; |_ Gets the pointer to the screen stored on HL
	ld de, #0xC000                ;; |
	call cpct_getScreenPtr_asm    ;;/
	
	ld__d_ixh                     ;;\_ Loads the pointer to the component on DE
	ld__e_ixl                     ;;/

	ex de, hl                     ;;- Swaps HL (Screen pointer) and DE (Pointer to the component)

	ld bc, #UI_COMPONENT_STRING   ;;\_ Makes HL point to the start of the string
	add hl, bc                    ;;/

	call bite_print               ;;- Prints the string

	ld l, #0xAE                   ;;\_ Sets the blend mode to XOR
	call cpct_setBlendMode_asm    ;;/
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Draws a text component
;; ----------------------------------------------------
;; PARAMS
;; IX: Pointer to the componenet
;; DE: Pointer to the screen
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; AF, BC, DE, HL
;; ----------------------------------------------------
bite_ui_drawComponentBox::
	
	ld b, UI_COMPONENT_Y(ix)      ;;\
	ld c, UI_COMPONENT_X(ix)      ;; |_ Gets the pointer to the screen stored on HL
	ld de, #0xC000                ;; |
	call cpct_getScreenPtr_asm    ;;/
	
	ex de, hl
	
	xor a
	ld c, UI_COMPONENT_WIDTH(ix)
	cp c
	ret z
	ld b, UI_COMPONENT_HEIGHT(ix)
	cp b
	ret z
	ld a, UI_COMPONENT_COLOR(ix)
	call cpct_drawSolidBox_asm
	ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Draws a text component
;; ----------------------------------------------------
;; PARAMS
;; IX: Pointer to the componenet
;; DE: Pointer to the screen
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; AF, BC, DE, HL
;; ----------------------------------------------------
bite_ui_drawComponentSprite::
	ld l, #0x7E
	call cpct_setBlendMode_asm
	
	ld b, UI_COMPONENT_Y(ix)      ;;\
	ld c, UI_COMPONENT_X(ix)      ;; |_ Gets the pointer to the screen stored on HL
	ld de, #0xC000                ;; |
	call cpct_getScreenPtr_asm    ;;/
	
	ex de, hl

	ld b, UI_COMPONENT_WIDTH(ix)
	ld c, UI_COMPONENT_HEIGHT(ix)
	ld h, UI_COMPONENT_SPRITE_H(ix)
	ld l, UI_COMPONENT_SPRITE_L(ix)
	call cpct_drawSpriteBlended_asm
	ld l, #0xAE
	call cpct_setBlendMode_asm
	ret





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Draws a text component
;; ----------------------------------------------------
;; PARAMS
;; IX: Pointer to the componenet
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; AF, BC, DE, HL
;; ----------------------------------------------------
bite_ui_drawComponentNode::
	ld__d_ixh                         ;;\_ Loads the pointer to the component on DE
	ld__e_ixl                         ;;/

	ex de, hl                         ;;- Moves the component pointer to HL
	ld bc, #UI_COMPONENT_CHILDS       ;;\_ HL now points to the childs
	add hl, bc                        ;;/

	bite_ui_drawComponentNodeLoop:
		ld e, (hl)                    ;:\
		inc hl                        ;; |_ Gets the pointer to the next child
		ld d, (hl)                    ;; |
		inc hl                        ;;/
		
		ld a, e                       ;;\
		or d                          ;;|- If the pointer is null end
		ret z                         ;;/
		
		ld__ixh_d                     ;;\_ Moves the pointer from DE to IX
		ld__ixl_e                     ;;/

		push hl                       ;;- Stores HL on the stack

		call bite_ui_drawComponent    ;;- Draw the child

		pop hl                        ;;- Recovers HL

		jr bite_ui_drawComponentNodeLoop




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Adds a child to a node
;; ----------------------------------------------------
;; PARAMS
;; IX: Pointer to the componenet
;; DE: Pointer to the child
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; HL
;; ----------------------------------------------------
bite_ui_componentNode_addChild::
	ld h, UI_COMPONENT_END_POINTER_H(ix)
	ld l, UI_COMPONENT_END_POINTER_L(ix)
	ld (hl), e
	inc hl
	ld (hl), d
	inc hl
	ld UI_COMPONENT_END_POINTER_H(ix), h
	ld UI_COMPONENT_END_POINTER_L(ix), l
	ret




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Clears all childs of a node
;; ----------------------------------------------------
;; PARAMS
;; IX: Pointer to the componenet
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; DE, HL, BC
;; ----------------------------------------------------
bite_ui_componentNode_clearChilds::
	ld__d_ixh                         ;;\_ Loads the pointer to the component on DE
	ld__e_ixl                         ;;/

	ex de, hl                         ;;- Moves the component pointer to HL
	ld bc, #UI_COMPONENT_CHILDS       ;;\_ HL now points to the childs
	add hl, bc                        ;;/

	ld UI_COMPONENT_END_POINTER_H(ix), h
	ld UI_COMPONENT_END_POINTER_L(ix), l

	ld (hl), #0x00
	inc hl
	ld (hl), #0x00

	ret




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Draws generic component
;; ----------------------------------------------------
;; PARAMS
;; IX: Pointer to the componenet
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; AF, BC, DE, HL
;; ----------------------------------------------------
bite_ui_drawComponent::
	ex de, hl
	
	ld a, UI_COMPONENT_TYPE(ix)
	ld hl, #bite_ui_drawFunctionsTable
	add a                               ;; A*=2
	add_hl_a

	ld a, (hl)
	inc hl
	ld h, (hl)
	ld l, a
	
	jp (hl)

