.include "utils.h.s"

bite_waitCycles::
	or a
	
	__wait1SecondLoop:
		ret z
		ld (__wait1SecondCounter), a
		halt
		halt
		call cpct_waitVSYNC_asm

		__wait1SecondCounter = .+1
		ld a, #0
		dec a
	jr __wait1SecondLoop

bite_wait_waitSeconds::
	__waitSecondLoop:
		ld a, #25
		call bite_waitCycles
		dec d
		ret z
	jr __waitSecondLoop

