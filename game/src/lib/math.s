.include "cpctelera.h.s"
;
; Divide 16-bit values (with 16-bit result)
; In: Divide BC by divider DE
; Out: BC = result, HL = rest
;
bite_math_div16::
    ld hl, #0
    ld a,b
    ld b, #8
Div16_Loop1:
    rla
    adc hl,hl
    sbc hl,de
    jr nc,Div16_NoAdd1
    add hl,de
Div16_NoAdd1:
    djnz Div16_Loop1
    rla
    cpl
    ld b,a
    ld a,c
    ld c,b
    ld b,#8
Div16_Loop2:
    rla
    adc hl,hl
    sbc hl,de
    jr nc,Div16_NoAdd2
    add hl,de
Div16_NoAdd2:
    djnz Div16_Loop2
    rla
    cpl
    ld b,c
    ld c,a
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; ----------------------------------------------------
;; PARAMS
;; HL: Pointer to the string
;; BC: 16 bit value
;; A: Number of digits
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; DE, HL, AF, BC, IX
;; ----------------------------------------------------
bite_math_u16ToStr::
	ld d, #48
	
	__u16ToStrFillOfZerosLoop:
		ld (hl), d
		dec a
		jr z, __u16ToStrHLToIX
		inc hl
	jr __u16ToStrFillOfZerosLoop

	__u16ToStrHLToIX:

	ex de, hl
	ld__ixh_d
	ld__ixl_e

	__u16ToStrLoop:
	xor a
	or b
	or c
	ret z
		ld de, #10
		call bite_math_div16
		ld a, #48
		add l
		ld 0(ix), a
		dec ix
	jr __u16ToStrLoop
	ret

