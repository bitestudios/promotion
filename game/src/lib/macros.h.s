.mdelete SetFieldW
.macro SetFieldW __field_name, __field_value
	.macro __FIELD_W_'__field_name
		.dw __field_value
	.endm
.endm

.mdelete SetFieldB
.macro SetFieldB __field_name, __field_value
	.macro __FIELD_B_'__field_name
		.db __field_value
	.endm
.endm


.mdelete AddFieldB
.macro AddFieldB __field_name
	__FIELD_B_'__field_name
	.mdelete __FIELD_B_'__field_name
.endm

.mdelete AddFieldW
.macro AddFieldW __field_name
	__FIELD_W_'__field_name
	.mdelete __FIELD_W_'__field_name
.endm


.mdelete OffsetInit
.macro OffsetInit
	__OffsetAux = 0
.endm

.mdelete DefineOffset
.macro DefineOffset __name, __size
	__name = __OffsetAux
	__OffsetAux = __OffsetAux + __size
.endm


.mdelete EnumInit
.macro EnumInit
	__EnumAux = 0
.endm

.mdelete DefineEnum
.macro DefineEnum __name
	__name = __EnumAux
	__EnumAux = __EnumAux + 1
.endm

