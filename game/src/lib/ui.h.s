.include "lib/macros.h.s"

.globl bite_ui_componentText_getString
.globl bite_ui_drawComponentText
.globl bite_ui_drawComponent

EnumInit
DefineEnum UI_COMPONENT_NULL
DefineEnum UI_COMPONENT_TEXT
DefineEnum UI_COMPONENT_BOX
DefineEnum UI_COMPONENT_SPRITE
DefineEnum UI_COMPONENT_NODE

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; COMPONENT TEXT MACRO DEFINITION
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.macro bite_ui_DefineComponentText __name, __text
	__name::
		.db UI_COMPONENT_TEXT
		AddFieldB UI_COMPONENT_X
		AddFieldB UI_COMPONENT_Y
		.asciz __text
.endm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; COMPONENT TEXT FIELDS OFFSETS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
OffsetInit
DefineOffset UI_COMPONENT_TYPE     , 1
DefineOffset UI_COMPONENT_X        , 1
DefineOffset UI_COMPONENT_Y        , 1
DefineOffset UI_COMPONENT_STRING   , 1

DefineOffset SIZEOF_UI_COMPONENT_TEXT, 0




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; COMPONENT BOX MACRO DEFINITION
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.macro bite_ui_DefineComponentBox __name
	__name::
		.db UI_COMPONENT_BOX
		AddFieldB UI_COMPONENT_X
		AddFieldB UI_COMPONENT_Y
		AddFieldB UI_COMPONENT_WIDTH
		AddFieldB UI_COMPONENT_HEIGHT
		AddFieldB UI_COMPONENT_COLOR
.endm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; COMPONENT BOX FIELDS OFFSETS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
OffsetInit
DefineOffset UI_COMPONENT_TYPE     , 1
DefineOffset UI_COMPONENT_X        , 1
DefineOffset UI_COMPONENT_Y        , 1
DefineOffset UI_COMPONENT_WIDTH    , 1
DefineOffset UI_COMPONENT_HEIGHT   , 1
DefineOffset UI_COMPONENT_COLOR    , 1

DefineOffset SIZEOF_UI_COMPONENT_BOX, 0



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; COMPONENT SPRITE MACRO DEFINITION
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.macro bite_ui_DefineComponentSprite __name
	__name::
		.db UI_COMPONENT_SPRITE
		AddFieldB UI_COMPONENT_X
		AddFieldB UI_COMPONENT_Y
		AddFieldB UI_COMPONENT_WIDTH
		AddFieldB UI_COMPONENT_HEIGHT
		AddFieldW UI_COMPONENT_SPRITE
.endm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; COMPONENT BOX FIELDS OFFSETS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
OffsetInit
DefineOffset UI_COMPONENT_TYPE     , 1
DefineOffset UI_COMPONENT_X        , 1
DefineOffset UI_COMPONENT_Y        , 1
DefineOffset UI_COMPONENT_WIDTH    , 1
DefineOffset UI_COMPONENT_HEIGHT   , 1
DefineOffset UI_COMPONENT_SPRITE_L , 1
DefineOffset UI_COMPONENT_SPRITE_H , 1

DefineOffset SIZEOF_UI_COMPONENT_SPRITE, 0




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; COMPONENT NODE MACRO DEFINITION
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.macro bite_ui_DefineComponentNodeInit __name
	__name::
		.db UI_COMPONENT_NODE
		.dw __endPointer'__name
.endm

.macro bite_ui_DefineComponentNodeAddChild __child
	.dw __child
.endm

.macro bite_ui_DefineComponentNodeReserveChilds __name, __num
	__endPointer'__name:
	.rept __num
		.dw 0x0000
	.endm
	.dw 0x0000
.endm

.macro bite_ui_DefineComponentNodeEnd __name
	__endPointer'__name:
	.dw 0x0000
.endm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; COMPONENT NODE FIELDS OFFSETS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
OffsetInit
DefineOffset UI_COMPONENT_TYPE          , 1
DefineOffset UI_COMPONENT_END_POINTER_L , 1
DefineOffset UI_COMPONENT_END_POINTER_H , 1
DefineOffset UI_COMPONENT_CHILDS        , 1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; COMPONENT NODE METHODS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.globl bite_ui_componentNode_addChild
.globl bite_ui_componentNode_clearChilds


