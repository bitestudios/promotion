.globl cpct_drawSpriteBlended_asm




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Sets the font to use
;; ----------------------------------------------------
;; PARAMS
;; HL: Pointer to the font
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; ----------------------------------------------------
bite_setFont::
	ld (_bite_font), hl
	ret
	



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Sets the size of the font
;; ----------------------------------------------------
;; PARAMS
;; B: Width (in bytes)
;; C: Height
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; ----------------------------------------------------
bite_setFontSize::
	ld (_bite_fontSize), bc
	ret




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Prints a char on the screen
;; ----------------------------------------------------
;; PARAMS
;; BC: char value
;; DE: Video memory position
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; AF, BC, DE, HL
;; ----------------------------------------------------
bite_putc::
	_bite_font = .+1
	ld hl, #0x0000 ;; Loads chars pointers array on hl

	add hl, bc     ;;\
	add hl, bc     ;;- Points to the corresponding character adding the character value to hl

	ld a, (hl)     ;;\
	inc hl         ;;\
	ld h, (hl)     ;;- Stores the char sprite pointer on hl
	ld l, a        ;;/
	
	_bite_fontSize = .+1
	ld bc, #0x0000 ;; Stores the sprite size on bc

	call cpct_drawSpriteBlended_asm ;; Draws the char sprite
	ret





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Prints a string on the screen
;; ----------------------------------------------------
;; PARAMS
;; HL: string pointer
;; DE: video memory position
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; AF, BC, DE, HL
;; ----------------------------------------------------
bite_print::
	xor a          ;; Sets a to 0

	ld c, (hl)     ;; Load character value on C

	cp c           ;;- If C is a string terminator (0) ret
	ret z          ;;/
	
	ld b, a        ;; Sets b to 0, Now BC is the character value

	push hl        ;; Stores the pointer to the string on the stack
	push de        ;; Stores the video memory pointer on the stack

	call bite_putc ;; Prints the char

	pop hl         ;; Restores the video memory pointer on hl

	inc hl         ;;\
	inc hl         ;;- Displaces the video memory pointer 3 bytes

	ex de, hl      ;; Moves the video memory pointer to de

	pop hl         ;; Restores the pointer to the string on hl
	inc hl         ;; Points to the next character on the string

	jr bite_print  ;; Prints again

