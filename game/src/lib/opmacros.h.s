
.mdelete __genShifts
.macro __genShifts __leftReg, __rightReg

	.mdelete shiftl_'__leftReg'__rightReg
	.macro shiftl_'__leftReg'__rightReg __displacements
		.rept __displacements
			sla __rightReg
			rl __leftReg
		.endm
	.endm
	
	.mdelete shiftr_'__leftReg'__rightReg
	.macro shiftr_'__leftReg'__rightReg __displacements
		.rept __displacements
			srl __leftReg
			rr __rightReg
		.endm
	.endm

.endm


__genshifts b,c
__genshifts d,e
__genshifts h,l

.mdelete shiftl_a
.macro shiftl_a __displacements
    .rept __displacements
        rra
        and #0b01111111         ;; Reset incomint bit from carry to bit 7
    .endm
.endm

.mdelete shiftr_a
.macro shiftr_a __displacements
    .rept __displacements
        rra
        and #0b01111111         ;; Reset incomint bit from carry to bit 7
    .endm
.endm



.mdelete shiftl_a_b
.macro shiftl_a_b ?__loopTag
	__loopTag:
	sla a
	dec b
	jr nz, __loopTag
.endm


.mdelete ld_neg_bc_a
.macro ld_neg_bc_a ?__negative, ?__end
	or a
	jp m, __negative
		ld c, a
		ld b, #0
	jp __end
	__negative:
		ld c, a
		ld b, #0xFF
	__end:
.endm


.mdelete clamp_a
.macro clamp_a __min, __max, ?__notMin, ?__notMax
	cp #__min
	jp p, __notMin
		ld a, #__min
	__notMin:
	cp #__max
	jp m, __notMax
		ld a, #__max
	__notMax:
.endm


.mdelete clamp_a_min_0_max_b
.macro clamp_a_min_0_max_b ?__notMin, ?__notMax
	or a
	jp p, __notMin
		xor a
		jr __notMax
	__notMin:
	cp b
	jr c, __notMax
		ld a, b
	__notMax:
.endm


.mdelete clamp_a_min_b_max_0
.macro clamp_a_min_b_max_0 ?__notMin, ?__notMax
	or a
	jp m, __notMax
		xor a
		jr __notMin
	__notMax:
	cp b
	jr nc, __notMin
		ld a, b
	__notMin:
.endm

;; Calculates the modulus on A
.mdelete modulus
.macro modulus _number
    and #_number-1
.endm

; ;; Increments the registry x times
; .mdelete _genIncrements
; .macro _genIncrements _reg
;     .mdelete increment
;     .macro increment_'_reg _x
;         .rept _x
;             inc _reg
        

;; Calculates the absolute value of A
.mdelete absA
.macro absA ?__endTagName
    or a
    jp p, __endTagName
    neg         ;or you can use      cpl \ inc a
__endTagName:
    ; nop
.endm

;; Calls to the hl direction
;; destroys DE
.mdelete call_hl
.macro call_hl ?__afterCallDir
	ld de, # __afterCallDir
	push de
	jp (hl)
	__afterCallDir:
.endm

