;;PALETTE
.globl _palette_00
.globl _palette_01
.globl _palette_02
.globl _palette_03
.globl _palette_04
.globl _palette_default
.globl _palette_title
.globl _palette_hud


;;;;;;;;;;;;;;;;;;;;;;;;
;;HUD
;;;;;;;;;;;;;;;;;;;;;;;;

.globl _pauseButton
PAUSE_BUTTON_WIDTH = 10/2
PAUSE_BUTTON_HEIGH = 12

.globl _playButton
PLAY_BUTTON_WIDTH = 10/2
PLAY_BUTTON_HEIGH = 12

.globl _emptyPowerUp
EMPTY_POWER_UP_WIDTH = 10/2
EMPTY_POWER_UP_HEIGH = 12

.globl _speaker
SPEAKER_WIDTH = 14/2
SPEAKER_HEIGH = 16

.globl _bossPlaceholderTop
BOSS_PLACEHOLDER_TOP_WIDTH = 40/2
BOSS_PLACEHOLDER_TOP_HEIGH = 6

.globl _bossPlaceholderBot
BOSS_PLACEHOLDER_BOT_WIDTH = 40/2
BOSS_PLACEHOLDER_BOT_HEIGH = 6

.globl _statusBar
STATUS_BAR_WIDTH = 22/2
STATUS_BAR_HEIGH = 7

 .globl _hudAmstrad
HUD_AMSTRAD_WIDTH = 38/2
HUD_AMSTRAD_HEIGH = 6

;;;;;;;;;;;;;;;;;;;
;;SPRITES
;;;;;;;;;;;;;;;;;;;
.globl _indicator_0
.globl _indicator_1
INDICATOR_WIDTH = 8/2
INDICATOR_HEIGHT = 8


.globl _spikes
SPIKES_WIDTH = 16/2
SPIKES_HEIGHT = 20

.globl _lollipop
LOLLIPOP_WIDTH = 6/2
LOLLIPOP_HEIGHT = 14


.globl _faceapolo
FACE_APOLO_WIDTH = 10/2
FACE_APOLO_HEIGHT = 10

.globl _sprite_player_00
.globl _sprite_player_01
.globl _sprite_player_02
.globl _sprite_player_03
.globl _sprite_player_04
.globl _sprite_player_05
.globl _sprite_player_06
.globl _sprite_player_07
.globl _sprite_player_08
.globl _sprite_player_09
.globl _sprite_player_10
.globl _sprite_player_11
.globl _sprite_player_12
.globl _sprite_player_13
.globl _sprite_player_14
.globl _sprite_player_15
.globl _sprite_player_16
.globl _sprite_player_17
.globl _sprite_player_18
.globl _sprite_player_19
.globl _sprite_player_20
.globl _sprite_player_21
.globl _sprite_player_22
.globl _sprite_player_23
.globl _sprite_player_24
.globl _sprite_player_25
.globl _sprite_player_26
SPRITE_PLAYER_HEIGHT = 16
SPRITE_PLAYER_WIDTH = 4

.globl _sprite_minions_00
.globl _sprite_minions_01
.globl _sprite_minions_02
.globl _sprite_minions_03
.globl _sprite_minions_04
.globl _sprite_minions_05
.globl _sprite_minions_06
.globl _sprite_minions_07
.globl _sprite_minions_08
.globl _sprite_minions_09
.globl _sprite_minions_10
.globl _sprite_minions_11
.globl _sprite_minions_12
.globl _sprite_minions_13
.globl _sprite_minions_14
.globl _sprite_minions_15
.globl _sprite_minions_16
.globl _sprite_minions_17
.globl _sprite_minions_18
.globl _sprite_minions_19
.globl _sprite_minions_20
.globl _sprite_minions_21
.globl _sprite_minions_22
.globl _sprite_minions_23
SPRITE_MINIONS_HEIGHT = 15
SPRITE_MINIONS_WIDTH = 4

.globl _sprite_rat_0
.globl _sprite_rat_1
.globl _sprite_rat_2
.globl _sprite_rat_3
.globl _sprite_rat_4
SPRITE_RAT_HEIGHT = 7
SPRITE_RAT_WIDTH = 10/2


.globl _boss1_00
.globl _boss1_01
.globl _boss1_02
.globl _boss1_03
.globl _boss1_04
.globl _boss1_05
.globl _boss1_06
.globl _boss1_07
.globl _boss1_08
.globl _boss1_09
.globl _boss1_10
SPRITE_BOSS1_HEIGHT = 22
SPRITE_BOSS1_WIDTH = 14/2

.globl _boss2_0
.globl _boss2_1
.globl _boss2_2
.globl _boss2_3
.globl _boss2_4
.globl _boss2_5
.globl _boss2_6
.globl _boss2_7
.globl _boss2_8
; .globl _boss2_09
; .globl _boss2_10
; .globl _boss2_11
; .globl _boss2_12
SPRITE_BOSS2_HEIGHT = 27
SPRITE_BOSS2_WIDTH = (70/5)/2

.globl _boss3_0
.globl _boss3_1
.globl _boss3_2
.globl _boss3_3
SPRITE_BOSS3_HEIGHT = 23
SPRITE_BOSS3_WIDTH = 14/2

.globl _boss4_00
.globl _boss4_01
.globl _boss4_02
.globl _boss4_03
.globl _boss4_04
.globl _boss4_05
.globl _boss4_06
.globl _boss4_07
.globl _boss4_08
.globl _boss4_09
.globl _boss4_10
.globl _boss4_11
.globl _boss4_12
SPRITE_BOSS4_HEIGHT = 27
SPRITE_BOSS4_WIDTH = 14/2

.globl _sprite_player_shot_0
.globl _sprite_player_shot_1
.globl _sprite_player_shot_2
.globl _sprite_player_shot_3
SPRITE_PLAYER_SHOT_HEIGHT = 4
SPRITE_PLAYER_SHOT_WIDTH = 4/2

.globl _sprite_boss_shot
SPRITE_BOSS_SHOT_HEIGHT = 8
SPRITE_BOSS_SHOT_WIDTH = 8/2

.globl _sprite_bird_0
.globl _sprite_bird_1
.globl _sprite_bird_2
.globl _sprite_bird_3
.globl _sprite_bird_4
SPRITE_BIRD_HEIGHT = 8
SPRITE_BIRD_WIDTH = 8/2

;;;;;;;;;;;;;;;;;;;
;;TILEMAPS
;;;;;;;;;;;;;;;;;;;
TILEMAP_CENTER_W = 16
TILEMAP_CENTER_H = 15

TILEMAP_TOP_W    = 20
TILEMAP_TOP_H    = 2

TILEMAP_MARGIN_W    = 2
TILEMAP_MARGIN_H    = 15


.globl _tile_000


;;;;;;;;;;;;;;;;;;;
;;FONT
;;;;;;;;;;;;;;;;;;;
.globl _font


;;;;;;;;;;;;;;;;;;;
;;SOUND
;;;;;;;;;;;;;;;;;;;
.globl _PLY_AKM_INIT
.globl _PLY_AKM_PLAY
.globl _PLY_AKM_STOP
.globl _PLY_AKM_INITSOUNDEFFECTS
.globl _PLY_AKM_PLAYSOUNDEFFECT
.globl _MUSIC_MAIN_THEME_START
.globl _MUSIC_MAIN_THEME_EXTENDED_START
.globl _MUSIC_BOSS_START
.globl _MUSIC_WIN_START
.globl _MUSIC_GAMEOVER_START
.globl _MUSIC_PLATFORMS_START
.globl _MUSIC_SEWERS_START
.globl _MUSIC_EMPTY_START
.globl _MUSIC_SLOT_MACHINE_START
.globl _MUSIC_BOSS_DOOR_START
.globl _SFX_SOUNDEFFECTS

SFX_SHOOT = 1
SFX_SHOOT_BOSS = 2
SFX_HEALTH = 3
SFX_MAN_LAUGH = 4
SFX_EXPLOSION = 5
SFX_SLOT_MACHINE = 6
SFX_JUMP = 7
SFX_FALL = 8
SFX_OPEN_DOOR = 9
SFX_ELEVATOR = 10

