.globl cpct_disableFirmware_asm
.globl cpct_setPALColour_asm
.globl cpct_setVideoMode_asm
.globl cpct_getScreenPtr_asm
.globl cpct_drawSolidBox_asm
.globl cpct_waitVSYNC_asm
.globl cpct_scanKeyboard_if_asm
.globl cpct_isKeyPressed_asm
.globl cpct_isAnyKeyPressed_asm
.globl cpct_setPalette_asm
.globl cpct_drawSpriteBlended_asm
.globl cpct_setBlendMode_asm
.globl cpct_etm_setDrawTilemap4x8_ag_asm
.globl cpct_etm_drawTilemap4x8_ag_asm
.globl cpct_zx7b_decrunch_s_asm
.globl _cpct_keyboardStatusBuffer
.globl cpct_getRandom_lcg_u8_asm

TOTAL_TILE_DIVISION_DISPLACEMENTS_X = 2
TOTAL_TILE_DIVISION_DISPLACEMENTS_Y = 3
TILE_WIDTH_IN_BYTES = 4
TILE_HEIGHT = 8
PIXELS_PER_BYTE = 2

