.include "assets/assets.h.s"

START_BUFFERS = 0x40

.macro addBuffer __name, __size
	__name = START_BUFFERS
	START_BUFFERS = START_BUFFERS + __size
	__name'End = START_BUFFERS-1
.endm

addBuffer entitiesArray, 1600

addBuffer sys_render_eraseQueue , 2+6*25
addBuffer sys_render_drawQueue , 2+6*25

addBuffer game_score , 2
addBuffer game_scorePrev , 2
addBuffer man_game_timer , 2
addBuffer man_game_timerStopped , 1
;; 16*15
addBuffer game_tilemapCenter , TILEMAP_CENTER_W*TILEMAP_CENTER_H
;; 2*20
addBuffer game_tilemapTop , TILEMAP_TOP_W*TILEMAP_TOP_H
;; 2*15
addBuffer game_tilemapMarginLeft , TILEMAP_MARGIN_W*TILEMAP_MARGIN_H
;; 2*15
addBuffer game_tilemapMarginRight , TILEMAP_MARGIN_W*TILEMAP_MARGIN_H

