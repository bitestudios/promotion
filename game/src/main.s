.include "cpctelera.h.s"
.include "man/game.h.s"
.include "utils.h.s"
.include "assets/assets.h.s"
.include "helper/string.h.s"
.include "helper/keyboard.h.s"
.include "lib/print.h.s"
.include "man/scene.h.s"
.include "man/interruption.h.s"
.include "lib/opmacros.h.s"


.area _DATA

.area _CODE


_main::
	;; Disable firmware to prevent it from interfering with string drawing
	call cpct_disableFirmware_asm
	ld sp, #0xC000-1
	
	ld c, #0
	call cpct_setVideoMode_asm
	
	ld hl, #_SFX_SOUNDEFFECTS    ;Address of the sound effects.
	call _PLY_AKM_INITSOUNDEFFECTS
	
	
	call man_interruption_init
	
	ld hl, #_font-48*2
	call bite_setFont
	ld b, #2
	ld c, #8
	call bite_setFontSize
	

	call man_game_init
	call man_game_play
	call man_game_terminate
	jr _main
	ret


