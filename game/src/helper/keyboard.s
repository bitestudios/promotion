.include "utils.h.s"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Wait until no key is pressed
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; AF, BC, DE, HL
;; ----------------------------------------------------
helper_keyboard_waitNoKeysPressed::
	call cpct_isAnyKeyPressed_asm
	jr nz, helper_keyboard_waitNoKeysPressed
	ret

