.include"lib/print.h.s"
CHAR_HEIGHT = 8
CHAR_WIDTH_M0 = 3
CHAR_WIDTH_M1 = 2


.macro helper_stringm_defineString __name, __lenght
	__name'_length = __lenght
	__name'_centerXM0 = SCREEN_WIDTH/2-__lenght*CHAR_WIDTH_M0/2
	__name'_centerXM1 = SCREEN_WIDTH/2-__lenght*CHAR_WIDTH_M1/2
	__name:
.endm

.macro helper_stringm_setM0 __color_bg, __color_fg
	ld hl, #__color_bg*256+__color_fg
	call cpct_setDrawCharM0_asm
.endm

.macro helper_stringm_printM0 __msg, __posY, __posX
	ld de, #0xC000   ;; DE = Pointer to start of the screen
	ld bc, #__posY*256+__posX
	call cpct_getScreenPtr_asm
	ex de, hl
	ld hl, #__msg
	call bite_print
.endm

.macro helper_stringm_setM1 __color_bg, __color_fg
	ld de, #__color_bg*256+__color_fg
	call cpct_setDrawCharM1_asm
.endm

.macro helper_stringm_printM1 __msg, __posY, __posX
	ld de, #0xC000   ;; DE = Pointer to start of the screen
	ld bc, #__posY*256+__posX
	call cpct_getScreenPtr_asm
	ld iy, #__msg
	call cpct_drawStringM1_asm
.endm


.macro digitToChar
	add #48
.endm

