.include "utils.h.s"


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Copy sprite to another memory zone
;; ----------------------------------------------------
;; PARAMS
;; HL: sprite
;; DE: memory destination
;; BC: width
;; A: height
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; BC, A, DE, HL
;; ----------------------------------------------------
helper_video_copySprite::
	push bc
	ldir
	pop bc
	dec a
	jr nz, helper_video_copySprite
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Swaps sprite and screen
;; ----------------------------------------------------
;; PARAMS
;; HL: video memory pointer
;; DE: sprite
;; C: width
;; B: height
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; HL, AF, DE, BC
;; ----------------------------------------------------

;; Buffer to make the swap possible
;;BUFFER_WIDTH = 64
;;BUFFER_HEIGHT = 64
;;renderBuffer: .ds BUFFER_WIDTH*BUFFER_HEIGHT
;;
;;helper_video_swapScreenSprite::
;;	push hl ;; memory
;;	push de ;; sprite
;;	push bc ;; width, height
;;
;;	ld de, #renderBuffer
;;	call cpct_getScreenToSprite_asm
;;
;;	pop bc ;; width, height
;;	pop hl ;; Sprite
;;	pop de ;; memory
;;
;;	push bc ;; width, height
;;	push hl ;; Sprite
;;
;;    call cpct_drawSprite_asm
;;
;;	ld hl, #renderBuffer ;; memory
;;	pop de ;; Sprite
;;	pop bc ;; width, height
;;	ld a, b
;;	ld b, #0
;;
;;	call helper_video_copySprite
;;	ret

