.include "assets/assets.h.s"

.macro helper_soundm_playSFXFull __SFXID
    ld a,#__SFXID  ;Sound effect number (>=1)
    ld c,#0  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
    ld a,#__SFXID  ;Sound effect number (>=1)
    ld c,#2  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
.endm

.macro helper_soundm_playSFXFull_a
	push af
	push af
    ld c,#0  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
    pop af
    ld c,#2  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
    pop af
    ld c,#1  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
.endm

.macro helper_soundm_playSFX_0 __SFXID
    ld a,#__SFXID  ;Sound effect number (>=1)
    ld c,#2  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
.endm

.macro helper_soundm_playSFX_1 __SFXID
    ld a,#__SFXID  ;Sound effect number (>=1)
    ld c,#2  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
.endm

.macro helper_soundm_playSFX_2 __SFXID
    ld a,#__SFXID  ;Sound effect number (>=1)
    ld c,#2  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
.endm

.macro helper_soundm_setSong __song
	;1 - Initializes the music.
    ld hl,#__song    ;The music.
    ld a, #0         ;The Subsong to play (>=0).
	di
    call _PLY_AKM_INIT
	ei
.endm

