.include "lib/ui.h.s"
.include "assets/assets.h.s"

SetFieldB UI_COMPONENT_Y       , 95
SetFieldB UI_COMPONENT_X       , 33
SetFieldB UI_COMPONENT_WIDTH   , FACE_APOLO_WIDTH
SetFieldB UI_COMPONENT_HEIGHT  , FACE_APOLO_HEIGHT
SetFieldW UI_COMPONENT_SPRITE  , #_faceapolo
bite_ui_DefineComponentSprite __ui_lostLife_faceapolo

SetFieldB UI_COMPONENT_Y       , 96
SetFieldB UI_COMPONENT_X       , 39
bite_ui_DefineComponentText ui_lostlife_remainingLives, "X3"

bite_ui_DefineComponentNodeInit ui_lostlife
bite_ui_DefineComponentNodeAddChild __ui_lostLife_faceapolo
bite_ui_DefineComponentNodeAddChild ui_lostlife_remainingLives
bite_ui_DefineComponentNodeEnd ui_lostlife

