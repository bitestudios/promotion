.globl ui_hud_bg
.globl ui_hud
.globl ui_hudUpdated

.globl ui_hud_playButton
.globl ui_hud_pauseButton
.globl ui_hud_playOrPauseButton

.globl ui_hud_healthBarFillPlayer
.globl ui_hud_healthBarPlayer
.globl ui_hud_energyBarFillPlayer
.globl ui_hud_energyBarPlayer
.globl ui_hud_remainingLives
.globl ui_hud_healthBarFillBoss
.globl ui_hud_healthBarBoss
.globl ui_hud_numberOfBars

.globl ui_hud_floorNode
.globl ui_hud_floorDefault
.globl ui_hud_floor0
.globl ui_hud_floor1
.globl ui_hud_floor2
.globl ui_hud_floor3
.globl ui_hud_floor4
.globl ui_hud_floor5

.globl ui_hud_bossNode
.globl ui_hud_bossNameEmpty
.globl ui_hud_bossName0
.globl ui_hud_bossName1
.globl ui_hud_bossName2
.globl ui_hud_bossName3

.globl ui_hud_timer

.globl ui_hud_scoreboardDigits

