.include "lib/ui.h.s"
.include "assets/assets.h.s"

SetFieldB UI_COMPONENT_Y       , 0
SetFieldB UI_COMPONENT_X       , 0
SetFieldB UI_COMPONENT_WIDTH   , 1
SetFieldB UI_COMPONENT_HEIGHT  , 20
SetFieldB UI_COMPONENT_COLOR   , 0xFF
bite_ui_DefineComponentBox __leftUpV

SetFieldB UI_COMPONENT_Y       , 0
SetFieldB UI_COMPONENT_X       , 0
SetFieldB UI_COMPONENT_WIDTH   , 10
SetFieldB UI_COMPONENT_HEIGHT  , 4
SetFieldB UI_COMPONENT_COLOR   , 0xFF
bite_ui_DefineComponentBox __leftUpH

SetFieldB UI_COMPONENT_Y       , 0
SetFieldB UI_COMPONENT_X       , 79
SetFieldB UI_COMPONENT_WIDTH   , 1
SetFieldB UI_COMPONENT_HEIGHT  , 20
SetFieldB UI_COMPONENT_COLOR   , 0xFF
bite_ui_DefineComponentBox __rightUpV

SetFieldB UI_COMPONENT_Y       , 0
SetFieldB UI_COMPONENT_X       , 70
SetFieldB UI_COMPONENT_WIDTH   , 10
SetFieldB UI_COMPONENT_HEIGHT  , 4
SetFieldB UI_COMPONENT_COLOR   , 0xFF
bite_ui_DefineComponentBox __rightUpH

SetFieldB UI_COMPONENT_Y       , 179
SetFieldB UI_COMPONENT_X       , 0
SetFieldB UI_COMPONENT_WIDTH   , 1
SetFieldB UI_COMPONENT_HEIGHT  , 20
SetFieldB UI_COMPONENT_COLOR   , 0xFF
bite_ui_DefineComponentBox __leftDownV

SetFieldB UI_COMPONENT_Y       , 196
SetFieldB UI_COMPONENT_X       , 0
SetFieldB UI_COMPONENT_WIDTH   , 10
SetFieldB UI_COMPONENT_HEIGHT  , 4
SetFieldB UI_COMPONENT_COLOR   , 0xFF
bite_ui_DefineComponentBox __leftDownH

SetFieldB UI_COMPONENT_Y       , 180
SetFieldB UI_COMPONENT_X       , 79
SetFieldB UI_COMPONENT_WIDTH   , 1
SetFieldB UI_COMPONENT_HEIGHT  , 20
SetFieldB UI_COMPONENT_COLOR   , 0xFF
bite_ui_DefineComponentBox __rightDownV

SetFieldB UI_COMPONENT_Y       , 196
SetFieldB UI_COMPONENT_X       , 70
SetFieldB UI_COMPONENT_WIDTH   , 10
SetFieldB UI_COMPONENT_HEIGHT  , 4
SetFieldB UI_COMPONENT_COLOR   , 0xFF
bite_ui_DefineComponentBox __rightDownH

SetFieldB UI_COMPONENT_Y       , 15
SetFieldB UI_COMPONENT_X       , 32
bite_ui_DefineComponentText __promotion, "PROMOTION"

SetFieldB UI_COMPONENT_Y       , 50
SetFieldB UI_COMPONENT_X       , 4
bite_ui_DefineComponentText __keyboard, "TO<USE<JOYSTICK<PLEASE<BIND<CONTROLS"

; SetFieldB UI_COMPONENT_Y       , 70
; SetFieldB UI_COMPONENT_X       , 30
; bite_ui_DefineComponentText __joystick, "2<JOYSTICK"

SetFieldB UI_COMPONENT_Y       , 65
SetFieldB UI_COMPONENT_X       , 21
bite_ui_DefineComponentText __play, ^/"PRESS<fire<TO<play;"/

SetFieldB UI_COMPONENT_Y       , 90
SetFieldB UI_COMPONENT_X       , 4
bite_ui_DefineComponentText __bind_controls, "PRESS<c<TO<REDEFINE<KEYS<OR<JOYSTICK"

SetFieldB UI_COMPONENT_Y       , 150
SetFieldB UI_COMPONENT_X       , 29
bite_ui_DefineComponentText __code1, "CODE:<tonet"

SetFieldB UI_COMPONENT_Y       , 160
SetFieldB UI_COMPONENT_X       , 25
bite_ui_DefineComponentText __code2, "CODE:<prototano"

SetFieldB UI_COMPONENT_Y       , 170
SetFieldB UI_COMPONENT_X       , 20
bite_ui_DefineComponentText __music, "MUSIC<AND<ART:<salva"

SetFieldB UI_COMPONENT_Y       , 184
SetFieldB UI_COMPONENT_X       , 2
bite_ui_DefineComponentText __bitestudios, "bitestudios2020"

bite_ui_DefineComponentNodeInit ui_title
bite_ui_DefineComponentNodeAddChild __leftUpV
bite_ui_DefineComponentNodeAddChild __leftUpH
bite_ui_DefineComponentNodeAddChild __rightUpV
bite_ui_DefineComponentNodeAddChild __rightUpH
bite_ui_DefineComponentNodeAddChild __leftDownV
bite_ui_DefineComponentNodeAddChild __leftDownH
bite_ui_DefineComponentNodeAddChild __rightDownV
bite_ui_DefineComponentNodeAddChild __rightDownH
bite_ui_DefineComponentNodeAddChild __promotion
; bite_ui_DefineComponentNodeAddChild __keyboard
; bite_ui_DefineComponentNodeAddChild __joystick
bite_ui_DefineComponentNodeAddChild __play
bite_ui_DefineComponentNodeAddChild __bind_controls
bite_ui_DefineComponentNodeAddChild __code1
bite_ui_DefineComponentNodeAddChild __code2
bite_ui_DefineComponentNodeAddChild __music
bite_ui_DefineComponentNodeAddChild __bitestudios
bite_ui_DefineComponentNodeEnd ui_title

