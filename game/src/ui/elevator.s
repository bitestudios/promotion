.include "lib/ui.h.s"
.include "lib/colors.h.s"
.include "assets/assets.h.s"

SetFieldB UI_COMPONENT_Y       , 96
SetFieldB UI_COMPONENT_X       , 32
bite_ui_DefineComponentText ui_elevator_waitPlease, "GOING<UP"

SetFieldB UI_COMPONENT_Y       , 106
SetFieldB UI_COMPONENT_X       , 38
SetFieldB UI_COMPONENT_WIDTH   , SPRITE_PLAYER_WIDTH
SetFieldB UI_COMPONENT_HEIGHT  , SPRITE_PLAYER_HEIGHT
SetFieldW UI_COMPONENT_SPRITE  , #_sprite_player_00
bite_ui_DefineComponentSprite ui_elevator_player

bite_ui_DefineComponentNodeInit ui_elevator
bite_ui_DefineComponentNodeAddChild ui_elevator_waitPlease
bite_ui_DefineComponentNodeAddChild ui_elevator_player
bite_ui_DefineComponentNodeEnd ui_elevator

