.include "lib/ui.h.s"
.include "lib/colors.h.s"
.include "assets/assets.h.s"

SetFieldB UI_COMPONENT_Y       , 55
SetFieldB UI_COMPONENT_X       , 19
bite_ui_DefineComponentText ui_end_msg, ^/"YOU<ARE<THE<NEW<BOSS;"/

SetFieldB UI_COMPONENT_Y       , 112
SetFieldB UI_COMPONENT_X       , 38
SetFieldB UI_COMPONENT_WIDTH   , SPRITE_PLAYER_WIDTH
SetFieldB UI_COMPONENT_HEIGHT  , SPRITE_PLAYER_HEIGHT
SetFieldW UI_COMPONENT_SPRITE  , #_sprite_player_00
bite_ui_DefineComponentSprite ui_end_player

SetFieldB UI_COMPONENT_Y       , 155
SetFieldB UI_COMPONENT_X       , 31
bite_ui_DefineComponentText ui_end_gameOver, "GAME<OVER"

SetFieldB UI_COMPONENT_Y       , 170
SetFieldB UI_COMPONENT_X       , 15
bite_ui_DefineComponentText ui_end_pressAnyKey, "PRESS<any<key<TO<CONTINUE"


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; SCOREBOARD
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SetFieldB UI_COMPONENT_Y       , 84
SetFieldB UI_COMPONENT_X       , 29
bite_ui_DefineComponentText ui_end_scoreboardTxt, "score:"

SetFieldB UI_COMPONENT_Y       , 84
SetFieldB UI_COMPONENT_X       , 41
bite_ui_DefineComponentText ui_end_scoreboardDigits, "00000"


bite_ui_DefineComponentNodeInit ui_end
bite_ui_DefineComponentNodeAddChild ui_end_msg
bite_ui_DefineComponentNodeAddChild ui_end_player
bite_ui_DefineComponentNodeAddChild ui_end_gameOver
bite_ui_DefineComponentNodeAddChild ui_end_pressAnyKey
bite_ui_DefineComponentNodeAddChild ui_end_scoreboardTxt
bite_ui_DefineComponentNodeAddChild ui_end_scoreboardDigits
bite_ui_DefineComponentNodeEnd ui_end

