.include "lib/ui.h.s"
.include "assets/assets.h.s"
.include "lib/colors.h.s"

SetFieldB UI_COMPONENT_Y       , 15
SetFieldB UI_COMPONENT_X       , 9
SetFieldB UI_COMPONENT_WIDTH   , 62
SetFieldB UI_COMPONENT_HEIGHT  , 64
SetFieldB UI_COMPONENT_COLOR   , COLOR_02_M0
bite_ui_DefineComponentBox __topDinA4

SetFieldB UI_COMPONENT_Y       , 79
SetFieldB UI_COMPONENT_X       , 9
SetFieldB UI_COMPONENT_WIDTH   , 62
SetFieldB UI_COMPONENT_HEIGHT  , 64
SetFieldB UI_COMPONENT_COLOR   , COLOR_03_M0
bite_ui_DefineComponentBox __middleDinA4

SetFieldB UI_COMPONENT_Y       , 143
SetFieldB UI_COMPONENT_X       , 9
SetFieldB UI_COMPONENT_WIDTH   , 62
SetFieldB UI_COMPONENT_HEIGHT  , 42
SetFieldB UI_COMPONENT_COLOR   , COLOR_04_M0
bite_ui_DefineComponentBox __bottomDinA4

SetFieldB UI_COMPONENT_Y       , 28
SetFieldB UI_COMPONENT_X       , 27
SetFieldB UI_COMPONENT_WIDTH   , FACE_APOLO_WIDTH
SetFieldB UI_COMPONENT_HEIGHT  , FACE_APOLO_HEIGHT
SetFieldW UI_COMPONENT_SPRITE  , #_faceapolo
bite_ui_DefineComponentSprite __faceapolo

SetFieldB UI_COMPONENT_Y       , 40
SetFieldB UI_COMPONENT_X       , 38
bite_ui_DefineComponentText __name, "NAME:APOLO"

SetFieldB UI_COMPONENT_Y       , 50
SetFieldB UI_COMPONENT_X       , 38
bite_ui_DefineComponentText __status, "STATUS:EMPLOYED"

SetFieldB UI_COMPONENT_Y       , 70
SetFieldB UI_COMPONENT_X       , 28
bite_ui_DefineComponentText __contract, "CONTRACT"

SetFieldB UI_COMPONENT_Y       , 80
SetFieldB UI_COMPONENT_X       , 28
bite_ui_DefineComponentText __text1, "==========="

SetFieldB UI_COMPONENT_Y       , 90
SetFieldB UI_COMPONENT_X       , 28
bite_ui_DefineComponentText __text2, "==========="

SetFieldB UI_COMPONENT_Y       , 100
SetFieldB UI_COMPONENT_X       , 28
bite_ui_DefineComponentText __text3, "====="
                                      
SetFieldB UI_COMPONENT_Y       , 110
SetFieldB UI_COMPONENT_X       , 28
bite_ui_DefineComponentText __text4, "==========="

SetFieldB UI_COMPONENT_Y       , 120
SetFieldB UI_COMPONENT_X       , 28
bite_ui_DefineComponentText __text5, "===<10<3<2020"

SetFieldB UI_COMPONENT_Y       , 130
SetFieldB UI_COMPONENT_X       , 28
bite_ui_DefineComponentText __company, "COMPANY<SIGNATURE"

SetFieldB UI_COMPONENT_Y       , 150
SetFieldB UI_COMPONENT_X       , 28
bite_ui_DefineComponentText __holder, "HOLDER<SIGNATURE"

bite_ui_DefineComponentNodeInit ui_contractscreen
bite_ui_DefineComponentNodeAddChild __topDinA4
bite_ui_DefineComponentNodeAddChild __middleDinA4
bite_ui_DefineComponentNodeAddChild __bottomDinA4
bite_ui_DefineComponentNodeAddChild __faceapolo
bite_ui_DefineComponentNodeAddChild __name
bite_ui_DefineComponentNodeAddChild __status
bite_ui_DefineComponentNodeAddChild __contract
bite_ui_DefineComponentNodeAddChild __text1
bite_ui_DefineComponentNodeAddChild __text2
bite_ui_DefineComponentNodeAddChild __text3
bite_ui_DefineComponentNodeAddChild __text4
bite_ui_DefineComponentNodeAddChild __text5
bite_ui_DefineComponentNodeAddChild __company
bite_ui_DefineComponentNodeAddChild __holder
bite_ui_DefineComponentNodeEnd ui_contractscreen