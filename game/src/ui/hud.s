.include "lib/ui.h.s"
.include "assets/assets.h.s"
.include "lib/colors.h.s"

SetFieldB UI_COMPONENT_Y       , 140
SetFieldB UI_COMPONENT_X       , 0
SetFieldB UI_COMPONENT_WIDTH   , 40
SetFieldB UI_COMPONENT_HEIGHT  , 60
SetFieldB UI_COMPONENT_COLOR   , COLOR_08_M0   ;; 1000 1000 00000011
bite_ui_DefineComponentBox __background0

SetFieldB UI_COMPONENT_Y       , 140
SetFieldB UI_COMPONENT_X       , 40
SetFieldB UI_COMPONENT_WIDTH   , 40
SetFieldB UI_COMPONENT_HEIGHT  , 60
SetFieldB UI_COMPONENT_COLOR   , COLOR_08_M0
bite_ui_DefineComponentBox __background1

SetFieldB UI_COMPONENT_Y       , 143
SetFieldB UI_COMPONENT_X       , 9
SetFieldB UI_COMPONENT_WIDTH   , SPEAKER_WIDTH
SetFieldB UI_COMPONENT_HEIGHT  , SPEAKER_HEIGH
SetFieldW UI_COMPONENT_SPRITE  , #_speaker
bite_ui_DefineComponentSprite __speaker0

SetFieldB UI_COMPONENT_Y       , 143
SetFieldB UI_COMPONENT_X       , 20
SetFieldB UI_COMPONENT_WIDTH   , SPEAKER_WIDTH
SetFieldB UI_COMPONENT_HEIGHT  , SPEAKER_HEIGH
SetFieldW UI_COMPONENT_SPRITE  , #_speaker
bite_ui_DefineComponentSprite __speaker1

SetFieldB UI_COMPONENT_Y       , 163
SetFieldB UI_COMPONENT_X       , 42
SetFieldB UI_COMPONENT_WIDTH   , 16
SetFieldB UI_COMPONENT_HEIGHT  , 16
SetFieldB UI_COMPONENT_COLOR   , COLOR_06_M0   ;; 1000 1000 00000011
bite_ui_DefineComponentBox __hud_timeBg0

SetFieldB UI_COMPONENT_Y       , 165
SetFieldB UI_COMPONENT_X       , 43
SetFieldB UI_COMPONENT_WIDTH   , 14
SetFieldB UI_COMPONENT_HEIGHT  , 12
SetFieldB UI_COMPONENT_COLOR   , COLOR_00_M0
bite_ui_DefineComponentBox __hud_timeBg1

SetFieldB UI_COMPONENT_Y       , 167
SetFieldB UI_COMPONENT_X       , 45
bite_ui_DefineComponentText ui_hud_timeT, "T:"

SetFieldB UI_COMPONENT_Y       , 167
SetFieldB UI_COMPONENT_X       , 49
bite_ui_DefineComponentText ui_hud_timer, "000"


SetFieldB UI_COMPONENT_Y       , 146
SetFieldB UI_COMPONENT_X       , 31
bite_ui_DefineComponentText __hud_promotion, "PROMOTION"

SetFieldB UI_COMPONENT_Y       , 198-HUD_AMSTRAD_HEIGH
SetFieldB UI_COMPONENT_X       , 79-HUD_AMSTRAD_WIDTH
SetFieldB UI_COMPONENT_WIDTH   , HUD_AMSTRAD_WIDTH
SetFieldB UI_COMPONENT_HEIGHT  , HUD_AMSTRAD_HEIGH
SetFieldW UI_COMPONENT_SPRITE  , #_hudAmstrad
bite_ui_DefineComponentSprite __ui_hud_amstrad


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; BOSS NAMES
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


SetFieldB UI_COMPONENT_Y       , 150
SetFieldB UI_COMPONENT_X       , 60
SetFieldB UI_COMPONENT_WIDTH   , 20
SetFieldB UI_COMPONENT_HEIGHT  , 8
SetFieldB UI_COMPONENT_COLOR   , COLOR_13_M0   ;; 1000 1000 00000011
bite_ui_DefineComponentBox __bossNameBg0

SetFieldB UI_COMPONENT_Y       , 150
SetFieldB UI_COMPONENT_X       , 61
SetFieldB UI_COMPONENT_WIDTH   , 18
SetFieldB UI_COMPONENT_HEIGHT  , 8
SetFieldB UI_COMPONENT_COLOR   , COLOR_08_M0   ;; 1000 1000 00000011
bite_ui_DefineComponentBox __bossNameBg1

SetFieldB UI_COMPONENT_Y       , 144
SetFieldB UI_COMPONENT_X       , 60
SetFieldB UI_COMPONENT_WIDTH   , BOSS_PLACEHOLDER_TOP_WIDTH
SetFieldB UI_COMPONENT_HEIGHT  , BOSS_PLACEHOLDER_TOP_HEIGH
SetFieldW UI_COMPONENT_SPRITE  , #_bossPlaceholderTop
bite_ui_DefineComponentSprite __bossPlaceholderTop

SetFieldB UI_COMPONENT_Y       , 158
SetFieldB UI_COMPONENT_X       , 60
SetFieldB UI_COMPONENT_WIDTH   , BOSS_PLACEHOLDER_BOT_WIDTH
SetFieldB UI_COMPONENT_HEIGHT  , BOSS_PLACEHOLDER_BOT_HEIGH
SetFieldW UI_COMPONENT_SPRITE  , #_bossPlaceholderBot
bite_ui_DefineComponentSprite __bossPlaceholderBot

SetFieldB UI_COMPONENT_Y       , 150
SetFieldB UI_COMPONENT_X       , 65
bite_ui_DefineComponentText ui_hud_bossNameEmpty, ""

SetFieldB UI_COMPONENT_Y       , 150
SetFieldB UI_COMPONENT_X       , 63
bite_ui_DefineComponentText ui_hud_bossName0, "MANAGER"

SetFieldB UI_COMPONENT_Y       , 150
SetFieldB UI_COMPONENT_X       , 62
bite_ui_DefineComponentText ui_hud_bossName1, "DIRECTOR"

SetFieldB UI_COMPONENT_Y       , 150
SetFieldB UI_COMPONENT_X       , 65
bite_ui_DefineComponentText ui_hud_bossName2, "C<O<O"

SetFieldB UI_COMPONENT_Y       , 150
SetFieldB UI_COMPONENT_X       , 65
bite_ui_DefineComponentText ui_hud_bossName3, "C<E<O"

bite_ui_DefineComponentNodeInit     ui_hud_bossNode
bite_ui_DefineComponentNodeAddChild ui_hud_bossNameEmpty
bite_ui_DefineComponentNodeEnd      ui_hud_bossNode

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; PLANTS
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SetFieldB UI_COMPONENT_Y       , 163
SetFieldB UI_COMPONENT_X       , 22
SetFieldB UI_COMPONENT_WIDTH   , 16
SetFieldB UI_COMPONENT_HEIGHT  , 16
SetFieldB UI_COMPONENT_COLOR   , COLOR_13_M0
bite_ui_DefineComponentBox __hud_floorBg0

SetFieldB UI_COMPONENT_Y       , 165
SetFieldB UI_COMPONENT_X       , 23
SetFieldB UI_COMPONENT_WIDTH   , 14
SetFieldB UI_COMPONENT_HEIGHT  , 12
SetFieldB UI_COMPONENT_COLOR   , COLOR_00_M0
bite_ui_DefineComponentBox __hud_floorBg1

SetFieldB UI_COMPONENT_Y       , 167
SetFieldB UI_COMPONENT_X       , 28
bite_ui_DefineComponentText ui_hud_floorDefault, ""

SetFieldB UI_COMPONENT_Y       , 167
SetFieldB UI_COMPONENT_X       , 28
bite_ui_DefineComponentText ui_hud_floor0, "GF"

SetFieldB UI_COMPONENT_Y       , 167
SetFieldB UI_COMPONENT_X       , 28
bite_ui_DefineComponentText ui_hud_floor1, "1F"

SetFieldB UI_COMPONENT_Y       , 167
SetFieldB UI_COMPONENT_X       , 28
bite_ui_DefineComponentText ui_hud_floor2, "2F"

SetFieldB UI_COMPONENT_Y       , 167
SetFieldB UI_COMPONENT_X       , 28
bite_ui_DefineComponentText ui_hud_floor3, "3F"

SetFieldB UI_COMPONENT_Y       , 167
SetFieldB UI_COMPONENT_X       , 28
bite_ui_DefineComponentText ui_hud_floor4, "4F"

SetFieldB UI_COMPONENT_Y       , 167
SetFieldB UI_COMPONENT_X       , 28
bite_ui_DefineComponentText ui_hud_floor5, "SW"

bite_ui_DefineComponentNodeInit     ui_hud_floorNode
bite_ui_DefineComponentNodeAddChild ui_hud_floorDefault
bite_ui_DefineComponentNodeEnd      ui_hud_floorNode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  PLAY OR PAUSE BUTTONS
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SetFieldB UI_COMPONENT_Y       , 145
SetFieldB UI_COMPONENT_X       , 0
SetFieldB UI_COMPONENT_WIDTH   , PLAY_BUTTON_WIDTH
SetFieldB UI_COMPONENT_HEIGHT  , PLAY_BUTTON_HEIGH
SetFieldW UI_COMPONENT_SPRITE  , #_playButton
bite_ui_DefineComponentSprite ui_hud_playButton

SetFieldB UI_COMPONENT_Y       , 145
SetFieldB UI_COMPONENT_X       , 0
SetFieldB UI_COMPONENT_WIDTH   , PLAY_BUTTON_WIDTH
SetFieldB UI_COMPONENT_HEIGHT  , PLAY_BUTTON_HEIGH
SetFieldW UI_COMPONENT_SPRITE  , #_pauseButton
bite_ui_DefineComponentSprite ui_hud_pauseButton

bite_ui_DefineComponentNodeInit     ui_hud_playOrPauseButton
bite_ui_DefineComponentNodeAddChild ui_hud_playButton
bite_ui_DefineComponentNodeEnd      ui_hud_playOrPauseButton



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  HEALTH BAR PLAYER
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SetFieldB UI_COMPONENT_Y       , 165
SetFieldB UI_COMPONENT_X       , 0
bite_ui_DefineComponentText __hp, "HP"

SetFieldB UI_COMPONENT_Y       , 166
SetFieldB UI_COMPONENT_X       , 5
SetFieldB UI_COMPONENT_WIDTH   , STATUS_BAR_WIDTH
SetFieldB UI_COMPONENT_HEIGHT  , STATUS_BAR_HEIGH
SetFieldW UI_COMPONENT_SPRITE  , #_statusBar
bite_ui_DefineComponentSprite __healthBarEmptyPlayer

SetFieldB UI_COMPONENT_Y       , 167
SetFieldB UI_COMPONENT_X       , 6
SetFieldB UI_COMPONENT_WIDTH   , STATUS_BAR_WIDTH-3
SetFieldB UI_COMPONENT_HEIGHT  , STATUS_BAR_HEIGH-2
__COLOR_AUX = COLOR_04_M0_L | COLOR_06_M0_R
SetFieldB UI_COMPONENT_COLOR   ,  __COLOR_AUX
bite_ui_DefineComponentBox ui_hud_healthBarFillPlayer

bite_ui_DefineComponentNodeInit     ui_hud_healthBarPlayer
bite_ui_DefineComponentNodeAddChild __healthBarEmptyPlayer
bite_ui_DefineComponentNodeAddChild ui_hud_healthBarFillPlayer
bite_ui_DefineComponentNodeEnd      ui_hud_healthBarPlayer



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  ENERGY BAR PLAYER
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SetFieldB UI_COMPONENT_Y       , 174
SetFieldB UI_COMPONENT_X       , 0
bite_ui_DefineComponentText __pp, "PP"

SetFieldB UI_COMPONENT_Y       , 175
SetFieldB UI_COMPONENT_X       , 5
SetFieldB UI_COMPONENT_WIDTH   , STATUS_BAR_WIDTH
SetFieldB UI_COMPONENT_HEIGHT  , STATUS_BAR_HEIGH
SetFieldW UI_COMPONENT_SPRITE  , #_statusBar
bite_ui_DefineComponentSprite __energyBarEmptyPlayer

SetFieldB UI_COMPONENT_Y       , 176
SetFieldB UI_COMPONENT_X       , 6
SetFieldB UI_COMPONENT_WIDTH   , STATUS_BAR_WIDTH-3
SetFieldB UI_COMPONENT_HEIGHT  , STATUS_BAR_HEIGH-2
__COLOR_AUX = COLOR_04_M0_L | COLOR_11_M0_R
SetFieldB UI_COMPONENT_COLOR   ,  __COLOR_AUX
bite_ui_DefineComponentBox ui_hud_energyBarFillPlayer

bite_ui_DefineComponentNodeInit ui_hud_energyBarPlayer
bite_ui_DefineComponentNodeAddChild __energyBarEmptyPlayer
bite_ui_DefineComponentNodeAddChild ui_hud_energyBarFillPlayer
bite_ui_DefineComponentNodeEnd ui_hud_energyBarPlayer



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  PLAYER LIFES
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SetFieldB UI_COMPONENT_Y       , 189
SetFieldB UI_COMPONENT_X       , 3
SetFieldB UI_COMPONENT_WIDTH   , FACE_APOLO_WIDTH
SetFieldB UI_COMPONENT_HEIGHT  , FACE_APOLO_HEIGHT
SetFieldW UI_COMPONENT_SPRITE  , #_faceapolo
bite_ui_DefineComponentSprite ui_hud_faceapolo

SetFieldB UI_COMPONENT_Y       , 190
SetFieldB UI_COMPONENT_X       , 9
bite_ui_DefineComponentText ui_hud_remainingLives, "X3"




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  HEALTH BAR BOSS
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SetFieldB UI_COMPONENT_Y       , 169
SetFieldB UI_COMPONENT_X       , 80-4-STATUS_BAR_WIDTH-5
bite_ui_DefineComponentText __hpBoss, "HP"

SetFieldB UI_COMPONENT_Y       , 170
SetFieldB UI_COMPONENT_X       , 80-4-STATUS_BAR_WIDTH
SetFieldB UI_COMPONENT_WIDTH   , STATUS_BAR_WIDTH
SetFieldB UI_COMPONENT_HEIGHT  , STATUS_BAR_HEIGH
SetFieldW UI_COMPONENT_SPRITE  , #_statusBar
bite_ui_DefineComponentSprite __healthBarEmptyBoss

SetFieldB UI_COMPONENT_Y       , 171
SetFieldB UI_COMPONENT_X       , 80-4-STATUS_BAR_WIDTH+1
SetFieldB UI_COMPONENT_WIDTH   , STATUS_BAR_WIDTH-3
SetFieldB UI_COMPONENT_HEIGHT  , STATUS_BAR_HEIGH-2
__COLOR_AUX = COLOR_04_M0_L | COLOR_06_M0_R
SetFieldB UI_COMPONENT_COLOR   ,  __COLOR_AUX
bite_ui_DefineComponentBox ui_hud_healthBarFillBoss

SetFieldB UI_COMPONENT_Y       , 169
SetFieldB UI_COMPONENT_X       , 80-4
bite_ui_DefineComponentText ui_hud_numberOfBarsX, "X"

SetFieldB UI_COMPONENT_Y       , 169
SetFieldB UI_COMPONENT_X       , 80-2
bite_ui_DefineComponentText ui_hud_numberOfBars, "0"

bite_ui_DefineComponentNodeInit ui_hud_healthBarBoss
bite_ui_DefineComponentNodeAddChild __healthBarEmptyBoss
bite_ui_DefineComponentNodeAddChild ui_hud_healthBarFillBoss
bite_ui_DefineComponentNodeAddChild ui_hud_numberOfBarsX
bite_ui_DefineComponentNodeAddChild ui_hud_numberOfBars
bite_ui_DefineComponentNodeEnd ui_hud_healthBarBoss




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; SCOREBOARD
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SetFieldB UI_COMPONENT_Y       , 184
SetFieldB UI_COMPONENT_X       , 24
SetFieldB UI_COMPONENT_WIDTH   , 32
SetFieldB UI_COMPONENT_HEIGHT  , 16
SetFieldB UI_COMPONENT_COLOR   , COLOR_10_M0   ;; 1000 1000 00000011
bite_ui_DefineComponentBox __hud_scoreboardBg0

SetFieldB UI_COMPONENT_Y       , 186
SetFieldB UI_COMPONENT_X       , 25
SetFieldB UI_COMPONENT_WIDTH   , 30
SetFieldB UI_COMPONENT_HEIGHT  , 12
SetFieldB UI_COMPONENT_COLOR   , COLOR_00_M0
bite_ui_DefineComponentBox __hud_scoreboardBg1

SetFieldB UI_COMPONENT_Y       , 188
SetFieldB UI_COMPONENT_X       , 29
bite_ui_DefineComponentText ui_hud_scoreboardTxt, "SCORE:"

SetFieldB UI_COMPONENT_Y       , 188
SetFieldB UI_COMPONENT_X       , 41
bite_ui_DefineComponentText ui_hud_scoreboardDigits, "00000"






bite_ui_DefineComponentNodeInit ui_hud_bg
bite_ui_DefineComponentNodeAddChild __background0
bite_ui_DefineComponentNodeAddChild __background1
bite_ui_DefineComponentNodeEnd ui_hud_bg



bite_ui_DefineComponentNodeInit ui_hud
bite_ui_DefineComponentNodeAddChild ui_hud_bg
bite_ui_DefineComponentNodeAddChild ui_hud_playOrPauseButton
bite_ui_DefineComponentNodeAddChild __hud_promotion
bite_ui_DefineComponentNodeAddChild __hud_floorBg0
bite_ui_DefineComponentNodeAddChild __hud_floorBg1
bite_ui_DefineComponentNodeAddChild ui_hud_floorNode
bite_ui_DefineComponentNodeAddChild __hud_timeBg0
bite_ui_DefineComponentNodeAddChild __hud_timeBg1
bite_ui_DefineComponentNodeAddChild ui_hud_timeT
bite_ui_DefineComponentNodeAddChild ui_hud_timer
bite_ui_DefineComponentNodeAddChild __speaker0
bite_ui_DefineComponentNodeAddChild __speaker1
bite_ui_DefineComponentNodeAddChild __bossPlaceholderTop
bite_ui_DefineComponentNodeAddChild __bossPlaceholderBot
bite_ui_DefineComponentNodeAddChild __bossNameBg0
bite_ui_DefineComponentNodeAddChild __bossNameBg1
bite_ui_DefineComponentNodeAddChild ui_hud_bossNode
bite_ui_DefineComponentNodeAddChild __hp
bite_ui_DefineComponentNodeAddChild ui_hud_healthBarPlayer
bite_ui_DefineComponentNodeAddChild __pp
bite_ui_DefineComponentNodeAddChild ui_hud_energyBarPlayer
bite_ui_DefineComponentNodeAddChild __hpBoss
bite_ui_DefineComponentNodeAddChild ui_hud_healthBarBoss
bite_ui_DefineComponentNodeAddChild ui_hud_faceapolo
bite_ui_DefineComponentNodeAddChild ui_hud_remainingLives
bite_ui_definecomponentnodeaddchild __hud_scoreboardBg0
bite_ui_definecomponentnodeaddchild __hud_scoreboardBg1
bite_ui_definecomponentnodeaddchild ui_hud_scoreboardTxt
bite_ui_definecomponentnodeaddchild ui_hud_scoreboardDigits
bite_ui_DefineComponentNodeAddChild __ui_hud_amstrad
bite_ui_DefineComponentNodeEnd ui_hud


bite_ui_DefineComponentNodeInit ui_hudUpdated
bite_ui_DefineComponentNodeReserveChilds ui_hudUpdated, 10


