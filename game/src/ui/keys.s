.include "lib/ui.h.s"
.include "assets/assets.h.s"
.include "lib/colors.h.s"

SetFieldB UI_COMPONENT_Y       , 110
SetFieldB UI_COMPONENT_X       , 8
bite_ui_DefineComponentText _any_button_text, "PRESS<ANY<KEY<OR<JOYSTICK<BUTTON"

SetFieldB UI_COMPONENT_Y       , 80
SetFieldB UI_COMPONENT_X       , 36
bite_ui_DefineComponentText _fire_key_text, "FIRE"

bite_ui_DefineComponentNodeInit ui_bind_fire
bite_ui_DefineComponentNodeAddChild _any_button_text
bite_ui_DefineComponentNodeAddChild _fire_key_text
bite_ui_DefineComponentNodeEnd ui_bind_fire

SetFieldB UI_COMPONENT_Y       , 80
SetFieldB UI_COMPONENT_X       , 36
bite_ui_DefineComponentText _jump_key_text, "JUMP"

bite_ui_DefineComponentNodeInit ui_bind_jump
bite_ui_DefineComponentNodeAddChild _any_button_text
bite_ui_DefineComponentNodeAddChild _jump_key_text
bite_ui_DefineComponentNodeEnd ui_bind_jump

SetFieldB UI_COMPONENT_Y       , 80
SetFieldB UI_COMPONENT_X       , 31
bite_ui_DefineComponentText _left_key_text, "MOVE<LEFT"

bite_ui_DefineComponentNodeInit ui_bind_left
bite_ui_DefineComponentNodeAddChild _any_button_text
bite_ui_DefineComponentNodeAddChild _left_key_text
bite_ui_DefineComponentNodeEnd ui_bind_left

SetFieldB UI_COMPONENT_Y       , 80
SetFieldB UI_COMPONENT_X       , 30
bite_ui_DefineComponentText _right_key_text, "MOVE<RIGHT"

bite_ui_DefineComponentNodeInit ui_bind_right
bite_ui_DefineComponentNodeAddChild _any_button_text
bite_ui_DefineComponentNodeAddChild _right_key_text
bite_ui_DefineComponentNodeEnd ui_bind_right

SetFieldB UI_COMPONENT_Y       , 80
SetFieldB UI_COMPONENT_X       , 35
bite_ui_DefineComponentText _pause_key_text, "PAUSE"

bite_ui_DefineComponentNodeInit ui_bind_pause
bite_ui_DefineComponentNodeAddChild _any_button_text
bite_ui_DefineComponentNodeAddChild _pause_key_text
bite_ui_DefineComponentNodeEnd ui_bind_pause

SetFieldB UI_COMPONENT_Y       , 80
SetFieldB UI_COMPONENT_X       , 35
bite_ui_DefineComponentText _reset_key_text, "RESET"

bite_ui_DefineComponentNodeInit ui_bind_reset
bite_ui_DefineComponentNodeAddChild _any_button_text
bite_ui_DefineComponentNodeAddChild _reset_key_text
bite_ui_DefineComponentNodeEnd ui_bind_reset

; SetFieldB UI_COMPONENT_Y       , 95
; SetFieldB UI_COMPONENT_X       , 15
; bite_ui_DefineComponentText _random_key_text, "PRESS<ANY<KEY<TO<CONTINUE"

; bite_ui_DefineComponentNodeInit ui_bind_any
; bite_ui_DefineComponentNodeAddChild _random_key_text
; bite_ui_DefineComponentNodeEnd ui_bind_any