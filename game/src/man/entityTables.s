.include "man/sfx.h.s"
.include "man/entity.h.s"
.include "man/behaviors/collision.h.s"
.include "man/behaviors/shooting.h.s"
.include "man/behaviors/death.h.s"
.include "assets/assets.h.s"
.include "cpctelera.h.s"
.include "cfg.h.s"
.include "lib/null.h.s"




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DEFINE ENTITY TABLE OF POINTERS (2 bytes)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.macro DefineEntityTableW __name
	__name::
		AddFieldW ENT_TABLE_W_PLAYER
		AddFieldW ENT_TABLE_W_BOSS1
		AddFieldW ENT_TABLE_W_BOSS2
		AddFieldW ENT_TABLE_W_BOSS3
		AddFieldW ENT_TABLE_W_BOSS4
		AddFieldW ENT_TABLE_W_PLAYER_SHOT
        AddFieldW ENT_TABLE_W_BIRD
		AddFieldW ENT_TABLE_W_BOSS_SHOT
		AddFieldW ENT_TABLE_W_NEXT_MAP
		AddFieldW ENT_TABLE_W_NEXT_MAP_DOOR
		AddFieldW ENT_TABLE_W_NEXT_MAP_ELEVATOR
		AddFieldW ENT_TABLE_W_MINION
		AddFieldW ENT_TABLE_W_LOLLIPOP
		AddFieldW ENT_TABLE_W_SPIKES
.endm

OffsetInit
DefineOffset ENT_TABLE_W_PLAYER_L      , 1
DefineOffset ENT_TABLE_W_PLAYER_H      , 1

DefineOffset ENT_TABLE_W_BOSS1_L       , 1
DefineOffset ENT_TABLE_W_BOSS1_H       , 1

DefineOffset ENT_TABLE_W_BOSS2_L       , 1
DefineOffset ENT_TABLE_W_BOSS2_H       , 1

DefineOffset ENT_TABLE_W_BOSS3_L       , 1
DefineOffset ENT_TABLE_W_BOSS3_H       , 1

DefineOffset ENT_TABLE_W_BOSS4_L       , 1
DefineOffset ENT_TABLE_W_BOSS4_H       , 1

DefineOffset ENT_TABLE_W_PLAYER_SHOT_L , 1
DefineOffset ENT_TABLE_W_PLAYER_SHOT_H , 1

DefineOffset ENT_TABLE_W_BIRD_L   , 1
DefineOffset ENT_TABLE_W_BIRD_H   , 1

DefineOffset ENT_TABLE_W_BOSS_SHOT_L   , 1
DefineOffset ENT_TABLE_W_BOSS_SHOT_H   , 1

DefineOffset ENT_TABLE_W_NEXT_MAP_L    , 1
DefineOffset ENT_TABLE_W_NEXT_MAP_H    , 1

DefineOffset ENT_TABLE_W_NEXT_MAP_DOOR_L    , 1
DefineOffset ENT_TABLE_W_NEXT_MAP_DOOR_H    , 1

DefineOffset ENT_TABLE_W_NEXT_MAP_ELEVATOR_L    , 1
DefineOffset ENT_TABLE_W_NEXT_MAP_ELEVATOR_H    , 1

DefineOffset ENT_TABLE_W_MINION_L      , 1
DefineOffset ENT_TABLE_W_MINION_H      , 1

DefineOffset ENT_TABLE_W_LOLLIPOP_L   , 1
DefineOffset ENT_TABLE_W_LOLLIPOP_H   , 1

DefineOffset ENT_TABLE_W_SPIKES_L   , 1
DefineOffset ENT_TABLE_W_SPIKES_H   , 1

DefineOffset SIZEOF_ENT_TABLE_W, 0





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DEFINE ENTITY TABLE OF BYTES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.macro DefineEntityTableB __name
	__name::
		AddFieldB ENT_TABLE_B_PLAYER
		AddFieldB ENT_TABLE_B_BOSS1
		AddFieldB ENT_TABLE_B_BOSS2
		AddFieldB ENT_TABLE_B_BOSS3
		AddFieldB ENT_TABLE_B_BOSS4
		AddFieldB ENT_TABLE_B_PLAYER_SHOT
        AddFieldB ENT_TABLE_B_BIRD
		AddFieldB ENT_TABLE_B_BOSS_SHOT
		AddFieldB ENT_TABLE_B_NEXT_MAP
		AddFieldB ENT_TABLE_B_NEXT_MAP_DOOR
		AddFieldB ENT_TABLE_B_NEXT_MAP_ELEVATOR
		AddFieldB ENT_TABLE_B_MINION
		AddFieldB ENT_TABLE_B_LOLLIPOP
		AddFieldB ENT_TABLE_B_SPIKES
.endm

OffsetInit
DefineOffset ENT_TABLE_B_PLAYER      , 1

DefineOffset ENT_TABLE_B_BOSS1       , 1

DefineOffset ENT_TABLE_B_BOSS2       , 1

DefineOffset ENT_TABLE_B_BOSS3       , 1

DefineOffset ENT_TABLE_B_BOSS4       , 1

DefineOffset ENT_TABLE_B_PLAYER_SHOT , 1

DefineOffset ENT_TABLE_B_BIRD        , 1

DefineOffset ENT_TABLE_B_BOSS_SHOT   , 1

DefineOffset ENT_TABLE_B_NEXT_MAP    , 1

DefineOffset ENT_TABLE_B_NEXT_MAP_DOOR , 1

DefineOffset ENT_TABLE_B_NEXT_MAP_ELEVATOR , 1

DefineOffset ENT_TABLE_B_MINION      , 1

DefineOffset ENT_TABLE_B_LOLLIPOP   , 1

DefineOffset ENT_TABLE_B_SPIKES     , 1

DefineOffset SIZEOF_ENT_TABLE, 0




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Gets the corresponding value on the table for this 
;; entity type ID
;; ----------------------------------------------------
;; PARAMS
;; HL: pointer to the table
;; IX: entity
;; ----------------------------------------------------
;; RETURNS
;; HL: the value
;; ----------------------------------------------------
;; DESTROYS
;; A
;; ----------------------------------------------------
man_entity_entityTableWLookup::
	ld a, ENT_TYPE_ID(ix)
	add a
	add_hl_a
	ld a, (hl)
	inc hl
	ld h, (hl)
	ld l, a
	ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Gets the corresponding value on the table for this 
;; entity type ID
;; ----------------------------------------------------
;; PARAMS
;; HL: pointer to the table
;; IX: entity
;; ----------------------------------------------------
;; RETURNS
;; A: the value
;; ----------------------------------------------------
;; DESTROYS
;; HL
;; ----------------------------------------------------
man_entity_entityTableBLookup::
	ld a, ENT_TYPE_ID(ix)
	add_hl_a
	ld a, (hl)
	ret






;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ON BOUNDARY COLLISION
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SetFieldW ENT_TABLE_W_PLAYER            , man_behaviors_collision_playerBoundaryCollision
SetFieldW ENT_TABLE_W_BOSS1             , man_behaviors_collision_playerBoundaryCollision
SetFieldW ENT_TABLE_W_BOSS2             , man_behaviors_collision_playerBoundaryCollision
SetFieldW ENT_TABLE_W_BOSS3             , man_behaviors_collision_playerBoundaryCollision
SetFieldW ENT_TABLE_W_BOSS4             , man_behaviors_collision_playerBoundaryCollision
SetFieldW ENT_TABLE_W_PLAYER_SHOT       , man_behaviors_collision_shotsBoundaryCollision
SetFieldW ENT_TABLE_W_BIRD              , man_behaviors_collision_playerBoundaryCollision
SetFieldW ENT_TABLE_W_BOSS_SHOT         , man_behaviors_collision_shotsBoundaryCollision
SetFieldW ENT_TABLE_W_NEXT_MAP          , bite_null_call
SetFieldW ENT_TABLE_W_NEXT_MAP_DOOR     , bite_null_call
SetFieldW ENT_TABLE_W_NEXT_MAP_ELEVATOR , bite_null_call
SetFieldW ENT_TABLE_W_MINION            , man_behaviors_collision_playerBoundaryCollision
SetFieldW ENT_TABLE_W_LOLLIPOP          , man_behaviors_collision_playerBoundaryCollision
SetFieldW ENT_TABLE_W_SPIKES            , bite_null_call
DefineEntityTableW man_behaviors_collision_onBoundaryTable



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SHOOTING
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SetFieldW ENT_TABLE_W_PLAYER            , man_behaviors_shooting_playerShoot
SetFieldW ENT_TABLE_W_BOSS1             , man_behaviors_shooting_boss1Shoot
SetFieldW ENT_TABLE_W_BOSS2             , man_behaviors_shooting_boss2Shoot
SetFieldW ENT_TABLE_W_BOSS3             , man_behaviors_shooting_boss4Shoot
SetFieldW ENT_TABLE_W_BOSS4             , man_behaviors_shooting_boss4Shoot
SetFieldW ENT_TABLE_W_PLAYER_SHOT       , bite_null_call
SetFieldW ENT_TABLE_W_BIRD              , man_behaviors_shooting_birdShoot
SetFieldW ENT_TABLE_W_BOSS_SHOT         , bite_null_call
SetFieldW ENT_TABLE_W_NEXT_MAP          , bite_null_call
SetFieldW ENT_TABLE_W_NEXT_MAP_DOOR     , bite_null_call
SetFieldW ENT_TABLE_W_NEXT_MAP_ELEVATOR , bite_null_call
SetFieldW ENT_TABLE_W_MINION            , man_behaviors_shooting_minionShoot
SetFieldW ENT_TABLE_W_LOLLIPOP          , bite_null_call
SetFieldW ENT_TABLE_W_SPIKES            , bite_null_call
DefineEntityTableW man_behaviors_shooting_table



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ON DEATH
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SetFieldW ENT_TABLE_W_PLAYER            , man_behaviors_death_player
SetFieldW ENT_TABLE_W_BOSS1             , man_behaviors_death_boss
SetFieldW ENT_TABLE_W_BOSS2             , man_behaviors_death_boss
SetFieldW ENT_TABLE_W_BOSS3             , man_behaviors_death_boss
SetFieldW ENT_TABLE_W_BOSS4             , man_behaviors_death_boss
SetFieldW ENT_TABLE_W_PLAYER_SHOT       , man_behaviors_death_null
SetFieldW ENT_TABLE_W_BIRD              , man_behaviors_death_bird
SetFieldW ENT_TABLE_W_BOSS_SHOT         , man_behaviors_death_null
SetFieldW ENT_TABLE_W_NEXT_MAP          , man_behaviors_death_nextMap
SetFieldW ENT_TABLE_W_NEXT_MAP_DOOR     , man_behaviors_death_nextMap
SetFieldW ENT_TABLE_W_NEXT_MAP_ELEVATOR , man_behaviors_death_elevator
SetFieldW ENT_TABLE_W_MINION            , man_behaviors_death_minion
SetFieldW ENT_TABLE_W_LOLLIPOP          , man_behaviors_death_null
SetFieldW ENT_TABLE_W_SPIKES            , man_behaviors_death_null
DefineEntityTableW entity_table_onDeath



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ON COLLISION SFX
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SetFieldW ENT_TABLE_W_PLAYER            , man_sfx_playNothing
SetFieldW ENT_TABLE_W_BOSS1             , man_sfx_playExplosion
SetFieldW ENT_TABLE_W_BOSS2             , man_sfx_playExplosion
SetFieldW ENT_TABLE_W_BOSS3             , man_sfx_playExplosion
SetFieldW ENT_TABLE_W_BOSS4             , man_sfx_playExplosion
SetFieldW ENT_TABLE_W_PLAYER_SHOT       , man_sfx_playNothing
SetFieldW ENT_TABLE_W_BIRD              , man_sfx_playExplosion
SetFieldW ENT_TABLE_W_BOSS_SHOT         , man_sfx_playExplosion
SetFieldW ENT_TABLE_W_NEXT_MAP          , man_sfx_playNothing
SetFieldW ENT_TABLE_W_NEXT_MAP_DOOR     , man_sfx_playNothing
SetFieldW ENT_TABLE_W_NEXT_MAP_ELEVATOR , man_sfx_playNothing
SetFieldW ENT_TABLE_W_MINION            , man_sfx_playExplosion
SetFieldW ENT_TABLE_W_LOLLIPOP          , man_sfx_playNothing
SetFieldW ENT_TABLE_W_SPIKES            , man_sfx_playExplosion
DefineEntityTableW entity_table_onCollisionSFX




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ON DEATH START
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SetFieldW ENT_TABLE_W_PLAYER            , man_behaviors_death_playerStart
SetFieldW ENT_TABLE_W_BOSS1             , man_behaviors_death_bossStart
SetFieldW ENT_TABLE_W_BOSS2             , man_behaviors_death_bossStart
SetFieldW ENT_TABLE_W_BOSS3             , man_behaviors_death_bossStart
SetFieldW ENT_TABLE_W_BOSS4             , man_behaviors_death_bossStart
SetFieldW ENT_TABLE_W_PLAYER_SHOT       , man_sfx_playNothing
SetFieldW ENT_TABLE_W_BIRD              , man_sfx_playNothing
SetFieldW ENT_TABLE_W_BOSS_SHOT         , man_sfx_playNothing
SetFieldW ENT_TABLE_W_NEXT_MAP          , man_sfx_playNothing
SetFieldW ENT_TABLE_W_NEXT_MAP_DOOR     , man_sfx_playOpenDoor
SetFieldW ENT_TABLE_W_NEXT_MAP_ELEVATOR , man_sfx_playElevator
SetFieldW ENT_TABLE_W_MINION            , man_sfx_playNothing
SetFieldW ENT_TABLE_W_LOLLIPOP          , man_sfx_playHealth
SetFieldW ENT_TABLE_W_SPIKES            , man_sfx_playNothing
DefineEntityTableW entity_table_onDeathStart




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; MAX VELOCITY X
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SetFieldB ENT_TABLE_B_PLAYER            , MAX_VEL_X
SetFieldB ENT_TABLE_B_BOSS1             , MAX_VEL_BOSS1_X
SetFieldB ENT_TABLE_B_BOSS2             , MAX_VEL_BOSS2_X
SetFieldB ENT_TABLE_B_BOSS3             , MAX_VEL_BOSS2_X
SetFieldB ENT_TABLE_B_BOSS4             , MAX_VEL_BOSS4_X
SetFieldB ENT_TABLE_B_PLAYER_SHOT       , MAX_VEL_X_SHOOTS
SetFieldB ENT_TABLE_B_BIRD              , MAX_VEL_BIRD
SetFieldB ENT_TABLE_B_BOSS_SHOT         , MAX_VEL_X_SHOOTS
SetFieldB ENT_TABLE_B_NEXT_MAP          , 0
SetFieldB ENT_TABLE_B_NEXT_MAP_DOOR     , 0
SetFieldB ENT_TABLE_B_NEXT_MAP_ELEVATOR , 0
SetFieldB ENT_TABLE_B_MINION            , MAX_VEL_X
SetFieldB ENT_TABLE_B_LOLLIPOP          , 0
SetFieldB ENT_TABLE_B_SPIKES            , 0
DefineEntityTableB entity_table_maxVelocityX

