.include "lib/macros.h.s"

.globl man_scene_setFunctionPlaceholder
.globl man_scene_clearStack
.globl man_scene_push
.globl man_scene_setPhase
.globl man_scene_sceneInitEnd
.globl man_scene_sceneStartEnd
.globl man_scene_sceneTerminateEnd

SCENE_PHASE_INIT      = 0
SCENE_PHASE_START     = 1
SCENE_PHASE_UPDATE    = 2
SCENE_PHASE_TERMINATE = 3

.macro DefineScene __name
	__name::
		AddFieldW SCENE_FUNCTION_INIT
		AddFieldW SCENE_FUNCTION_START
		AddFieldW SCENE_FUNCTION_UPDATE
		AddFieldW SCENE_FUNCTION_TERMINATE
		.db SCENE_PHASE_INIT  ;; SCENE_PHASE_STATUS
.endm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ENTITY FIELDS OFFSETS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
OffsetInit
DefineOffset SCENE_FUNCTION_INIT_L, 1
DefineOffset SCENE_FUNCTION_INIT_H, 1
DefineOffset SCENE_FUNCTION_START_L, 1
DefineOffset SCENE_FUNCTION_START_H, 1
DefineOffset SCENE_FUNCTION_UPDATE_L, 1
DefineOffset SCENE_FUNCTION_UPDATE_H, 1
DefineOffset SCENE_FUNCTION_TERMINATE_L, 1
DefineOffset SCENE_FUNCTION_TERMINATE_H, 1
DefineOffset SCENE_PHASE_STATUS, 1

DefineOffset SIZEOF_SCENE, 0

