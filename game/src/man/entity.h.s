.include "lib/macros.h.s"

MAX_ENTITIES = 25
MAX_ENTITIES_DELETED = MAX_ENTITIES/2

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ENTITY TYPE FLAGS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ENTITY_TYPE_FLAG_NULL			= 0
ENTITY_TYPE_FLAG_PLAYER			= 0b00000001
ENTITY_TYPE_FLAG_BOSS			= 0b00000010
ENTITY_TYPE_FLAG_PLAYER_SHOT	= 0b00000100
ENTITY_TYPE_FLAG_BOSS_SHOT		= 0b00001000
ENTITY_TYPE_FLAG_NEXT_MAP		= 0b00010000
ENTITY_TYPE_FLAG_MINION  		= 0b00100000
ENTITY_TYPE_FLAG_LOLLIPOP 		= 0b01000000
ENTITY_TYPE_FLAG_SPIKES 		= 0b10000000

ENTITY_TYPE_FLAG_PLAYER_BIT			= 0
ENTITY_TYPE_FLAG_BOSS_BIT			= 1
ENTITY_TYPE_FLAG_PLAYER_SHOT_BIT	= 2
ENTITY_TYPE_FLAG_BOSS_SHOT_BIT		= 3
ENTITY_TYPE_FLAG_NEXT_MAP_BIT		= 4
ENTITY_TYPE_FLAG_MINION_BIT  		= 5
ENTITY_TYPE_FLAG_LOLLIPOP_BIT 		= 6
ENTITY_TYPE_FLAG_SPIKES_BIT 		= 7

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ENTITY TYPE IDs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ENTITY_TYPE_ID_PLAYER		     = 0
ENTITY_TYPE_ID_BOSS1	         = 1
ENTITY_TYPE_ID_BOSS2	         = 2
ENTITY_TYPE_ID_BOSS3	         = 3
ENTITY_TYPE_ID_BOSS4	         = 4
ENTITY_TYPE_ID_PLAYER_SHOT	     = 5
ENTITY_TYPE_ID_BIRD              = 6
ENTITY_TYPE_ID_BOSS_SHOT	     = 7
ENTITY_TYPE_ID_NEXT_MAP		     = 8
ENTITY_TYPE_ID_NEXT_MAP_DOOR     = 9
ENTITY_TYPE_ID_NEXT_MAP_ELEVATOR = 10
ENTITY_TYPE_ID_MINION  		     = 11
ENTITY_TYPE_ID_LOLLIPOP 	     = 12
ENTITY_TYPE_ID_SPIKES    	     = 13


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ENTITY COMPONENTS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
COMPONENT_L_RENDER_BIT     = 0
COMPONENT_L_PHYSICS_BIT    = 1
COMPONENT_L_COLLISION_BIT  = 2
COMPONENT_L_ANIMATION_BIT  = 3
COMPONENT_L_AI_BIT         = 4
COMPONENT_L_HEALTH_BIT     = 5
COMPONENT_L_SHOOTING_BIT   = 6
COMPONENT_L_DEATH_BIT      = 7

COMPONENT_L_RENDER          = 0b00000001
COMPONENT_L_PHYSICS         = 0b00000010
COMPONENT_L_COLLISION       = 0b00000100
COMPONENT_L_ANIMATION       = 0b00001000
COMPONENT_L_AI              = 0b00010000
COMPONENT_L_HEALTH          = 0b00100000 
COMPONENT_L_SHOOTING        = 0b01000000 
COMPONENT_L_DEATH           = 0b10000000 

COMPONENT_H_TILE_COLLISION_BIT      = 0
COMPONENT_H_BOUNDARY_COLLISION_BIT  = 1
COMPONENT_H_INPUT_BIT               = 2
COMPONENT_H_ENERGY_BIT              = 3
COMPONENT_H_JUMP_BIT                = 4

COMPONENT_H_TILE_COLLISION      = 0b00000001
COMPONENT_H_BOUNDARY_COLLISION  = 0b00000010
COMPONENT_H_INPUT               = 0b00000100
COMPONENT_H_ENERGY              = 0b00001000
COMPONENT_H_JUMP                = 0b00010000


RENDER_ERASE_BIT = 0
RENDER_DRAW_BIT  = 1
RENDER_BLANK_BIT = 2

RENDER_ERASE = 0b00000001
RENDER_DRAW  = 0b00000010
RENDER_BLANK = 0b00000100

IA_LEFT_FOOT_BIT = 7
IA_RIGHT_FOOT_BIT = 6

IA_LEFT_FOOT = 0b10000000
IA_RIGHT_FOOT = 0b01000000
IA_COUNTER_BITS = 0b00111111
IA_FLAGS_BITS = 0b11000000

TARGET_UNSET = 0xFF

ENTITY_FACING_LEFT = 1
ENTITY_FACING_RIGHT = 0


.macro DefineEntity __name
	__name::
		AddFieldB ENT_TYPE_FLAG
		AddFieldB ENT_TYPE_ID
		AddFieldB ENT_COMPONENTS_L
		AddFieldB ENT_COMPONENTS_H
		AddFieldW ENT_Y_WORLD
		AddFieldW ENT_X_WORLD
		AddFieldB ENT_Y
		AddFieldB ENT_X
		.dw 0x0000 ;; Previous screen pointer
		.db 0xFF   ;; Previous Y
		.db 0xFF   ;; Previous X
		AddFieldB ENT_VEL_Y
		AddFieldB ENT_VEL_X
		AddFieldB ENT_ACCELERATION_X
		AddFieldB ENT_ACCELERATION_Y
		AddFieldB ENT_FRICTION_X_DEFAULT
		AddFieldB ENT_FRICTION_X
		AddFieldB ENT_FRICTION_Y_DEFAULT
		AddFieldB ENT_FRICTION_Y
		AddFieldB ENT_WIDTH
		AddFieldB ENT_HEIGHT
		AddFieldW ENT_SPRITE
		.db 0x00   ;; Ent jump status
        AddFieldB ENT_FACING_DIR
        AddFieldB ENT_COLLIDES_WITH
        AddFieldW ENT_IA_STATE
        .db 0x00
		AddFieldB ENT_MAX_HEALTH
		AddFieldB ENT_HEALTH
		.db 0x00 ;; Damage received
		AddFieldB ENT_ATTACK
		AddFieldB ENT_INFLICTS_DAMAGE_ON
        .db 0x00 ;; Animation counter
        AddFieldW ENT_CURRENT_ANIMATION
        AddFieldW ENT_LAST_SPRITE
        .db 0x00
        .db 0xFE        ;; Animation ID. FE so its not equal to any animation ID, or death animation (wich is FF)
        AddFieldW ENT_ANIMATION_ARRAY
        .db #TARGET_UNSET ;; IA target x
        .db #TARGET_UNSET ;; IA target y
        .db 0x00 ;; IA arrived flag
        AddFieldB ENT_INVINCIBLE_TIME
        .db 0x00 ;; Health invincible counter
        .db #RENDER_DRAW
        AddFieldB ENT_DEATH_COUNTER
        AddFieldW ENT_DEATH_ANIMATION
        .db 0x00  ;; SHOOTING DATA  Holds shooting counters and flags
        AddFieldW ENT_POINTS
.endm






;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ENTITY FIELDS OFFSETS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
OffsetInit
DefineOffset ENT_TYPE_FLAG, 1
DefineOffset ENT_TYPE_ID, 1
DefineOffset ENT_COMPONENTS_L, 1
DefineOffset ENT_COMPONENTS_H, 1
DefineOffset ENT_Y_WORLD_L, 1
DefineOffset ENT_Y_WORLD_H, 1
DefineOffset ENT_X_WORLD_L, 1
DefineOffset ENT_X_WORLD_H, 1
DefineOffset ENT_Y, 1
DefineOffset ENT_X, 1
DefineOffset ENT_PREV_SCREEN_PTR_L, 1
DefineOffset ENT_PREV_SCREEN_PTR_H, 1
DefineOffset ENT_PREV_Y, 1
DefineOffset ENT_PREV_X, 1
DefineOffset ENT_VY, 1
DefineOffset ENT_VX, 1
DefineOffset ENT_ACCELERATION_X, 1
DefineOffset ENT_ACCELERATION_Y, 1
DefineOffset ENT_FRICTION_X_DEFAULT, 1
DefineOffset ENT_FRICTION_X, 1
DefineOffset ENT_FRICTION_Y_DEFAULT, 1
DefineOffset ENT_FRICTION_Y, 1
DefineOffset ENT_WIDTH, 1
DefineOffset ENT_HEIGHT, 1
DefineOffset ENT_SPRITE_L, 1
DefineOffset ENT_SPRITE_H, 1
DefineOffset ENT_JUMP_STATUS, 1
DefineOffset ENT_FACING_DIR, 1
DefineOffset ENT_COLLIDES_WITH, 1
DefineOffset ENT_IA_STATE_L, 1
DefineOffset ENT_IA_STATE_H, 1
DefineOffset ENT_IA_DATA, 1
DefineOffset ENT_MAX_HEALTH, 1
DefineOffset ENT_HEALTH, 1
DefineOffset ENT_DAMAGE, 1
DefineOffset ENT_ATTACK, 1
DefineOffset ENT_INFLICTS_DAMAGE_ON, 1
DefineOffset ENT_ANIMATION_COUNTER, 1
DefineOffset ENT_CURRENT_ANIMATION_L, 1
DefineOffset ENT_CURRENT_ANIMATION_H, 1
DefineOffset ENT_LAST_SPRITE_L, 1
DefineOffset ENT_LAST_SPRITE_H, 1
DefineOffset ENT_ANIMATION_CHANGED, 1
DefineOffset ENT_ANIMATION_ID, 1
DefineOffset ENT_ANIMATION_ARRAY_L, 1
DefineOffset ENT_ANIMATION_ARRAY_H, 1
DefineOffset ENT_IA_TARGET_X, 1
DefineOffset ENT_IA_TARGET_Y, 1
DefineOffset ENT_IA_ARRIVED, 1
DefineOffset ENT_INVINCIBLE_TIME, 1
DefineOffset ENT_INVINCIBLE_COUNTER, 1
DefineOffset ENT_RENDER_FLAGS, 1
DefineOffset ENT_DEATH_COUNTER, 1
DefineOffset ENT_DEATH_ANIMATION_L, 1
DefineOffset ENT_DEATH_ANIMATION_H, 1
DefineOffset ENT_SHOOTING_DATA, 1
DefineOffset ENT_POINTS_L, 1
DefineOffset ENT_POINTS_H, 1

DefineOffset SIZEOF_ENTITY, 0



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; JUMP STATES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
JUMP_STATUS_QUIET = 0
JUMP_STATUS_MAX = 6


;;;;;;;;;;;;;;;;;;;;;;;;;;
;; TILE PROPERTIES
;;;;;;;;;;;;;;;;;;;;;;;;;;
TILE_COLLISIONABLE = 0b10000000

;;;;;;;;;;;;;;;;;;;;;;;;;
;; PUBLIC FUNCTIONS
;;;;;;;;;;;;;;;;;;;;;;;;;
.globl man_entity_init
.globl man_entity_clearExceptPlayer
.globl man_entity_createEntity
.globl man_entity_forallEntities
.globl man_entity_forallMatchingEntities
.globl man_entity_getPlayer
.globl man_entity_compareAllEntities
.globl man_entity_compareAllMatchingEntities
.globl man_entity_markForDelete
.globl man_entity_deleteMarked
.globl man_entity_getBoss
.globl man_entity_setPosition
.globl man_entity_restorePlayer
.globl numEntities
