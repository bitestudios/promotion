.include"cpctelera.h.s"
.include"man/scene.h.s"
.include"man/interruption.h.s"



MAX_SCENES = 10

_man_scene_stack:
.ds MAX_SCENES*SIZEOF_SCENE


_man_scene_stackPtr:: .dw _man_scene_stack-SIZEOF_SCENE


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Sets where is the function placeholder
;; ----------------------------------------------------
;; PARAMS
;; HL = function placeholder address
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; ----------------------------------------------------
man_scene_setFunctionPlaceholder::
	ld (_function_placeholder), hl
	ret
	


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Clears the stack
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; HL
;; ----------------------------------------------------
man_scene_clearStack::
    ld hl, #_man_scene_stack-SIZEOF_SCENE
	ld (_man_scene_stackPtr), hl
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Push a scene into the scenes stack and sets to call
;; to the scene's init function
;; ----------------------------------------------------
;; PARAMS
;; HL = pointer to the scene
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, BC, HL, DE
;; ----------------------------------------------------
man_scene_push::

	ex de, hl                     ;; Stores hl on de
	ld hl, (_man_scene_stackPtr)  ;; Stores de pointer to the stack on hl
	ld bc, #SIZEOF_SCENE          ;; Stores the size of a scene on BC
	add hl, bc                    ;;\ Adds the size of a scene to HL, now it points to the future
	                              ;;|- scene

	ld (_man_scene_stackPtr), hl  ;; Stores the new value of the stackPtr
	ex de, hl                     ;;\-Swap HL and DE, now HL is the source scene and DE the dest
	                              ;; |_scene

	ldir                          ;; Copies the source scene (HL) into the dest scene (DE)

	call man_scene_phaseToPlaceholder

	ret
	
	



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Pop a scene from the stack
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, BC, HL, A
;; ----------------------------------------------------
man_scene_pop::
	ld hl, (_man_scene_stackPtr)  ;; Stores de pointer to the stack on hl
	ld a, #SIZEOF_SCENE           ;; Stores the size of a scene on A
	sub_hl_a                      ;;-- Subs the size of a scene to HL, now it points to the
	                              ;;\_ previous scene
	ld (_man_scene_stackPtr), hl
	call man_scene_phaseToPlaceholder
	ret




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Sets the actual top stack scene phase on the function
;; placeholder
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, BC, HL
;; ----------------------------------------------------
man_scene_phaseToPlaceholder::
	ld ix, (_man_scene_stackPtr) ;; Loads the actual scene on ix

	ld c, SCENE_PHASE_STATUS(ix) ;;- Loads the scene phase on BC
	ld b, #0                     ;;/

	add ix, bc                   ;; Adds BC to IX, now IX points to the escene phase function
	add ix, bc                   ;; Adds BC to IX, now IX points to the escene phase function

	ld l, (ix)                   ;;- Loads the phase function on HL
	ld h, 1(ix)                  ;;/

	_function_placeholder = .+1  ;;/- Stores HL (the phase function of the scene) on the function
	ld (0x0000), hl              ;;\_ placeholder
	ret

	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; sets to call to the scene's init function
;; ----------------------------------------------------
;; PARAMS
;; A = phase id
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, BC, HL
;; ----------------------------------------------------
man_scene_setPhase::
	ld ix, (_man_scene_stackPtr)
	ld SCENE_PHASE_STATUS(ix), a

	call man_scene_phaseToPlaceholder

	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Function to call at the end of the init function of
;; the scene
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, BC, HL, A
;; ----------------------------------------------------
man_scene_sceneInitEnd::
	ld a, #SCENE_PHASE_START
	call man_scene_setPhase
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Function to call at the end of the start function of
;; the scene
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, BC, HL, A
;; ----------------------------------------------------
man_scene_sceneStartEnd::
	ld a, #SCENE_PHASE_UPDATE
	call man_scene_setPhase
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Function to call at the end of the terminate function
;; of the scene
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, BC, HL
;; ----------------------------------------------------
man_scene_sceneTerminateEnd::
	call man_scene_pop
	ret

