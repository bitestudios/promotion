.globl man_game_init
.globl man_game_play
.globl man_game_terminate
.globl man_game_setGameStatus
.globl man_game_getGameStatus
.globl man_game_timer
.globl man_game_decRemainingLives
.globl man_game_setRemainingLives
.globl man_game_checkRemainingLives
.globl man_game_playerEnergy
.globl man_game_initScore

.globl game_tilemapCenter
.globl game_tilemapCenterEnd
.globl game_tilemapTop
.globl game_tilemapTopEnd
.globl game_tilemapMarginLeft
.globl game_tilemapMarginLeftEnd
.globl game_tilemapMarginRight
.globl game_tilemapMarginRightEnd
.globl game_score
.globl game_scorePrev
.globl man_game_remainingLives
.globl man_game_timer
.globl man_game_timerStopped


MAN_GAME_STATUS_NORMAL          = 0
MAN_GAME_STATUS_PAUSED	        = 1
MAN_GAME_STATUS_RESETED         = 2
MAN_GAME_STATUS_PLAYER_DEFEATED = 3
MAN_GAME_STATUS_BOSS_DEFEATED   = 4
MAN_GAME_STATUS_STARTED         = 5
MAN_GAME_STATUS_NEXT_MAP        = 6
MAN_GAME_STATUS_BINDING         = 7
MAN_GAME_STATUS_NEXT_FLOOR      = 8
MAN_GAME_STATUS_BONUS_COMPLETED = 9

