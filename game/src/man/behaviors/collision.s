.include "man/entity.h.s"
.include "sys/collision.h.s"


man_behaviors_collision_playerBoundaryCollision::
    ;; Set feet flags
    ld a, e
    and #IA_FLAGS_BITS ;; reset other flags except feet ones
    ld b, a
    ld a, ENT_IA_DATA(ix)
    or b                        ;; Set new flag bits
    ld ENT_IA_DATA(ix), a
	
	;;Left or right boundary
	ld a, #BOUNDARY_COLLISION_LEFT|BOUNDARY_COLLISION_RIGHT
	and e
	jr z, endif_leftRightBoundaryColl
		ld ENT_VX(ix), #0
	endif_leftRightBoundaryColl:

	;;Top boundary
	bit BOUNDARY_COLLISION_TOP_BIT, e
	jr z, endif_topBoundaryColl
		ld ENT_VY(ix), #0
		ret
	endif_topBoundaryColl:

	;;Bottom boundary (ONLY CHECKS IF TOP ISN'T TRUE)
	bit BOUNDARY_COLLISION_BOTTOM_BIT, e
	ret z
		ld ENT_VY(ix), #0
		ld ENT_JUMP_STATUS(ix), #JUMP_STATUS_QUIET
	ret



man_behaviors_collision_shotsBoundaryCollision::
		ld ENT_DAMAGE(ix), #1
	ret


