.include "man/game.h.s"
.include "man/entity.h.s"
.include "man/sfx.h.s"
.include "sys/render.h.s"
.include "helper/sound.h.s"
.include "sys/ia.h.s"
.include "cpctelera.h.s"


man_behaviors_death_player::
    ld a, #MAN_GAME_STATUS_PLAYER_DEFEATED
    call man_game_setGameStatus
	ret


man_behaviors_death_bossStart::
    ld a, ENT_TYPE_FLAG(ix)
    cp #ENTITY_TYPE_FLAG_BOSS
    ret nz
	ld a, #1
	ld (man_game_timerStopped), a
	ld__d_ixh
	ld__e_ixl
	call man_entity_getPlayer
	res COMPONENT_L_HEALTH_BIT, ENT_COMPONENTS_L(ix)
	res COMPONENT_L_COLLISION_BIT, ENT_COMPONENTS_L(ix)
	ld a, #0
	ld ENT_DAMAGE(ix), a
	ld__ixh_d
	ld__ixl_e
	ret


man_behaviors_death_playerStart::
	call man_entity_getBoss
	res COMPONENT_L_COLLISION_BIT, ENT_COMPONENTS_L(iy)
	ld a, #0
	ld ENT_DAMAGE(iy), a
	jp man_sfx_playLaugh


man_behaviors_death_boss::
    ld a, ENT_TYPE_FLAG(ix)
    cp #ENTITY_TYPE_FLAG_BOSS
    ret nz
    ld a, #MAN_GAME_STATUS_BOSS_DEFEATED
    call man_game_setGameStatus
	ret

man_behaviors_death_null::
	ret

man_behaviors_death_minion::
    ld a, (_minion_counter)
    dec a
    ld (_minion_counter), a
    ret


man_behaviors_death_bird::
    ld a, (_bird_counter)
    dec a
    ld (_bird_counter), a
    ret


man_behaviors_death_nextMap::
    ld a, #MAN_GAME_STATUS_NEXT_MAP
    call man_game_setGameStatus
	ret

man_behaviors_death_elevator::
    ld a, #MAN_GAME_STATUS_NEXT_FLOOR
    call man_game_setGameStatus
	ret


