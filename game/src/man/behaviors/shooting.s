.include "cpctelera.h.s"
.include "assets/assets.h.s"
.include "lib/opmacros.h.s"
.include "utils.h.s"
.include "man/entity.h.s"
.include "man/game.h.s"
.include "factory/factory.h.s"
.include "cfg.h.s"
.include "man/sfx.h.s"



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Makes the acton of the player shooting
;; Instantiates a bullet in the position of the player,
;; with a speed relative to where its facing
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IY, HL, A, DE
;; ----------------------------------------------------
man_behaviors_shooting_playerShoot::
    
	ld a, (man_game_playerEnergy)
	sub #ENERGY_CONSUMPTION

	ret c
	
	ld (man_game_playerEnergy), a

	call man_sfx_playShoot
	call factory_createPlayerShot               ;; Creates the bullet entity
	call man_behaviors_directionalShoot

    ret


_x_substract::
    .db 0

_y_substract::
    .db 1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Makes the acton of the bird shooting
;; Instantiates a bullet in the position of the bird,
;; with diagonal speed relative to the player
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity that shoots
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IY, BC, A, HL, IX, IY
;; ----------------------------------------------------
man_behaviors_shooting_birdShoot::
    call man_sfx_playShoot
    push ix
    pop iy

    call man_entity_getPlayer       ;; IX: Player, IY: Bird

    ;; Check if is vertical. The x neets to be checked, as if px = bx, they are on the same vertical plane
    ld a, ENT_X(iy)
    sub ENT_X(ix)
    ld (_x_substract), a
    absA
    cp #BIRD_VERTICAL_THRESHOLD
    jr nc, _not_vertical
        ;; Check y
        ld a, ENT_Y(iy)
        sub ENT_Y(ix)
        ld (_y_substract), a
        jp m, _negative_vertical
            ;; Positive vertical
            ld d, #0
            ld a, #BIRD_BULLET_SPEED_Y
            neg
            ld e, a
            jr _speed_already_set

_negative_vertical:
            ;; Negative vertical
            ld d, #0
            ld e, #BIRD_BULLET_SPEED_Y
            jr _speed_already_set

_not_vertical:
    ld b, a ;; Save x value

    ;; Check if horizontal
    ld a, ENT_Y(iy)
    sub ENT_Y(ix)
    ld (_y_substract), a
    absA
    cp #BIRD_HORIZONTAL_THRESHOLD
    jr nc, _not_horizontal
    ;; Check x
    ld a, (_x_substract)
    or a
    jp m, _negative_horizontal
        ;; Positive horizontal
        ld a, #BIRD_BULLET_SPEED_Y
        neg
        ld d, a
        ld e, #0
        jr _speed_already_set


_negative_horizontal:
        ;; Negative horizontal
        ld d, #BIRD_BULLET_SPEED_Y
        ld e, #0
        jr _speed_already_set

_not_horizontal:

    ;; Its diagonal then
    ;; Check y
    ld a, (_y_substract)
    or a
    jp m, _negative_y_diagonal

    ;; Check X
    ld a, (_x_substract)
    or a
    jp m, _positive_y_negative_x_diagonal
        ;; Positive y positive x diagonal
        ld a, #BIRD_BULLET_SPEED_X
        neg
        ld d, a
        ld a, #BIRD_BULLET_SPEED_Y
        neg
        ld e, a
        jr _speed_already_set

_positive_y_negative_x_diagonal:
        ;; Positive y negative x diagonal
        ld d, #BIRD_BULLET_SPEED_X
        ld a, #BIRD_BULLET_SPEED_Y
        neg
        ld e, a
        jr _speed_already_set

_negative_y_diagonal:
    ;; Check X
    ld a, (_x_substract)
    or a
    jp m, _negative_y_negative_x_diagonal
        ;; Negative y positive x diagonal
        ld a, #BIRD_BULLET_SPEED_X
        neg
        ld d, a
        ld e, #BIRD_BULLET_SPEED_Y
        jr _speed_already_set

_negative_y_negative_x_diagonal:
        ;; Negative y negative x diagonal
        ld d, #BIRD_BULLET_SPEED_X
        ld e, #BIRD_BULLET_SPEED_Y
        jr _speed_already_set


_speed_already_set:
    push de
    call factory_createStaticBirdShot
    pop de
    ld a, h
    ld__ixh_a
    ld a, l
    ld__ixl_a

    ;; Load calculated speeds
    ld ENT_VX(ix), d
    ld ENT_VY(ix), e

    ;; Load initial position
    ld l, ENT_X(iy)
    ld ENT_X(ix), l
	ld h, #0
	shiftl_hl WORLD_SCALE_X_DISP
	ld ENT_X_WORLD_L(ix), l
	ld ENT_X_WORLD_H(ix), h

    ; ld a, ENT_Y(iy)
    ; ld ENT_Y(ix), a

    ld l, ENT_Y(iy)
    ld ENT_Y(ix), l
	ld h, #0
	shiftl_hl WORLD_SCALE_Y_DISP
	ld ENT_Y_WORLD_L(ix), l
	ld ENT_Y_WORLD_H(ix), h

	push iy   ;;\_ Restore de IX value
	pop ix    ;;/

;     ;; Calculate X speed
;     ld a, ENT_X(iy)
;     sub ENT_X(ix)
;     bit 7, a
;     jr z, _negative_x_value
;     shiftr_a 2          ;; X / 8
;     jr _x_end
; _negative_x_value:
;     neg
;     shiftr_a 2          ;; X / 8
;     neg
; _x_end:
;     ld b, a             ;; Save x speed on b

;     ;; Calculate Y speed
;     ld a, ENT_Y(iy)
;     sub ENT_Y(ix)
;     bit 7, a
;     jr z, _negative_y_value
;     shiftr_a 2          ;; X / 8
;     jr _y_end
; _negative_y_value:
;     neg
;     shiftr_a 2          ;; X / 8
;     neg
; _y_end:
;     ld c, a
;     push bc

;     call factory_createStaticBirdShot
;     ld a, h
;     ld__ixh_a
;     ld a, l
;     ld__ixl_a
;     ; push hl
;     ; pop ix                      
;     pop bc

;     ;; Load calculated speeds
;     ld ENT_VY(ix), c
;     ld ENT_VX(ix), b

;     ;; Load initial position
;     ; ld a, ENT_X(iy)
;     ; ld ENT_X(ix), a

;     ld l, ENT_X(iy)
;     ld ENT_X(ix), l
; 	ld h, #0
; 	shiftl_hl WORLD_SCALE_X_DISP
; 	ld ENT_X_WORLD_L(ix), l
; 	ld ENT_X_WORLD_H(ix), h

;     ; ld a, ENT_Y(iy)
;     ; ld ENT_Y(ix), a

;     ld l, ENT_Y(iy)
;     ld ENT_Y(ix), l
; 	ld h, #0
; 	shiftl_hl WORLD_SCALE_Y_DISP
; 	ld ENT_Y_WORLD_L(ix), l
; 	ld ENT_Y_WORLD_H(ix), h

    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Instantiates a bullet in the position of the shooter,
;; with a speed relative to where its facing
;; ----------------------------------------------------
;; PARAMS
;; HL: New entity
;; IX: Father entity
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IY, HL, A, DE
;; ----------------------------------------------------
man_behaviors_directionalShoot::
    ex de, hl                                   ;;\
	ld__iyh_d                                   ;; \- Stores the new entity on iy
	ld__iyl_e                                   ;;/
    
    ld l, ENT_X(ix)
    ld ENT_X(iy), l
	ld h, #0
	shiftl_hl WORLD_SCALE_X_DISP
	ld ENT_X_WORLD_L(iy), l
	ld ENT_X_WORLD_H(iy), h

    ld a, ENT_Y(ix)
    add #7                                      ;; Put the bullet at hands height
    ld ENT_Y(iy), a
	ld l, a
	ld h, #0
	shiftl_hl WORLD_SCALE_Y_DISP
	ld ENT_Y_WORLD_L(iy), l
	ld ENT_Y_WORLD_H(iy), h

    ;; Set bullet direction
    ld a, ENT_FACING_DIR(ix)
    or a
    jp z, _dir_right
    ; ld ENT_VX(iy), #-PLAYER_SHOOT_VEL
    ld a, ENT_VX(iy)
    neg
    ld ENT_VX(iy), a
    jp _endif_right
_dir_right:                                     ;; IF direction id right, bullet goes right, etc.
    ; ld ENT_VX(iy), #PLAYER_SHOOT_VEL
_endif_right:
ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Shoots a boss 1 bullet
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with IA to perform the actions
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IY, HL, A, DE
;; ----------------------------------------------------
man_behaviors_shooting_boss1Shoot::
	call man_sfx_playShootBoss
    call factory_createBossShot                 ;; Creates the bullet entity
	call man_behaviors_directionalShoot
    
    ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Shoots a boss 1 bullet
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with IA to perform the actions
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IY, HL, A, DE
;; ----------------------------------------------------
man_behaviors_shooting_minionShoot::
	call man_sfx_playShoot
    call factory_createStaticBirdShot                 ;; Creates the bullet entity. Is the same as the bird
    ld a, h
    ld__iyh_a
    ld a, l
    ld__iyl_a
    ld ENT_VX(iy), #SHOOTING_MINION_BULLET_SPEED         ;; Set bullet speed
	call man_behaviors_directionalShoot
    
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Shoots 3 bullets. Up straight and down
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with IA to perform the actions
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IY, HL, A, DE
;; ----------------------------------------------------
man_behaviors_shooting_boss4Shoot::
    call man_sfx_playShootBoss
    ;; First bullet goes up
    call factory_createBossShotgunShot          ;; Creates the bullet entity
    ex de, hl                                   ;;\
	ld__iyh_d                                   ;; \- Stores the new entity on iy
	ld__iyl_e                                   ;;/

    ld a, #FOURTH_BOSS_SHOTGUN_Y_SPAN           ;; Load up speed
    neg
    ld ENT_VY(iy), a

    ld__d_iyh
    ld__e_iyl                                   ;; Load the entity pointer back to HL
    ex de, hl                                   ;; HL: Bullet
	call man_behaviors_directionalShoot

    ;; Second bullet goes straight
    call factory_createBossShotgunShot          ;; Creates the bullet entity
    call man_behaviors_directionalShoot

    ;; Third bullet goes down
    call factory_createBossShotgunShot          ;; Creates the bullet entity
    ex de, hl                                   ;;\
	ld__iyh_d                                   ;; \- Stores the new entity on iy
	ld__iyl_e                                   ;;/
  
    ld ENT_VY(iy), #FOURTH_BOSS_SHOTGUN_Y_SPAN  ;; Load down speed

    ld__d_iyh
    ld__e_iyl                                   ;; Load the entity pointer back to HL
    ex de, hl                                   ;; HL: Bullet
	call man_behaviors_directionalShoot

    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Shoots a bird (boss 2)
;; ----------------------------------------------------
;; PARAMS
;; IX: Entity with IA to perform the actions
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IY, HL, A, DE
;; ----------------------------------------------------
man_behaviors_shooting_boss2Shoot::
    call man_sfx_playShootBoss
    call factory_createBirdShot                 ;; Creates the bullet entity
	call man_behaviors_directionalShoot
    
    ret


