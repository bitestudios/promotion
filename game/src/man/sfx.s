.include "assets/assets.h.s"


man_sfx_playNothing::
ret

man_sfx_playLaugh::
	ld a, #SFX_MAN_LAUGH
    ld c,#0  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
	ld a, #SFX_MAN_LAUGH
    ld c,#2  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
	ld a, #SFX_MAN_LAUGH
    ld c,#1  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
	ret


man_sfx_playFall::
    ld a,#SFX_FALL  ;Sound effect number (>=1)
    ld c,#0  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
	ret


man_sfx_playShoot::
    ld a,#SFX_SHOOT  ;Sound effect number (>=1)
    ld c,#2  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
	ret


man_sfx_playShootBoss::
    ld a,#SFX_SHOOT_BOSS  ;Sound effect number (>=1)
    ld c,#1  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
	ret

man_sfx_playHealth::
    ld a,#SFX_HEALTH  ;Sound effect number (>=1)
    ld c,#1  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
	ret

man_sfx_playOpenDoor::
    ld a,#SFX_OPEN_DOOR ;Sound effect number (>=1)
    ld c,#2  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
	ret

man_sfx_playElevator::
	ld a, #SFX_ELEVATOR
    ld c,#0  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
	ld a, #SFX_ELEVATOR
    ld c,#2  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
	ld a, #SFX_ELEVATOR
    ld c,#1  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
	ret


man_sfx_playScoreBonus::
	ld a, #SFX_SLOT_MACHINE
    ld c,#0  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
	ld a, #SFX_SLOT_MACHINE
    ld c,#2  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
	ld a, #SFX_SLOT_MACHINE
    ld c,#1  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
	ret


man_sfx_playExplosion::
    ld a,#SFX_EXPLOSION ;Sound effect number (>=1)
    ld c,#2  ;channel (0-2)
    ld b,#0  ;Inverted volume (0-16)
    call _PLY_AKM_PLAYSOUNDEFFECT
	ret

