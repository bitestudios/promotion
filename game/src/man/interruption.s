.include "cpctelera.h.s"
.include "utils.h.s"
.include "assets/assets.h.s"


man_interruption_interruption0::
	ex af',af
	exx

	;cpctm_setBorder_asm HW_WHITE

	ld hl, #0x39
	ld (hl), #<man_interruption_interruption1
	inc hl
	ld (hl), #>man_interruption_interruption1
	ex af',af
	exx
	
	ei
	reti



man_interruption_interruption1::
	ex af',af
	exx
	
	;cpctm_setBorder_asm HW_RED
	
	__interruption1Palette = .+1
	ld hl, #0x0000
	ld de, #0x0010 ;; Size = 16
	call cpct_setPalette_asm
	

	changeCarry = .
	scf

	call c, cpct_scanKeyboard_if_asm
	
	ld a, (changeCarry)
	xor #0x80
	ld (changeCarry), a

	ld hl, #0x39
	ld (hl), #<man_interruption_interruption2
	inc hl
	ld (hl), #>man_interruption_interruption2

	ex af',af
	exx
	
	ei
	reti


man_interruption_interruption2::
	ex af',af
	exx

	;cpctm_setBorder_asm HW_BLUE

	ld hl, #0x39
	ld (hl), #<man_interruption_interruption3
	inc hl
	ld (hl), #>man_interruption_interruption3
	ex af',af
	exx	

	ei
	reti



man_interruption_interruption3::
	push af
	push bc
	push de
	push hl
	push ix
	push iy

	;cpctm_setBorder_asm HW_WHITE

	__callToPlayer = .+1
	call __nopCall
	;cpctm_setBorder_asm HW_BLACK

	ld hl, #0x39
	ld (hl), #<man_interruption_interruption4
	inc hl
	ld (hl), #>man_interruption_interruption4
	
	pop iy
	pop ix
	pop hl
	pop de
	pop bc
	pop af

	ei
	reti


__interruption4: .db 0

man_interruption_interruption4::
	ex af',af
	exx
	
	;cpctm_setBorder_asm HW_GREEN

	
	changeCarryI4 = .
	scf
	jr nc, __notSet
	ld a, #1
	ld (__interruption4), a

	__notSet:
	
	ld a, (changeCarryI4)
	xor #0x80
	ld (changeCarryI4), a
	
	__interruption4Palette = .+1
	ld hl, #0x0000
	ld de, #0x0010 ;; Size = 16
	call cpct_setPalette_asm
	
	
	ld hl, #0x39
	ld (hl), #<man_interruption_interruption5
	inc hl
	ld (hl), #>man_interruption_interruption5
	ex af',af
	exx

	ei
	reti




man_interruption_interruption5::
	ex af',af
	exx
	
	xor a
	ld (__interruption4), a
	
	;cpctm_setBorder_asm HW_YELLOW
	
	ld hl, #0x39
	ld (hl), #<man_interruption_interruption0
	inc hl
	ld (hl), #>man_interruption_interruption0
	ex af',af
	exx	

	ei
	reti


man_interruption_setGameplayPalette::
	ld (__interruption1Palette), hl
	ret

man_interruption_setHUDPalette::
	ld (__interruption4Palette), hl
	ret


man_interruption_waitInterruption4::
	ld a, (__interruption4)
	or a
	jr z, man_interruption_waitInterruption4
	ret


__nopCall::
	ret

man_interruption_disableMusic::
	ld hl, #__nopCall
	ld (__callToPlayer), hl
	di
	call _PLY_AKM_STOP
	ei
	ret

man_interruption_enableMusic::
	ld hl, #_PLY_AKM_PLAY
	ld (__callToPlayer), hl
	ret

man_interruption_init::
	call cpct_waitVSYNC_asm
	halt
	halt
	di
	call cpct_waitVSYNC_asm
	ld a, #0xC3
	ld (0x38), a
	ld hl, #man_interruption_interruption0
	ld (0x39), hl
	ld a, #0xC9
	ld (0x3B), a
	ei

	ret

