.include "man/game.h.s"
.include "utils.h.s"
.include "man/scene.h.s"
.include "scene/titleImage.h.s"
.include "assets/assets.h.s"
.include "ui/lostlife.h.s"
.include "ui/hud.h.s"
.include "lib/ui.h.s"
.include "cfg.h.s"
.include "buffers.h.s"


gameStatus: .db 0x00



man_game_remainingLives:: .db 0
man_game_playerEnergy:: .db MAX_ENERGY

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Initializes all managers and systems
;; ---------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; AF, BC, DE, HL
;; ----------------------------------------------------
man_game_init::
	ld hl, #functionPlaceholder
	call man_scene_setFunctionPlaceholder
	ld hl, #scene_titleImage
	call man_scene_push

	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Updates all systems
;; ---------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; ALL
;; ----------------------------------------------------
man_game_play::
	man_game_play_loop:
		functionPlaceholder = .+1
		call #0x0000
		ld a, #MAN_GAME_STATUS_NORMAL
		ld (gameStatus), a
	jr man_game_play_loop
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Terminate all systems
;; ---------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; HL, AF, DE, BC, IX
;; ----------------------------------------------------
man_game_terminate::
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Sets the game status
;; ---------------------------------------------------
;; PARAMS
;; A = status to set
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; ----------------------------------------------------
man_game_setGameStatus::
	ld (gameStatus), a
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Gets the game status
;; ---------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; A = status
;; ----------------------------------------------------
;; DESTROYS
;; ----------------------------------------------------
man_game_getGameStatus::
	ld a, (gameStatus)
	ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Decrements the remaining lives and sets them
;; ---------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A, HL, BC
;; ----------------------------------------------------
man_game_decRemainingLives::
	ld a, (man_game_remainingLives)
	dec a
	;; NO FINISH HAS TO SET THE LIVES VV LOOK BELOW




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Sets the lives on the remaining lives variables and
;; on the different UIs
;; ---------------------------------------------------
;; PARAMS
;; A = number of remaining lives
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A, HL, BC
;; ----------------------------------------------------
man_game_setRemainingLives::
	ld (man_game_remainingLives), a
	add a, #48

	ld hl, #ui_hud_remainingLives
	ld bc, #UI_COMPONENT_STRING
	add hl, bc
	inc hl
	ld (hl), a

	ld hl, #ui_lostlife_remainingLives
	add hl, bc
	inc hl
	ld (hl), a
	
	ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Checks if remaining lives are zero
;; ---------------------------------------------------
;; PARAMS
;; Z flag = Remaining lives equals to zero
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; A
;; ----------------------------------------------------
man_game_checkRemainingLives::
	ld a, (man_game_remainingLives)
	or a
	ret


man_game_initScore::
	ld hl, #0
	ld (game_score), hl
	ld hl, #-1
	ld (game_scorePrev), hl
	ret


