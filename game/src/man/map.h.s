
.include "lib/macros.h.s"

MAP_FLAGS_CHECKPOINT_BIT = 0
MAP_FLAGS_CONTINUE_MUSIC = 1
MAP_FLAGS_CONTINUE_TIMER = 2

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DEFINE MAP FIELDS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.macro man_map_DefineMap __name
	__name::
		AddFieldW MAP_TILEMAP_CENTER
		AddFieldW MAP_TILEMAP_TOP
		AddFieldW MAP_TILEMAP_MARGIN_LEFT
		AddFieldW MAP_TILEMAP_MARGIN_RIGHT
		AddFieldB MAP_FLAGS
		AddFieldW MAP_PALETTE
		AddFieldW MAP_MUSIC
		AddFieldW MAP_FLOOR
		AddFieldW MAP_BOSS
		AddFieldW MAP_TIMER
		AddFieldB MAP_PLAYER_X
		AddFieldB MAP_PLAYER_Y
.endm


.macro man_map_AddEntity __entityTemplate, __entityX, __entityY
		.dw __entityTemplate
		.db __entityX
		.db __entityY
.endm

.macro man_map_DefineMapEnd
		.dw 0x0000
.endm
		


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Public functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.globl man_map_init
.globl man_map_loadMap
.globl man_map_nextMap
.globl man_map_backToCheckpoint

