.include "man/entity.h.s"
.include "cpctelera.h.s"
.include "sys/render.h.s"
.include "lib/opmacros.h.s"
.include "cfg.h.s"
.include "buffers.h.s"

nextEntity::
	.dw entitiesArray

lastEntity::
    .dw 0xFECA                      ;; FECAL XDD (Para ver de un vistazo que no está asignado)

numEntities::
	.db 0

nextDeletePointer::
    .dw deleteArray

numDeletePointers::
    .db 0

deleteArray::
    .ds MAX_ENTITIES_DELETED*2                      ;; 2 is the size of a pointer

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Sets all entities related data to default values
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;;  A, HL
;; ----------------------------------------------------
man_entity_init::
	xor a
	ld (numEntities), a
    ld (numDeletePointers), a                       ;; Reset delete array counter
	ld hl, #entitiesArray
	ld (nextEntity), hl
    ld hl, #deleteArray
    ld (nextDeletePointer), hl                      ;; Reset next (delete pointer) pointer
	ret




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Removes all entities and all marked to delete entities
;; except the player
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;;  A, HL
;; ----------------------------------------------------
man_entity_clearExceptPlayer::
    xor a
	ld (numDeletePointers), a                       ;; Reset delete array counter
	ld a, #1
	ld (numEntities), a
	ld hl, #entitiesArray
	ld (lastEntity), hl
	ld hl, #entitiesArray+SIZEOF_ENTITY
	ld (nextEntity), hl
    ld hl, #deleteArray
    ld (nextDeletePointer), hl                      ;; Reset next (delete pointer) pointer
	ret




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Ads an entity to the delete array, to be deleted later
;; ----------------------------------------------------
;; PARAMS
;;  HL: Pointer to entity
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;;  HL, IY
;; ----------------------------------------------------
man_entity_markForDelete::
    ld iy, (nextDeletePointer)
    ld (iy), l                      ;; Save pointer onto the next position of the delete array
    inc iy
    ld (iy), h
    inc iy
    ld (nextDeletePointer), iy      ;; Save the pointer to the next empty position of the array
    
    ld hl, #numDeletePointers
    inc (hl)                                        ;; numDeletePointers++
    
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Deletes all entities on the delete array
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;;  HL, IX, A, DE, BC
;; ----------------------------------------------------
man_entity_deleteMarked::
    ld ix, #deleteArray             ;; IX = Pointer to delete pointer
    ld a, (numDeletePointers)       ;; A = Counter
    ld (_deleteMarked_counter), a   ;; Save counter
    
_deleteMarked_loop:
    
        or a
        jp z, _deleteMarked_endLoop

        ld l, (ix)                      ;; Load entity pointer on HL
        ld h, 1(ix)

        push ix
        push hl
        ld a, l
        ld__ixl_a
        ld a, h
        ld__ixh_a
        ld d, ENT_PREV_SCREEN_PTR_H(ix)
        ld e, ENT_PREV_SCREEN_PTR_L(ix)

        call sys_render_eraseBlendedSprite       ;; Erase the sprite on the screen
        pop hl

        call man_entity_deleteEntity
        pop ix

        _deleteMarked_counter = .+1
        ld a, #00                       ;; AUTOMODIFIABLE A = Counter
        dec a
        ld (_deleteMarked_counter), a
        ld bc, #2                       ;; BC = Size of pointer
        add ix, bc                      ;; Make ix point to the next pointer
        jp _deleteMarked_loop
_deleteMarked_endLoop:

    ;; Reset delete array
    ld hl, #deleteArray
    ld (nextDeletePointer), hl
    xor a                               ;; Set a to 0
    ld (numDeletePointers), a

    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Deletes the entity pointed by hl 
;; WARNING DO NOT USE OUTSIDE. ONLY FOR INTERNAL USE.
;; CALL man_entity_markForDelete instead
;; ----------------------------------------------------
;; PARAMS
;; HL: Entity to delete
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;;  HL, IX, IY, A, DE
;; ----------------------------------------------------
man_entity_deleteEntity::
    ld d, h                         ;; DE = Pointer where to copy data (entity to delete)
    ld e, l

    ;; LastEntity = NextEntity - SIZEOF_ENTITY
    ld hl, (nextEntity)             ;; HL = Pointer to data to be copied (last entity)
    ld a, l
    sub #SIZEOF_ENTITY
    ld l, a
    ld a, h
    sbc #0              ;; Substract carry if last substraction overflowed
    ld h, a
    ld (nextEntity), hl ;; Make next entity point to the last entity, as array is reduced

    call man_entity_rearrangeAll

    ld bc, #SIZEOF_ENTITY

    ldir                            ;; Copy last entity on entity to delete

    ;;numEntities--
    ld hl, #numEntities
    dec (hl)

    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Rearranges all pointer arrays based on the entity deleted pointer (DE)
;; and the last entity pointer (HL)
;; ----------------------------------------------------
;; PARAMS
;; DE: New pointer
;; HL: Last entity pointer
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;;  IX, A, C
;; ----------------------------------------------------
man_entity_rearrangeAll::
    ld ix, #deleteArray
    ld a, (numDeletePointers)
    ld c, a
    call man_entity_rearrange   ;; Rearrange delete array
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Changes THE FIRST encounter of last entity pointer to the pointer in DE
;; WARNING, IT IS EXPECTED THAT ARRAYS OF POINTERS DO NOT HAVE REPEATED POINTERS
;; ----------------------------------------------------
;; PARAMS
;; DE: New pointer
;; HL: Last entity pointer
;; IX: Array of pointers
;; C : Size of array
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;;  IX, A, C
;; ----------------------------------------------------
man_entity_rearrange::
    xor a                       ;; Reset a
    cp c                        ;; Check if C = 0
    ret z                       ;; If C = 0 there are no pointers to check

    ;; Check if current pointer is equal to HL pointer
    ld a, (ix)                  ;; Load low pointer address to compare
    cp l
    jp nz, _not_equal
    ld a, 1(ix)                 ;; Load high pointer address to compare
    cp h
    jp nz,_not_equal

        ;; Change pointer to DE (New pointer) pointer
        ld (ix), e
        ld 1(ix), d
        ret                     ;; If found, there's no point in keep searching

_not_equal:
    ;; Change counters
    dec c
    inc ix
    inc ix
    jp man_entity_rearrange
;; NO RETURN, as it is above


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Creates an entity
;; ----------------------------------------------------
;; PARAMS
;; HL: Pointer to entity data values
;; ----------------------------------------------------
;; RETURNS
;; HL: Address of the entity created
;; ----------------------------------------------------
;; DESTROYS
;; DE, BC, HL
;; ----------------------------------------------------
man_entity_createEntity::

	;; *nextEntity = entityData
	ld de, (nextEntity)   ;; Store next entity ptr on de
	ld bc, #SIZEOF_ENTITY ;; Store the size of one entity on bc
	ldir                  ;; Copy default entity to next entity position

	;; numEntities++
	ld hl, #numEntities
	inc (hl)

	;; nextEntity += SIZEOF_ENTITY
	ld hl, (nextEntity)   ;; Gets created entitie address
    ld (lastEntity), hl   ;; Sets the last entity
	ld (nextEntity), de   ;; Store the new next entity position on nextEntity

	ret




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Restores player data
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; DE, BC, HL
;; ----------------------------------------------------
man_entity_restorePlayer::
	.globl player
	ld hl, #player
	ld de, #entitiesArray
	ld bc, #SIZEOF_ENTITY
	ldir
	ret





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Iterates over entitites array and calls the function
;; passed on hl, each entity pointer is stored on IX
;; ----------------------------------------------------
;; PARAMS
;; HL = function to call
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, A, HL, BC
;; ----------------------------------------------------
man_entity_forallEntities::
	ld ix, #entitiesArray   ;; [14]
	ld (functionToCall), hl ;; [20] Save pointer to function
	ld a, (numEntities)     ;; [7]
	or a                    ;; [4] Sets the flags
	ret z                   ;; [11/5] if a == 0 return
	loop:
		ld (counter), a     ;; [13]

		functionToCall = .+1
		call #0x0000        ;; [17]

		;; Points to next entity
		ld bc, #SIZEOF_ENTITY ;; [10]
		add ix, bc          ;; [15]

		counter = .+1
		ld a, #0            ;; [7] Restore num entities
		dec a               ;; [4] dec num entities

		jr nz, loop         ;; [12/7]
	ret




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Iterates over entitites array and calls the function
;; passed on hl if the entity matches the signature
;; passed on A, each entity pointer is stored on IX
;; ----------------------------------------------------
;; PARAMS
;; HL = function to call
;; A  = signature L
;; B  = signature H
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, A, HL, BC, D
;; ----------------------------------------------------
man_entity_forallMatchingEntities::
	ld ix, #entitiesArray   ;; [14]
	ld (funcToCallForAllMatching), hl ;; [20] Save pointer to function
	ld (signatureLForAllMatching), a
	ld a, b
	ld (signatureHForAllMatching), a
	ld a, (numEntities)     ;; [7]
	or a                    ;; [4] Sets the flags
	ret z                   ;; [11/5] if a == 0 return
	_loopForAllMatching:
		ld (counterForAllMatching), a     ;; [13]

		signatureLForAllMatching = .+1
		ld d, #0x00
		ld a, ENT_COMPONENTS_L(ix)
		and d
		cp d
		jr nz, __notSignatureLForAllMatching

		signatureHForAllMatching = .+1
		ld d, #0x00
		ld a, ENT_COMPONENTS_H(ix)
		and d
		cp d
		
		funcToCallForAllMatching = .+1
		call z, #0x0000        ;; [17]

		__notSignatureLForAllMatching:

		;; Points to next entity
		ld bc, #SIZEOF_ENTITY ;; [10]
		add ix, bc          ;; [15]

		counterForAllMatching = .+1
		ld a, #0            ;; [7] Restore num entities
		dec a               ;; [4] dec num entities

		jr nz, _loopForAllMatching         ;; [12/7]
	ret





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Iterates over all entities and compares all of them
;; using a function passed on hl, entityA is stored on
;; IX and entityB on IY
;; ---------------------------------------------------
;; CODE
;; entA = entitiesArray
;; FOR i=numEntities; i!=0; --i
;;   entB = entA+ENTITY_SIZE
;;   FOR j=i-1; j!=0; --j
;;     func(entA, entB)
;;     entB+=ENTITY_SIZE
;;   entA+=ENTITY_SIZE
;; ----------------------------------------------------
;; PARAMS
;; HL = function pointer to call
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, IY, A, BC
;; ----------------------------------------------------
man_entity_compareAllEntities::
	ld (compareAllEntitiesFunctionToCall), hl ;; [20] Save pointer to function
	;; entA = entitiesArray
	ld ix, #entitiesArray
	
	;; FOR
	ld a, (numEntities)        ;; i=numEntities
	or a                       ;; Set flags
	outerLoop:
		ret z                  ;; i!=0
		ld (iCounter), a       ;; Store i on iCounter position

        ;; entB = entA+SIZEOF_ENTITY
		push ix
		pop iy
		ld bc, #SIZEOF_ENTITY    ;; [10]
		add iy, bc             ;; [15]
		
		;; FOR
		dec a                  ;; j=i-1
		innerLoop:
			jr z, endInnerLoop ;; j!=0
			ld (jCounter), a   ;; Store j on jCounter position
			
			;; CALLS TO FUNCTION
			compareAllEntitiesFunctionToCall = .+1
			call #0x0000        ;; [17]

			jCounter = .+1      ;; 
			ld a, #00           ;; Recovers j
			dec a               ;; --j
			
			;; entB += SIZEOF_ENTITY
			ld bc, #SIZEOF_ENTITY ;; [10]
			add iy, bc          ;; [15]

			jr innerLoop        ;; Returns to the start of inner loop

		endInnerLoop:

		;; entA += SIZEOF_ENTITY
		ld bc, #SIZEOF_ENTITY ;; [10]
		add ix, bc          ;; [15]
		
		iCounter = .+1      ;; 
		ld a, #00           ;; Recovers i
		dec a               ;; --i
		
		jr outerLoop        ;; Returns to the start of the loop



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Iterates over all entities and compares all of them
;; using a function passed on hl, entityA is stored on
;; IX and entityB on IY
;; ---------------------------------------------------
;; CODE
;; entA = entitiesArray
;; FOR i=numEntities; i!=0; --i
;;   if entA.components & signature == signature
;;		entB = entA+ENTITY_SIZE
;;   	FOR j=i-1; j!=0; --j
;;        if entB.components & signature == signature
;;			func(entA, entB)
;;   	  entB+=ENTITY_SIZE
;;   entA+=ENTITY_SIZE
;; ----------------------------------------------------
;; PARAMS
;; HL = function pointer to call
;; A  = signature
;; B  = signature H
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; IX, IY, A, BC
;; ----------------------------------------------------
man_entity_compareAllMatchingEntities::
	ld (compareAllMatchingEntitiesFunctionToCall), hl ;; [20] Save pointer to function
	ld (signatureLForCompareAllMatchingA), a
	ld (signatureLForCompareAllMatchingB), a
	ld a, b
	ld (signatureHForCompareAllMatchingA), a
	ld (signatureHForCompareAllMatchingB), a
	
	;; entA = entitiesArray
	ld ix, #entitiesArray
	
	;; FOR
	ld a, (numEntities)        ;; i=numEntities
	or a                       ;; Set flags
	outerLoopCompareAllMatching:
		ret z                  ;; i!=0
		ld (iCounterCompareAllMatching), a       ;; Store i on iCounter position
		ld (jCounterCompareAllMatching), a       ;; Store j on jCounter position
		
		signatureLForCompareAllMatchingA = .+1
		ld d, #0x00
		ld a, ENT_COMPONENTS_L(ix)
		and d
		cp d

		jr nz, __nextCompareAllMatching

		signatureHForCompareAllMatchingA = .+1
		ld d, #0x00
		ld a, ENT_COMPONENTS_H(ix)
		and d
		cp d
		
		jr nz, __nextCompareAllMatching
        
		;; entB = entA+SIZEOF_ENTITY
		push ix
		pop iy
		ld bc, #SIZEOF_ENTITY    ;; [10]
		add iy, bc             ;; [15]
		
		;; FOR
		innerLoopCompareAllMatching:
			jCounterCompareAllMatching = .+1     ;; 
			ld a, #00                            ;; Recovers j
			dec a                                ;; j=i-1
			jr z, endInnerLoopCompareAllMatching ;; j!=0
			ld (jCounterCompareAllMatching), a   ;; Store j on jCounter position


			signatureLForCompareAllMatchingB = .+1
			ld d, #0x00
			ld a, ENT_COMPONENTS_L(iy)
			and d
			cp d
		
			jr nz, __notSignatureLForCompareAllMatchingB

			signatureHForCompareAllMatchingB = .+1
			ld d, #0x00
			ld a, ENT_COMPONENTS_H(iy)
			and d
			cp d

			;; CALLS TO FUNCTION
			compareAllMatchingEntitiesFunctionToCall = .+1
			call z, #0x0000        ;; [17]
			
			__notSignatureLForCompareAllMatchingB:
			
			;; entB += SIZEOF_ENTITY
			ld bc, #SIZEOF_ENTITY ;; [10]
			add iy, bc          ;; [15]

			jr innerLoopCompareAllMatching        ;; Returns to the start of inner loop

		endInnerLoopCompareAllMatching:

		__nextCompareAllMatching:

		;; entA += SIZEOF_ENTITY
		ld bc, #SIZEOF_ENTITY ;; [10]
		add ix, bc          ;; [15]
		
		iCounterCompareAllMatching = .+1      ;; 
		ld a, #00           ;; Recovers i
		dec a               ;; --i
		
		jr outerLoopCompareAllMatching        ;; Returns to the start of the loop





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Stores player ptr on ix
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; IX = player entity ptr
;; ----------------------------------------------------
;; DESTROYS
;; ----------------------------------------------------
man_entity_getPlayer::
	ld ix, #entitiesArray
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Stores boss ptr on iy
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; IY = boss entity ptr
;; ----------------------------------------------------
;; DESTROYS
;; ----------------------------------------------------
man_entity_getBoss::
	ld iy, #entitiesArray+SIZEOF_ENTITY
	ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Sets the position of the entity
;; ----------------------------------------------------
;; PARAMS
;; B: x
;; C: y
;; IX: entity
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; HL
;; ----------------------------------------------------
man_entity_setPosition::
	ld ENT_X(ix), b
	ld l, b
	ld h, #0
	shiftl_hl WORLD_SCALE_X_DISP
	ld ENT_X_WORLD_H(ix), h
	ld ENT_X_WORLD_L(ix), l

	ld ENT_Y(ix), c
	ld l, c
	ld h, #0
	shiftl_hl WORLD_SCALE_Y_DISP
	ld ENT_Y_WORLD_H(ix), h
	ld ENT_Y_WORLD_L(ix), l
	ret

