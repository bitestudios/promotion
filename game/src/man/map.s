.include "man/entity.h.s"
.include "man/map.h.s"
.include "man/interruption.h.s"
.include "man/game.h.s"
.include "factory/factory.h.s"
.include "assets/assets.h.s"
.include "sys/render.h.s"
.include "lib/ui.h.s"
.include "lib/wait.h.s"
.include "ui/hud.h.s"
.include "utils.h.s"
.include "cpctelera.h.s"
.include "maps/maps.h.s"
.include "cfg.h.s"



man_map_flags : .db 0


man_map_maps::
    .dw mapHallStart
    .dw mapPlatforms00x00
    .dw mapPlatforms00x01
    .dw mapPlatforms00x02
    .dw mapPlatforms00x03
    .dw mapPlatforms00x04
    .dw mapPlatforms00x05
    .dw mapPlatforms00x06
    .dw mapBossCenter00
    .dw mapHallFirstFloorEnd

    .dw mapHallSecondFloorStart
    .dw mapPlatforms01x00
    .dw mapPlatforms01x01
    .dw mapPlatforms01x02
    .dw mapPlatforms01x03
    .dw mapPlatforms01x04
    .dw mapPlatforms01x05
    .dw mapPlatforms01x06
    .dw mapPlatforms01x07
    .dw mapBossCenter01
    .dw mapHallSecondFloorEnd

    .dw mapHallThirdFloorStart
    .dw mapPlatforms02x00
    .dw mapPlatforms02x01
    .dw mapPlatforms02x02
    .dw mapPlatforms02x03
    .dw mapPlatforms02x04
    .dw mapPlatforms02x05
    .dw mapPlatforms02x06
    .dw mapPlatforms02x07
    .dw mapPlatforms02x08
    .dw mapPlatforms02x09
	.dw mapBossCenter02
    .dw mapHallThirdFloorEnd
    
	.dw mapHallFourthFloorStart
	.dw mapPlatforms03x00
	.dw mapPlatforms03x01
	.dw mapPlatforms03x02
	.dw mapPlatforms03x03
	.dw mapPlatforms03x04
	.dw mapPlatforms03x05
	.dw mapPlatforms03x06
	.dw mapPlatforms03x07
	.dw mapPlatforms03x08
	.dw mapPlatforms03x09
	.dw mapPlatforms03x010
	.dw mapPlatforms03x011
	.dw mapPlatforms03x012
	.dw mapPlatforms03x013
	.dw mapPlatforms03x014
	.dw mapPlatforms03x015
	.dw mapPlatforms03x016
	.dw mapPlatforms03x017
	.dw mapPlatforms03x018
	.dw mapPlatforms03x019
	.dw mapBossCenter03

	.dw 0x0000

man_map_currentMap::
	.dw man_map_maps

man_map_backsFromCheckpoint::
	.db 0

man_map_checkpoint::
	.dw man_map_maps


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Resets the current map en the checkpoint
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; HL
;; ----------------------------------------------------
man_map_init::
	ld hl, #man_map_maps
	ld (man_map_currentMap), hl
	ld (man_map_checkpoint), hl
	xor a
	ld (man_map_backsFromCheckpoint), a
	ret





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Set next map
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; Z flag activated if map is null
;; ----------------------------------------------------
;; DESTROYS
;; HL, AF
;; ----------------------------------------------------
man_map_nextMap::
	xor a
	ld (man_map_backsFromCheckpoint), a
	ld hl, (man_map_currentMap)
	inc hl
	inc hl
	ld (man_map_currentMap), hl
	ld a, (hl)
	inc hl
	or (hl)
	ret





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Set current map to checkpoint
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; HL
;; ----------------------------------------------------
man_map_backToCheckpoint::
	ld hl, (man_map_checkpoint)
	ld (man_map_currentMap), hl
	ld a, #1
	ld (man_map_backsFromCheckpoint), a
	ret






;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DESCRIPTION
;; Loads the current map data
;; ----------------------------------------------------
;; PARAMS
;; ----------------------------------------------------
;; RETURNS
;; ----------------------------------------------------
;; DESTROYS
;; HL, AF, DE, BC, IX
;; ----------------------------------------------------
man_map_loadMap::
	ld hl, (man_map_currentMap)        ;;- Loads the pointer to a pointer to the current map on hl

	ld a, (hl)                         ;;\
	inc hl                             ;; |_Loads the pointer to the current map on hl
	ld h, (hl)                         ;; | 
	ld l, a                            ;;/
	
	call man_map_setTilemaps

	call man_map_setMapFlags
	call man_map_setCheckpoint
	call man_map_setPalette
	call man_map_setMusic
	call man_map_setFloor
	call man_map_setBossName
	call man_map_setTimer
	call man_map_setPlayer

	
	__man_map_loadMapLoop:
		ld e, (hl)                    ;;\
		inc hl                        ;; |_ Loads the pointer to the entity template on DE
		ld d, (hl)                    ;; |
		inc hl                        ;;/

		ld a, e                       ;;\
		or d                          ;; |- Checks if DE is a null pointer
		ret z                         ;;/

		push hl                       ;; Saves the pointer used on the stack
		ex de, hl                     ;; Loads the pointer to the template on hl
		call man_entity_createEntity  ;; Creates the entity (pointer to the entity on HL)
    
		ex de, hl                     ;;\
		ld__ixh_d                     ;; \- Stores the new entity on IX
		ld__ixl_e                     ;;/
		
		pop hl                        ;; Recovers hl

		ld b, (hl)                    ;; Stores the X coordinate
		inc hl                        ;;
		ld c, (hl)                    ;; Stores the Y coordinate
		inc hl                        ;;

		ex de, hl                     ;;- Sets the position of the player
		call man_entity_setPosition   ;; |
		ex de, hl                     ;;/

		jr __man_map_loadMapLoop



man_map_setMapFlags::
	ld a, (hl)                ;;- Stores the flags on A
	inc hl                    ;;- Makes HL point to the next value
	ld (man_map_flags), a     ;;- Stores the flag on the aux variable
	ret


man_map_setCheckpoint::
	ld a, (man_map_flags)             ;;- Loads the flags on A from the aux variable
	bit MAP_FLAGS_CHECKPOINT_BIT, a   ;;- Check if the checkpoint flag is enabled
	ret z
	ex de, hl                         ;;- Backups HL on DE
	ld hl, (man_map_currentMap)       ;;- Loads the current map pointer on HL
	ld (man_map_checkpoint), hl       ;;- Stores the current map pointer on the checkpoint map
	ex de, hl                         ;;- Recovers the HL value
	ret



man_map_setPalette::
	ld e, (hl)                         ;;\
	inc hl                             ;; |_ Loads the palette pointer on de
	ld d, (hl)                         ;; |
	inc hl                             ;;/
	ld (sys_render_actualPalette), de
	ret






man_map_setPlayer::
	call man_entity_getPlayer         ;;- Gets a pointer to the played on IX

	res #RENDER_ERASE_BIT, ENT_RENDER_FLAGS(ix) ;;- The player dont should be erased

	ld a, #JUMP_STATUS_QUIET
	ld ENT_JUMP_STATUS(ix), a

	xor a
	ld ENT_VY(ix), a
	
	xor a
	ld ENT_ACCELERATION_X(ix), a

	set #COMPONENT_L_COLLISION_BIT, ENT_COMPONENTS_L(ix)
	set #COMPONENT_L_HEALTH_BIT, ENT_COMPONENTS_L(ix)

	ld b, (hl)                        ;; Stores the X coordinate of the player
	inc hl                            ;;
	ld c, (hl)                        ;; Stores the Y coordinate of the player
	inc hl                            ;;

	ex de, hl                         ;;- Sets the position of the player
	call man_entity_setPosition       ;; |
	ex de, hl                         ;;/
	ret





man_map_setMusic::
	ld a, (man_map_backsFromCheckpoint)  ;;\
	or a                                 ;; |- If map backs from checkpoint has to reload music
	jr nz, __man_maps_reloadMusic        ;;/ \_ always

		ld a, (man_map_flags)            ;;\
		bit MAP_FLAGS_CONTINUE_MUSIC, a  ;; |- If the music continue flag is enabled has to no
		jr z, __man_maps_reloadMusic     ;;/ \_ reloads the music

		inc hl                           ;;\_ Makes HL point to the next value before ret
		inc hl                           ;;/
		ret

	__man_maps_reloadMusic:
	ld e, (hl)                         ;;\
	inc hl                             ;; |_ Loads the music pointer on DE
	ld d, (hl)                         ;; |
	inc hl                             ;;/

	push hl                            ;;-Stores HL on the stack
	push de                            ;;- Stores DE (music pointer) on the stack
	
	ld a, #10
	call bite_waitCycles

	pop hl                             ;;- Recovers the music pointer on HL

    ld a, #0                           ;;\_Inits the song of the map
    di
	call _PLY_AKM_INIT                 ;;/
	ei

	pop hl                             ;;- Recovers HL

	ret








man_map_setTilemaps::


    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; TILEMAP CENTER
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ld e, (hl)                           ;;\
	inc hl                               ;; |_ Loads the pointer to the tilemap center compressed
	ld d, (hl)                           ;; |\ on DE
	inc hl                               ;;/

	push hl                              ;;- Stores HL on the stack

	ex de, hl                            ;;- Moves the pointer to the compressed tilemap to HL
	ld de, #game_tilemapCenterEnd        ;;- Stores the paceholder target on DE
	call cpct_zx7b_decrunch_s_asm        ;;- Uncompress

	pop hl                               ;;- Recovers HL from the stack
	

	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; TILEMAP TOP
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ld e, (hl)                           ;;\
	inc hl                               ;; |_ Loads the tilemap top compressed on DE
	ld d, (hl)                           ;; |
	inc hl                               ;;/

	push hl                              ;;- Stores HL on the stack

	ex de, hl                            ;;- Moves the pointer to the compressed tilemap to HL
	ld de, #game_tilemapTopEnd           ;;- Stores the paceholder target on DE
	call cpct_zx7b_decrunch_s_asm        ;;- Uncompress

	pop hl                               ;;- Recovers HL from the stack
	

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; TILEMAP MARGIN LEFT
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ld e, (hl)                          ;;\
	inc hl                              ;; |_ Loads the tilemap margin left compressed on DE
	ld d, (hl)                          ;; |
	inc hl                              ;;/
	
	push hl                              ;;- Stores HL on the stack

	ex de, hl                            ;;- Moves the pointer to the compressed tilemap to HL
	ld de, #game_tilemapMarginLeftEnd    ;;- Stores the paceholder target on DE
	call cpct_zx7b_decrunch_s_asm        ;;- Uncompress

	pop hl                               ;;- Recovers HL from the stack
	
    
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; TILEMAP MARGIN RIGHT
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ld e, (hl)                           ;;\
	inc hl                               ;; |_ Loads the tilemap margin right compressed on DE
	ld d, (hl)                           ;; |
	inc hl                               ;;/
	
	push hl                              ;;- Stores HL on the stack

	ex de, hl                            ;;- Moves the pointer to the compressed tilemap to HL
	ld de, #game_tilemapMarginRightEnd   ;;- Stores the paceholder target on DE
	call cpct_zx7b_decrunch_s_asm        ;;- Uncompress

	pop hl                               ;;- Recovers HL from the stack
	
	ret


man_map_setFloor::
	push hl
	ld ix, #ui_hud_floorNode
	call bite_ui_componentNode_clearChilds
	pop hl

	ld e, (hl)
	inc hl
	ld d, (hl)
	inc hl
	push hl
	call bite_ui_componentNode_addChild
	pop hl
	ret

man_map_setBossName::
	push hl
	ld ix, #ui_hud_bossNode
	call bite_ui_componentNode_clearChilds
	pop hl

	ld e, (hl)
	inc hl
	ld d, (hl)
	inc hl
	push hl
	call bite_ui_componentNode_addChild
	pop hl
	ret




man_map_setTimer::
	xor a
	ld (man_game_timerStopped), a
	ld a, (man_map_backsFromCheckpoint)  ;;\
	or a                                 ;; |- If map backs from checkpoint has to reload the timer
	jr nz, __man_maps_reloadTimer        ;;/ \_ always

		ld a, (man_map_flags)            ;;\
		bit MAP_FLAGS_CONTINUE_TIMER, a  ;; |- If the timer continue flag is enabled has to no
		jr z, __man_maps_reloadTimer     ;;/ \_ reloads the timer

		inc hl                           ;;\_ Makes HL point to the next value before ret
		inc hl                           ;;/
		ret

	__man_maps_reloadTimer:

	ld e, (hl)             ;;\
	inc hl                 ;; |_ Loads the timer on DE
	ld d, (hl)             ;; |
	inc hl                 ;;/

	ld (man_game_timer), de

	ret

