

MAP_SOURCE = maps.json
MAP_OUT = src/maps/maps

MAP_FILES = $(MAP_OUT).s $(MAP_OUT).h.s

TILED_CONVERSOR = utils/tiledconversor

$(MAP_OUT).s : $(MAP_SOURCE)
	$(TILED_CONVERSOR) $(MAP_SOURCE) $(MAP_OUT) > obj/tiledconv.log
	
$(eval OBJS2CLEAN         := $(OBJS2CLEAN) $(MAP_FILES))

