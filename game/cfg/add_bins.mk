
BIN_SRC = bins/title.bin
BIN_OUT = src/assets/title.s
BIN2ASM_ADDR = 0xC000
BIN2ASM_NAME = titleImage
MAP_FILES = $(MAP_OUT).s $(MAP_OUT).h.s

BIN2ASM = utils/bin2asm
BIN2ASM_FILES = $(BIN_OUT)

$(BIN_OUT) : $(BIN_SRC)
	$(BIN2ASM) $(BIN_SRC) $(BIN2ASM_NAME) $(BIN2ASM_ADDR) > $(BIN_OUT)
	
$(eval OBJS2CLEAN         := $(OBJS2CLEAN) $(BIN_OUT))

