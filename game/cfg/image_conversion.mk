##-----------------------------LICENSE NOTICE------------------------------------
##  This file is part of CPCtelera: An Amstrad CPC Game Engine 
##  Copyright (C) 2018 ronaldo / Fremos / Cheesetea / ByteRealms (@FranGallegoBR)
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU Lesser General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU Lesser General Public License for more details.
##
##  You should have received a copy of the GNU Lesser General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.
##------------------------------------------------------------------------------
############################################################################
##                        CPCTELERA ENGINE                                ##
##                 Automatic image conversion file                        ##
##------------------------------------------------------------------------##
## This file is intended for users to automate image conversion from JPG, ##
## PNG, GIF, etc. into C-arrays.                                          ##
############################################################################

##
## NEW MACROS
##

## 16 colours palette
#PALETTE=0 1 2 3 6 9 11 12 13 15 16 18 20 24 25 26

## Default values
#$(eval $(call IMG2SP, SET_MODE        , 0                  ))  { 0, 1, 2 }
#$(eval $(call IMG2SP, SET_MASK        , none               ))  { interlaced, none }
#$(eval $(call IMG2SP, SET_FOLDER      , src/               ))
#$(eval $(call IMG2SP, SET_EXTRAPAR    ,                    ))
#$(eval $(call IMG2SP, SET_IMG_FORMAT  , sprites            ))	{ sprites, zgtiles, screen }
#$(eval $(call IMG2SP, SET_OUTPUT      , c                  ))  { bin, c }
#$(eval $(call IMG2SP, SET_PALETTE_FW  , $(PALETTE)         ))
#$(eval $(call IMG2SP, CONVERT_PALETTE , $(PALETTE), g_palette ))
#$(eval $(call IMG2SP, CONVERT         , img.png , w, h, array, palette, tileset))

##
## OLD MACROS (For compatibility)
##

## Example firmware palette definition as variable in cpct_img2tileset format

# PALETTE={0 1 3 4 7 9 10 12 13 16 19 20 21 24 25 26}

## AUTOMATED IMAGE CONVERSION EXAMPLE (Uncomment EVAL line to use)
##
##    This example would convert img/example.png into src/example.{c|h} files.
##    A C-array called pre_example[24*12*2] would be generated with the definition
##    of the image example.png in mode 0 screen pixel format, with interlaced mask.
##    The palette used for conversion is given through the PALETTE variable and
##    a pre_palette[16] array will be generated with the 16 palette colours as 
##	  hardware colour values.

#$(eval $(call IMG2SPRITES,img/example.png,0,pre,24,12,$(PALETTE),mask,src/,hwpalette))



############################################################################
##              DETAILED INSTRUCTIONS AND PARAMETERS                      ##
##------------------------------------------------------------------------##
##                                                                        ##
## Macro used for conversion is IMG2SPRITES, which has up to 9 parameters:##
##  (1): Image file to be converted into C sprite (PNG, JPG, GIF, etc)    ##
##  (2): Graphics mode (0,1,2) for the generated C sprite                 ##
##  (3): Prefix to add to all C-identifiers generated                     ##
##  (4): Width in pixels of each sprite/tile/etc that will be generated   ##
##  (5): Height in pixels of each sprite/tile/etc that will be generated  ##
##  (6): Firmware palette used to convert the image file into C values    ##
##  (7): (mask / tileset / zgtiles)                                       ##
##     - "mask":    generate interlaced mask for all sprites converted    ##
##     - "tileset": generate a tileset array with pointers to all sprites ##
##     - "zgtiles": generate tiles/sprites in Zig-Zag pixel order and     ##
##                  Gray Code row order                                   ##
##  (8): Output subfolder for generated .C/.H files (in project folder)   ##
##  (9): (hwpalette)                                                      ##
##     - "hwpalette": output palette array with hardware colour values    ##
## (10): Aditional options (you can use this to pass aditional modifiers  ##
##       to cpct_img2tileset)                                             ##
##                                                                        ##
## Macro is used in this way (one line for each image to be converted):   ##
##  $(eval $(call IMG2SPRITES,(1),(2),(3),(4),(5),(6),(7),(8),(9), (10))) ##
##                                                                        ##
## Important:                                                             ##
##  * Do NOT separate macro parameters with spaces, blanks or other chars.##
##    ANY character you put into a macro parameter will be passed to the  ##
##    macro. Therefore ...,src/sprites,... will represent "src/sprites"   ##
##    folder, whereas ...,  src/sprites,... means "  src/sprites" folder. ##
##                                                                        ##
##  * You can omit parameters but leaving them empty. Therefore, if you   ##
##  wanted to specify an output folder but do not want your sprites to    ##
##  have mask and/or tileset, you may omit parameter (7) leaving it empty ##
##     $(eval $(call IMG2SPRITES,imgs/1.png,0,g,4,8,$(PAL),,src/))        ##
############################################################################

## 16 colours palette
PALETTE_TITLE = 0 2 3 6 9 12 13 11 15 16 18 21 23 24 25 26
PALETTE_DEFAULT =    2  3  0  5   26 24 15 26   0  0  10 11   1  0  14 15
PALETTE_HUD     =    0  13 1  1   13 13 6  26   0  26 9  18   1  2  14 15

PALETTE_00      =    3  5  7  25  26 26 26 26   0  0  0  0    9  9  9  9
PALETTE_01      =    1  2  11 20  26 26 26 26   0  0  0  0    8  8  8  8
PALETTE_02      =    9  11 22 23  26 26 26 26   0  0  0  0    4  4  4  4
PALETTE_03      =    4  11 2  22  26 26 26 26   0  0  0  0    6  6  6  6
PALETTE_04      =    0  12 24 26  13 13 13 13   1  1  1  1    19 19 19 19

PALETTE_SPRITES =    13 1  1  1   1  1  1  26   0  1  1  1    2  1  1  1
PALETTE_FONT    =    13 10 0  10  2 26 6  10   10 10  10 10   1  3 10 10
PALETTE_TILESET =    4  5  8 17   0  0  0  0    0  0  0  0    0  0  0  0


## Default values
$(eval $(call IMG2SP, SET_MODE        , 0                  ))  # { 0, 1, 2 }
$(eval $(call IMG2SP, SET_MASK        , none               )) # { interlaced, none }
$(eval $(call IMG2SP, SET_FOLDER      , src/assets         ))
# $(eval $(call IMG2SP, SET_EXTRAPAR    ,                    ))
$(eval $(call IMG2SP, SET_IMG_FORMAT  , sprites            )) #	{ sprites, zgtiles, screen }
$(eval $(call IMG2SP, SET_OUTPUT      , c                  )) # { bin, c }

$(eval $(call IMG2SP, SET_IMG_FORMAT  , sprites            ))










###########################################################################
#
# PALETTES
#
##########################################################################
$(eval $(call IMG2SP, SET_FOLDER      , src/assets/palettes         ))
$(eval $(call IMG2SP, CONVERT_PALETTE , $(PALETTE_TITLE), palette_title ))
$(eval $(call IMG2SP, CONVERT_PALETTE , $(PALETTE_DEFAULT), palette_default ))
$(eval $(call IMG2SP, CONVERT_PALETTE , $(PALETTE_HUD), palette_hud ))
$(eval $(call IMG2SP, CONVERT_PALETTE , $(PALETTE_00), palette_00 ))
$(eval $(call IMG2SP, CONVERT_PALETTE , $(PALETTE_01), palette_01 ))
$(eval $(call IMG2SP, CONVERT_PALETTE , $(PALETTE_02), palette_02 ))
$(eval $(call IMG2SP, CONVERT_PALETTE , $(PALETTE_03), palette_03 ))
$(eval $(call IMG2SP, CONVERT_PALETTE , $(PALETTE_04), palette_04 ))











###########################################################################
#
# PLAYER
#
##########################################################################
$(eval $(call IMG2SP, SET_FOLDER      , src/assets         ))
$(eval $(call IMG2SP, SET_PALETTE_FW  , $(PALETTE_SPRITES)         ))
$(eval $(call IMG2SP, CONVERT         , assets/main-char.png , 8, 16, sprite_player))






###########################################################################
#
# BOSSES
#
##########################################################################
$(eval $(call IMG2SP, SET_FOLDER      , src/assets/bosses         ))
$(eval $(call IMG2SP, SET_PALETTE_FW  , $(PALETTE_SPRITES)         ))
$(eval $(call IMG2SP, CONVERT         , assets/bosses/boss1.png , 14,22, boss1))
$(eval $(call IMG2SP, CONVERT         , assets/bosses/boss2.png , 14,27, boss2))
$(eval $(call IMG2SP, CONVERT         , assets/bosses/boss3.png , 14,23, boss3))
$(eval $(call IMG2SP, CONVERT         , assets/bosses/boss4.png , 14,27, boss4))






###########################################################################
#
# ENEMIES
#
##########################################################################
$(eval $(call IMG2SP, SET_FOLDER      , src/assets/enemies         ))
$(eval $(call IMG2SP, SET_PALETTE_FW  , $(PALETTE_SPRITES)         ))
$(eval $(call IMG2SP, CONVERT         , assets/enemies/minions.png , 8, 15, sprite_minions))
$(eval $(call IMG2SP, CONVERT         , assets/enemies/bird.png , 8, 8, sprite_bird))
$(eval $(call IMG2SP, CONVERT         , assets/enemies/rat.png , 10, 7, sprite_rat))
$(eval $(call IMG2SP, CONVERT         , assets/enemies/spikes.png , 16,20, spikes))







###########################################################################
#
# OBJECTS
#
##########################################################################
$(eval $(call IMG2SP, SET_FOLDER      , src/assets/objects         ))
$(eval $(call IMG2SP, SET_PALETTE_FW  , $(PALETTE_SPRITES)         ))
$(eval $(call IMG2SP, CONVERT         , assets/objects/lollipop.png , 6,14, lollipop))
$(eval $(call IMG2SP, CONVERT         , assets/indicator.png , 8,8, indicator))












###########################################################################
#
# SHOTS
#
##########################################################################
$(eval $(call IMG2SP, SET_FOLDER      , src/assets/shots         ))
$(eval $(call IMG2SP, SET_PALETTE_FW  , $(PALETTE_SPRITES)         ))
$(eval $(call IMG2SP, CONVERT         , assets/shots/main-char-shot.png , 4, 4, sprite_player_shot))
$(eval $(call IMG2SP, CONVERT         , assets/shots/boss-shot.png , 8,8, sprite_boss_shot))
# $(eval $(call IMG2SP, CONVERT         , assets/shots/boomerang.png , 8,16, boomerang))













###########################################################################
#
# EXPLOSIONS
#
##########################################################################
$(eval $(call IMG2SP, SET_FOLDER      , src/assets         ))
$(eval $(call IMG2SP, SET_PALETTE_FW  , $(PALETTE_SPRITES)         ))
#$(eval $(call IMG2SP, CONVERT         , assets/explosiones.png , 4, 8, explosions))











###########################################################################
#
# FONT
#
##########################################################################
$(eval $(call IMG2SP, SET_FOLDER      , src/assets         ))
$(eval $(call IMG2SP, SET_PALETTE_FW  , $(PALETTE_FONT)         ))
$(eval $(call IMG2SP, CONVERT         , assets/font.png , 4, 8, character,,font))








###########################################################################
#
# HUD
#
##########################################################################
$(eval $(call IMG2SP, SET_FOLDER      , src/assets/hud         ))
$(eval $(call IMG2SP, SET_PALETTE_FW  , $(PALETTE_HUD)         ))
$(eval $(call IMG2SP, CONVERT         , assets/hud/playButton.png      , 10, 12, playButton))
$(eval $(call IMG2SP, CONVERT         , assets/hud/pauseButton.png     , 10, 12, pauseButton))
$(eval $(call IMG2SP, CONVERT         , assets/hud/emptyPowerUp.png    , 10, 12, emptyPowerUp))
$(eval $(call IMG2SP, CONVERT         , assets/hud/speaker.png         , 14, 16, speaker))
$(eval $(call IMG2SP, CONVERT         , assets/hud/bossPlaceholderTop.png , 40, 6, bossPlaceholderTop))
$(eval $(call IMG2SP, CONVERT         , assets/hud/bossPlaceholderBot.png , 40, 6, bossPlaceholderBot))
$(eval $(call IMG2SP, CONVERT         , assets/hud/statusBar.png       , 22, 7 , statusBar))
$(eval $(call IMG2SP, CONVERT         , assets/faceApolo.png , 10, 10, faceapolo))
$(eval $(call IMG2SP, CONVERT         , assets/hud/amstrad.png     , 38,6, hudAmstrad))











###########################################################################
#
# TILESETS
#
##########################################################################
$(eval $(call IMG2SP, SET_FOLDER      , src/assets         ))
$(eval $(call IMG2SP, SET_PALETTE_FW  , $(PALETTE_TILESET)         ))
$(eval $(call IMG2SP, SET_IMG_FORMAT  , zgtiles            ))
$(eval $(call IMG2SP, CONVERT		  , assets/tilesets/tileset.png,  8,  8, tile, , tiles))



###########################################################################
#
# TITLE
#
##########################################################################
$(eval $(call IMG2SP, SET_OUTPUT      , bin                  )) 
$(eval $(call IMG2SP, SET_FOLDER      , bins         ))
$(eval $(call IMG2SP, SET_PALETTE_FW  , $(PALETTE_TITLE)         ))
$(eval $(call IMG2SP, SET_IMG_FORMAT  , screen            ))
$(eval $(call IMG2SP, CONVERT		  , assets/title.png,  160,  200, titleScreen))

