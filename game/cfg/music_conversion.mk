##-----------------------------LICENSE NOTICE------------------------------------
##  This file is part of CPCtelera: An Amstrad CPC Game Engine 
##  Copyright (C) 2018 ronaldo / Fremos / Cheesetea / ByteRealms (@FranGallegoBR)
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU Lesser General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU Lesser General Public License for more details.
##
##  You should have received a copy of the GNU Lesser General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.
##------------------------------------------------------------------------------
############################################################################
##                        CPCTELERA ENGINE                                ##
##                 Automatic image conversion file                        ##
##------------------------------------------------------------------------##
## This file is intended for users to automate music conversion from      ##
## original files (like Arkos Tracker .aks) into data arrays.             ##
############################################################################

##
## NEW MACROS
##

# Default values
#$(eval $(call AKS2DATA, SET_FOLDER   , src/ ))
#$(eval $(call AKS2DATA, SET_OUTPUTS  , h s  )) { bin, h, hs, s }
#$(eval $(call AKS2DATA, SET_SFXONLY  , no   )) { yes, no       }
#$(eval $(call AKS2DATA, SET_EXTRAPAR ,      )) 
# Conversion
#$(eval $(call AKS2DATA, CONVERT      , music.aks , array , mem_address ))



##
## OLD MACROS (For compatibility)
##

## AUTOMATED MUSIC CONVERSION EXAMPLE (Uncomment EVAL line to use)

## Convert music/song.aks to src/music/song.s and src/music/song.h
##		This file contains a music created with Arkos Tracker. This macro 
## will convert the music into a data array called g_mysong that will be
## placed at the 0x42A0 memory address in an absolue way.
##

#$(eval $(call AKS2C,music/song.aks,g_mysong,src/music/,0x42A0))

############################################################################
##              DETAILED INSTRUCTIONS AND PARAMETERS                      ##
##------------------------------------------------------------------------##
##                                                                        ##
## Macro used for conversion is AKS2C, which has up to 5 parameters:      ##
##  (1): AKS file to be converted to data array                           ##
##  (2): C identifier for the generated data array (will have underscore  ##
##       in front in ASM)                                                 ##
##  (3): Output folder for .s and .h files generated (Default same folder)##
##  (4): Memory address where music data will be loaded                   ##
##  (5): Aditional options (you can use this to pass aditional modifiers  ##
##       to cpct_aks2c)                                                   ##
##                                                                        ##
## Macro is used in this way (one line for each image to be converted):   ##
##  $(eval $(call AKS2C,(1),(2),(3),(4),(5))                              ##
##                                                                        ##
## Important:                                                             ##
##  * Do NOT separate macro parameters with spaces, blanks or other chars.##
##    ANY character you put into a macro parameter will be passed to the  ##
##    macro. Therefore ...,src/music,... will represent "src/music"       ##
##    folder, whereas ...,  src/music,... means "  src/sprites" folder.   ##
##  * You can omit parameters by leaving them empty.                      ##
##  * Parameter  (5) (Aditional options) is  optional and  generally not  ##
##    required.                                                           ##
############################################################################

AT2UTILS=arkostraker2utils
INTERMEDIATE_FOLDER=obj/__aks2
INTERMEDIATE_FOLDER_FOLDER=obj/__aks2/.folder
MUSICFILES=

$(INTERMEDIATE_FOLDER_FOLDER):
	mkdir -p $(INTERMEDIATE_FOLDER)
	touch $(INTERMEDIATE_FOLDER_FOLDER)

define AKS2_SET_PLAYER
$(eval PLAYER           = $(1))
$(eval PLAYER_UPPERCASE = $(shell echo $(PLAYER) | tr a-z A-Z))
$(eval PLAYER_ASM       = Player$(PLAYER).asm)
$(eval PLAYER_SRC       = $(AT2UTILS)/players/Player$(PLAYER).asm)
$(eval PLAYER_DST       = $(INTERMEDIATE_FOLDER)/Player$(PLAYER).asm)
$(eval PLAYER_SFX_SRC   = $(AT2UTILS)/players/Player$(PLAYER)_SoundEffects.asm)
$(eval PLAYER_SFX_DST   = $(INTERMEDIATE_FOLDER)/Player$(PLAYER)_SoundEffects.asm)
$(eval PLAYER_HEAD      = $(INTERMEDIATE_FOLDER)/PlayerHead.asm)
$(eval PLAYER_OUT       = $(INTERMEDIATE_FOLDER)/Player$(PLAYER))
$(eval PLAYER_OUT_BIN   = $(PLAYER_OUT).bin)
$(eval PLAYER_OUT_SYM   = $(PLAYER_OUT).sym)
$(eval PLAYER_S         = $(OUTFOLDER)/Player$(PLAYER).s)
endef






define AKS2_GEN_PLAYER



$(PLAYER_DST): $(PLAYER_SRC) $(INTERMEDIATE_FOLDER_FOLDER)
	cp $(PLAYER_SRC) $(PLAYER_DST)

$(PLAYER_SFX_DST): $(PLAYER_SFX_SRC)
	cp $(PLAYER_SFX_SRC) $(PLAYER_SFX_DST)

$(PLAYER_HEAD): $(PLAYER_DST) $(PLAYER_SFX_DST)
	touch $(PLAYER_HEAD)
	echo ""                                                          > $(PLAYER_HEAD)
	echo 'PLY_$(PLAYER_UPPERCASE)_HARDWARE_CPC = 1'                  >> $(PLAYER_HEAD)
	echo 'PLY_$(PLAYER_UPPERCASE)_MANAGE_SOUND_EFFECTS = 1'          >> $(PLAYER_HEAD)
	$(foreach CONF, $(SONG_PLAYER_CONFIGS), echo 'include "$(CONF)"' >> $(PLAYER_HEAD) ;)
	$(foreach CONF, $(SFX_PLAYER_CONFIGS), echo 'include "$(CONF)"'  >> $(PLAYER_HEAD) ;)
	echo 'include "$(PLAYER_ASM)"'                                   >> $(PLAYER_HEAD)

$(PLAYER_OUT_BIN): $(PLAYER_HEAD) $(PLAYER_SFX_DST)
	$(AT2UTILS)/rasm $(PLAYER_HEAD) -o $(PLAYER_OUT) -s -sl -sq

$(PLAYER_S): $(PLAYER_OUT_BIN)
	$(AT2UTILS)/Disark $(PLAYER_OUT_BIN) $(PLAYER_S) --symbolFile $(PLAYER_OUT_SYM) --sourceProfile sdcc

$(eval MUSICFILES  = $(MUSICFILES) $(PLAYER_S))
$(eval OBJS2CLEAN := $(OBJS2CLEAN) $(PLAYER_S))



endef



define AKS2_SET_FOLDER
OUTFOLDER = $(1)
endef







define AKS2_CONVERT_SONG

$(eval SONG_AKS            = $(1))
$(eval SONG                = $(2))
$(eval SUBSONG             = $(3))
$(eval SONG_PLAYER_CONFIG  = $(SONG)_playerconfig.asm)
$(eval SONG_ASM            = $(INTERMEDIATE_FOLDER)/$(SONG).asm)
$(eval SONG_OUT            = $(INTERMEDIATE_FOLDER)/$(SONG))
$(eval SONG_OUT_BIN        = $(SONG_OUT).bin)
$(eval SONG_OUT_SYM        = $(SONG_OUT).sym)
$(eval SONG_S              = $(OUTFOLDER)/$(SONG).s)

$(eval SUBSONGS = $(foreach SUBSONG, $(3),s$(SUBSONG)p1,))
$(eval SUBSONGS = $(shell echo $(SUBSONGS) | tr -d ' '))

$(SONG_ASM):  $(SONG_AKS) $(INTERMEDIATE_FOLDER_FOLDER)
	$(AT2UTILS)/SongTo$(PLAYER) $(SONG_AKS) $(SONG_ASM) -sp $(SUBSONGS) --exportPlayerConfig --labelPrefix $(SONG)_

$(SONG_OUT_BIN): $(SONG_ASM)
	$(AT2UTILS)/rasm $(SONG_ASM) -o $(SONG_OUT) -s -sl -sq

$(SONG_S): $(SONG_OUT_BIN)
	$(AT2UTILS)/Disark $(SONG_OUT_BIN) $(SONG_S) --symbolFile $(SONG_OUT_SYM) --sourceProfile sdcc

$(eval SONG_PLAYER_CONFIGS  = $(SONG_PLAYER_CONFIGS) $(SONG_PLAYER_CONFIG))
$(eval MUSICFILES           = $(MUSICFILES) $(SONG_S))
$(eval OBJS2CLEAN          := $(OBJS2CLEAN) $(SONG_S))

endef




define AKS2_CONVERT_SFX

$(eval SFX_AKS            = $(1))
$(eval SFX                = $(2))
$(eval SFX_PLAYER_CONFIG  = $(SFX)_playerconfig.asm)
$(eval SFX_ASM            = $(INTERMEDIATE_FOLDER)/$(SFX).asm)
$(eval SFX_OUT            = $(INTERMEDIATE_FOLDER)/$(SFX))
$(eval SFX_OUT_BIN        = $(SFX_OUT).bin)
$(eval SFX_OUT_SYM        = $(SFX_OUT).sym)
$(eval SFX_S              = $(OUTFOLDER)/$(SFX).s)

$(SFX_ASM): $(SFX_AKS) $(INTERMEDIATE_FOLDER_FOLDER)
	$(AT2UTILS)/SongToSoundEffects $(SFX_AKS) $(SFX_ASM) --exportPlayerConfig --labelPrefix $(SFX)_

$(SFX_OUT_BIN): $(SFX_ASM)
	$(AT2UTILS)/rasm $(SFX_ASM) -o $(SFX_OUT) -s -sl -sq

$(SFX_S): $(SFX_OUT_BIN)
	$(AT2UTILS)/Disark $(SFX_OUT_BIN) $(SFX_S) --symbolFile $(SFX_OUT_SYM) --sourceProfile sdcc

$(eval SFX_PLAYER_CONFIGS  = $(SFX_PLAYER_CONFIGS) $(SFX_PLAYER_CONFIG))
$(eval MUSICFILES          = $(MUSICFILES) $(SFX_S))
$(eval OBJS2CLEAN         := $(OBJS2CLEAN) $(SFX_S))

endef


$(eval $(call AKS2_SET_FOLDER, src/assets/sound))
$(eval $(call AKS2_SET_PLAYER,Akm))
$(eval $(call AKS2_CONVERT_SONG, assets/sound/music.aks,music_main_theme, 1))
$(eval $(call AKS2_CONVERT_SONG, assets/sound/music.aks,music_boss, 2))
$(eval $(call AKS2_CONVERT_SONG, assets/sound/music.aks,music_win, 3))
$(eval $(call AKS2_CONVERT_SONG, assets/sound/music.aks,music_platforms, 4))
$(eval $(call AKS2_CONVERT_SONG, assets/sound/music.aks,music_gameover, 6))
$(eval $(call AKS2_CONVERT_SONG, assets/sound/music.aks,music_sewers, 7))
#$(eval $(call AKS2_CONVERT_SONG, assets/sound/music.aks,music_main_theme_extended, 8))
$(eval $(call AKS2_CONVERT_SONG, assets/sound/music.aks,music_empty, 9))
$(eval $(call AKS2_CONVERT_SONG, assets/sound/music.aks,music_slot_machine, 10))
$(eval $(call AKS2_CONVERT_SONG, assets/sound/music.aks,music_boss_door, 11))
$(eval $(call AKS2_CONVERT_SFX, assets/sound/soundEffects.aks,sfx))
$(eval $(call AKS2_GEN_PLAYER))

