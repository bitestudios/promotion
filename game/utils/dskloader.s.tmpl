.include \"cpctelera.h.s\"

.area _DATA
.area _CODE

firmware_amsdosReserved = 0xBE7D

;; Firmware routines
firmware_casInOpen = 0xBC77
firmware_casInClose = 0xBC7A
firmware_casInDirect = 0xBC83
firmware_MCStartProgram = 0xBD16
firmware_KLRomWalk	= 0xBCCB
firmware_SCRSetInk = 0xBC32
firmware_SCRSetBorder = 0xBC38
firmware_SCRSetMode = 0xBC0E

BIN_COMPRESSED_SIZE = ${BIN_COMPRESSED_SIZE:? Error no binary compressed size specified}

BIN_START = ${BIN_START:? Error, no binary start specified}
BIN_SIZE = ${BIN_SIZE:? Error no binary size specified}
BIN_RUN = ${BIN_RUN:? Error no binary run address specified}

palette: .db ${PALETTE:? Error, no palette specified}

titlestr:
.asciz  "\"${TITLE_STR:? Error no title scr filename specified}\""
titlestrEnd = .-1
gamestr:
.asciz  "\"${GAME_STR:? Error no game filename specified}\""
gamestrEnd = .-1

_main::
	
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;
	;; SETUP AMSDOS
	;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	ld hl,(firmware_amsdosReserved)    ;;\\
	ld a,(hl)                          ;;- Store the drive number the loader was run from
	ld (drive+1),a                     ;;/
	
	ld c, #0xff							;; disable all roms
	ld hl, #start						;; execution address for program
	call firmware_MCStartProgram		;; start it
	
	start:
	call firmware_KLRomWalk				;; enable all roms 
	
	drive:
	ld a, #0                          ;;/-When AMSDOS is enabled, the drive reverts back to drive 0
	ld hl, (firmware_amsdosReserved)  ;;- We need restore the drive number to the loader was run
	ld (hl),a		                  ;;\_ from
	
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;
	;; SETS THE MODE
	;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	xor a
	call firmware_SCRSetMode
	

	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;
	;; SETS THE PALETTE
	;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	ld hl, #palette
	xor a
	
	setNextInk:
		push hl
		push af

		ld b, (hl)
		ld c, b

		call firmware_SCRSetInk
		
		pop af
		pop hl

		inc hl
		inc a
		cp a, #16
		jr nz, #setNextInk
	
	ld b, (hl)
	ld c, b
	call firmware_SCRSetBorder


	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;
	;; Load title image
	;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	ld b, #titlestrEnd-titlestr
	ld hl, #titlestr
	ld de, #0xC000
	call loadFile
	
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;
	;; Load compressed game
	;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	ld b, #gamestrEnd-gamestr
	ld hl, #gamestr
	ld de, #LOADER_END
	call loadFile

	
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;
	;; Disables firmware
	;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	di                  ;; [1] Disable interrupts
   	ld   hl, (0x38+1)   ;; [5] Obtain pointer to the present interrupt handler
   	ex   de, hl         ;; [1] DE = HL (DE saves present pointer to previous interrupt handler)

   	im    1             ;; [2] Set Interrupt Mode 1 (CPU will jump to 0x38 when a interrupt occurs)
   	ld   hl, #0xC9FB    ;; [3] FB C9 (take into account little endian) => EI : RET

   	ld (0x38), hl       ;; [5] Setup new "null interrupt handler" and enable interrupts again
   	ei                  ;; [1] Reenable interrupts
   	ex   de, hl         ;; [1] HL = Pointer to previous interrupt handler (return value)
	
	
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;
	;; Decrunch game
	;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	ld hl, #LOADER_END+BIN_COMPRESSED_SIZE-1
	ld de, #BIN_START+BIN_SIZE-1
	call cpct_zx7b_decrunch_s_asm
	
	jp BIN_RUN					;; - Jumps to the start of the game 


;;;;ZX7B CODE
cpct_zx7b_decrunch_s_asm:
        ld      bc, #0x8000   ;; [3] marker bit at MSB of B
        ld      a, b          ;; [1] move marker bit to A
copyby: inc     c             ;; [1] to keep BC value after LDD
        ldd                   ;; [5] copy literal byte
mainlo: call    getbit        ;; [5+(5/11)] read next bit
        jr      nc, copyby    ;; [2/3] next bit indicates either literal or sequence

; determine number of bits used for length (Elias gamma coding)
        push    de            ;; [4] store destination on stack
        ld      d, c          ;; [1] D= 0, assume BC=$0000 here (or $8000)
lenval: call    nc, getbit    ;; [5+(5/11)] don't call first time (to save bytes)
        rl      c             ;; [2] 
        rl      b             ;; [2] insert bit on BC
        call    getbit        ;; [5+(5/11)] more bits?
        jr      nc, lenval    ;; [2/3] repeat, final length on BC

; check escape sequence
        inc     c             ;; [1] detect escape sequence
        jr      z, exitdz     ;; [2/3] end of algorithm

; determine offset
        ld      e, (hl)       ;; [2] load offset flag (1 bit) + offset value (7 bits)
        dec     hl            ;; [2]
        sll__e                ;; [2]
        jr      nc, offend    ;; [2/3] if offset flag is set, load 4 extra bits
        ld      d, #0x10      ;; [2] bit marker to load 4 bits
nexbit: call    getbit        ;; [5+(5/11)]
        rl      d             ;; [2] insert next bit into D
        jr      nc, nexbit    ;; [2/3] repeat 4 times, until bit marker is out
        inc     d             ;; [1] add 128 to DE
        srl     d             ;; [2] retrieve fourth bit from D
offend: rr      e             ;; [2] insert fourth bit into E
        ex      (sp), hl      ;; [6] store source, restore destination
        ex      de, hl        ;; [1] destination from HL to DE
        adc     hl, de        ;; [3] HL = destination + offset + 1
        lddr                  ;; [5xsize (BC)]
exitdz: pop     hl            ;; [3] restore source address (compressed data)
        jr      nc, mainlo    ;; [2/3] end of loop. exit with getbit instead ret (-1 byte)
getbit: add     a, a          ;; [1] check next bit
        ret     nz            ;; [2/4] no more bits left?
        ld      a, (hl)       ;; [2] load another group of 8 bits
        dec     hl            ;; [2]
        adc     a, a          ;; [1]
        ret                   ;; [3]


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Loads a file from disc to RAM
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; HL= Pointer to the file name string
;; B = Size of the file name
;; DE= Address where allocate the file
;;
loadFile::
	push de
	call firmware_casInOpen
	pop hl
	call firmware_casInDirect
	call firmware_casInClose
	ret


LOADER_END = .

