# Promotion
## Amstrad CPC Game for the RetroDev2020

<p>In this action platform game the goal is to become the new boss!</p>

<img src="game/assets/title.png" alt="Title screen" width="320" height="200">

<h2> Authors </h2>
 <ul>
  <li>Antonio Maciá Lillo <br><a href="mailto:tonetml23@gmail.com">tonetml23@gmail.com</a></li>
  <li>Salvador Moreno Pardines <br><a href="mailto:Proyectosardha@gmail.com">Proyectosardha@gmail.com</a></li>
  <li>Cayetano Manchón Pernis <br><a href="mailto:tano@tano.xyz">tano@tano.xyz</a></li>
</ul> 

## Compilation
This game uses the [CPCtelera](https://github.com/lronaldo/cpctelera) engine, so in order to compile this project, you should have installed CPCtelera in the first place.

To compile this game just execute the command **make** under the folder game.

## Special thanks
We want to give special thanks to our friends Carlos and Angel, who tested our game, and gave us good ideas to improve it.

We also want to thank [Ironaldo](https://github.com/lronaldo), for developing the CPCtelera engine, which made our task of making a game for the amstrad much more easy. Also, for his labor as a teacher, and the tons of informative videos on how to develop games, which teached us what we need for this project.

## License
<p>The MIT License (MIT)</p>
<p>Copyright © 2020</p>
<ul>
  <li>Antonio Maciá Lillo <br><a href="mailto:tonetml23@gmail.com">tonetml23@gmail.com</a></li>
  <li>Salvador Moreno Pardines <br><a href="mailto:proyectosardha@gmail.com">proyectosardha@gmail.com</a></li>
  <li>Cayetano Manchón Pernis <br><a href="mailto:tano@tano.xyz">tano@tano.xyz</a></li>
</ul> 

<p>Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:</p>

<p>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.</p>

<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</p>
