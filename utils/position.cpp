#include <iostream>
#include <string>

using namespace std;

/*
 * CALCULATES THE TILES MEMORY POSITION USING THE CHARACTER X AND Y, 
 * AND THE INITIAL POSITION OF THE TILEMAP
 */
int main() {
    int playerx, playery, memoryStart;
    string xStr, yStr, memoryStr;
    
    cout << "Insert player x in hexadecimal: " << endl;
    cin >> xStr;
    playerx = std::stoul(xStr, nullptr, 16);
    
    cout << "Insert player y in hexadecimal: " << endl;
    cin >> yStr;
    playery = std::stoul(yStr, nullptr, 16);
    
    cout << "Insert the memory start position in hexadecimal: " << endl;
    cin >> memoryStr;
    memoryStart = std::stoul(memoryStr, nullptr, 16);
    
    int tilex = playerx / 4;
    int tiley = playery / 4;
    
    cout << "The position in the tilemap is x: " << tilex << " y: " << tiley << endl;
    
    int memoryPosition = memoryStart + tiley * 40 + tilex;
    
    char hex_string[4];
    sprintf(hex_string, "%X", memoryPosition); //convert number to hex
    cout << "The tile 0 is at 0x" << hex_string << endl;
    
    memoryPosition += 40;
    sprintf(hex_string, "%X", memoryPosition); //convert number to hex
    cout << "The tile 1 is at 0x" << hex_string << endl;
    
    memoryPosition += 40;
    sprintf(hex_string, "%X", memoryPosition); //convert number to hex
    cout << "The tile 2 is at 0x" << hex_string << endl;
    
    memoryPosition += 40;
    sprintf(hex_string, "%X", memoryPosition); //convert number to hex
    cout << "The tile 3 is at 0x" << hex_string << endl;

    memoryPosition += 40;
    sprintf(hex_string, "%X", memoryPosition); //convert number to hex
    cout << "The tile 4 is at 0x" << hex_string << endl;
}
