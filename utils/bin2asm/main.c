#include <stdlib.h>
#include <stdio.h>

size_t fileToData(char const* path, char *buf){
    FILE  *file;
    size_t fileSize;
    long   off_end;
 
    /* Open the file */
    file = fopen(path, "r");
    if( !file ) {
        return 0;
    }
 
    /* Seek to the end of the file */
    fseek(file, 0L, SEEK_END);
 
    /* Byte offset to the end of the file (size) */
	off_end = ftell(file);
    
	fileSize = (size_t)off_end;
 
    /* Rewind file pointer to start of file */
    rewind(file);
 
    /* Slurp file into buffer */
    fread(buf, 1, fileSize, file);
 
    /* Close the file */
    fclose(file);
 
    return fileSize;
}

int main(int argc, char** argv){
	char data[500000];
	if(argc < 4){
		return -1;
	}
	size_t fsize = fileToData(argv[1], data);
	printf(".area __%s (ABS)\n", argv[2]);
	printf(".org %s\n", argv[3]);
	printf(";; Size: %lu\n", fsize);
	for(size_t i = 0; i<fsize;){
		printf(".db ");
		for(size_t j = 0; j < 10 && i < fsize; ++j, ++i){
			printf("%d", data[i]);
			if(i < fsize-1 && j < 10-1){
				printf(" , ");
			}
		}
		printf("\n");
	}
}


