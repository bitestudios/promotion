#include "3rdParty/parson.h"
#include "3rdParty/zx7b.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define MAX_TILEMAP_SIZE 100000
#define MAX_MAP_NAME_SIZE 50

#define MAX_TILEMAPS 200
#define MAX_MAPS 200
#define MAX_ENTITIES 600

#define TILE_INDEX_OFFSET 1

#define TILE_SIZE 8
#define TOP_TILEMAP_X_OFFSET 0
#define TOP_TILEMAP_Y_OFFSET 0
#define TOP_TILEMAP_WIDTH 20
#define TOP_TILEMAP_HEIGHT 2

#define MARGIN_LEFT_TILEMAP_X_OFFSET 0
#define MARGIN_LEFT_TILEMAP_Y_OFFSET 2
#define MARGIN_RIGHT_TILEMAP_X_OFFSET 18
#define MARGIN_RIGHT_TILEMAP_Y_OFFSET 2
#define MARGIN_TILEMAP_WIDTH 2
#define MARGIN_TILEMAP_HEIGHT 15

#define CENTER_TILEMAP_X_OFFSET 2
#define CENTER_TILEMAP_Y_OFFSET 2
#define CENTER_TILEMAP_WIDTH 16
#define CENTER_TILEMAP_HEIGHT 15

typedef struct tilemap_t{
	size_t width;
	size_t height;
	unsigned char data[MAX_TILEMAP_SIZE];
	unsigned char data_z[MAX_TILEMAP_SIZE];
	size_t size_z;
}tilemap_t;

typedef struct vector_tilemap_t{
	size_t count;
	tilemap_t tilemaps[MAX_TILEMAPS];
}vector_tilemap_t;

typedef struct entity_t{
	int worldX, worldY;
	int x, y;
	char template[50];
}entity_t;

typedef struct vector_entity_t{
	size_t count;
	entity_t entities[MAX_ENTITIES];
}vector_entity_t;

typedef struct map_t{
	char name[MAX_MAP_NAME_SIZE];
	int x, y, width, height;
	tilemap_t* tilemapTop;
	tilemap_t* tilemapMarginLeft;
	tilemap_t* tilemapMarginRight;
	tilemap_t* tilemapCenter;
	bool continueMusic;
	bool continueTimer;
	bool isCheckpoint;
	int timer;
	char palette[50];
	char music[50];
	char floor[50];
	char bossName[50];
	entity_t* player;
	entity_t* boss;
	vector_entity_t entities;
}map_t;

typedef struct vector_map_t{
	size_t count;
	map_t maps[MAX_MAPS];
}vector_map_t;

tilemap_t globalTilemap = {
	.width = 0,
	.height = 0,
	.data = {}
};

vector_tilemap_t centerTilemaps = {
	.count = 0,
	.tilemaps = {}
};

vector_tilemap_t topTilemaps = {
	.count = 0,
	.tilemaps = {}
};

vector_tilemap_t marginTilemaps = {
	.count = 0,
	.tilemaps = {}
};

vector_map_t maps = {
	.count = 0,
	.maps = {}
};

vector_entity_t players = {
	.count = 0,
	.entities = {}
};

vector_entity_t bosses = {
	.count = 0,
	.entities = {}
};

vector_entity_t entities = {
	.count = 0,
	.entities = {}
};

entity_t dummyEntity = {
	.template = "dummyEntity",
	.worldX = 0,
	.worldY = 0,
	.x = 0,
	.y = 0
};


FILE* outfile;

FILE* setOutfileFromFilename(const char* filename){
	outfile = fopen(filename, "w");
	return outfile;
}

void closeOutfile(){
	fclose(outfile);
}

int tilemap_getValue(tilemap_t* tilemap, size_t x, size_t y){
	return tilemap->data[y*tilemap->width+x];
}


void tilemap_setValue(tilemap_t* tilemap, size_t x, size_t y, int value){
	tilemap->data[y*tilemap->width+x] = value;
}


void tilemap_initFromWindow(
		tilemap_t* srcTilemap,
		tilemap_t* dstTilemap,
		size_t x,
		size_t y,
		size_t width,
		size_t height){
	
	dstTilemap->width = width;
	dstTilemap->height = height;
	for(size_t i = 0; i < height; ++i){
		for(size_t j = 0; j < width; ++j){
			tilemap_setValue(dstTilemap, j, i, tilemap_getValue(srcTilemap, x+j, y+i));
		}
	}
}


void copyReverse(const unsigned char* src, unsigned char* dst, const size_t size){
	for(size_t i = 0; i < size; ++i){
		dst[i] = src[size-i-1];
	}
}


void tilemap_compress(tilemap_t* tilemap){
	size_t inputSize = sizeof(tilemap->data[0])*tilemap->height*tilemap->width;
	size_t outputSize;
	unsigned char inputData[inputSize];
	unsigned char* outputData;

	copyReverse(tilemap->data, inputData, inputSize);
	outputData= compress(optimize(inputData, inputSize), inputData, inputSize, &outputSize);
	copyReverse(outputData, tilemap->data_z, outputSize);
	free(outputData);

	tilemap->size_z = outputSize;

}

void tilemap_printData(tilemap_t* tilemap){
	printf("\nTilemap: %p\n", tilemap);
	printf("Height: %lu\nWidth: %lu\n",tilemap->height, tilemap->width);

//	for(size_t i = 0; i < tilemap->height; ++i){
//		for(size_t j = 0; j < tilemap->width; ++j){
//			printf("%d\t", tilemap_getValue(tilemap, j, i));
//		}
//		printf("\n");
//	}
	
	printf("\nCompressed data:\n");
	printf("Compressed size: %lu bytes\n", tilemap->size_z);
	printf("Saved: %lu bytes\n", tilemap->height*tilemap->width-tilemap->size_z);
	printf("Data:\n");
	for(size_t i = 0; i < tilemap->size_z; ++i){
		printf("%d , ", tilemap->data_z[i]);
	}
	printf("\n");
}


void tilemap_fprintASM(tilemap_t* tilemap){
	fprintf(outfile, "\ntilemap_%p::\n", tilemap);
	fprintf(outfile, "\n.db ");
	for(size_t i = 0; i < tilemap->size_z; ++i){
		fprintf(outfile, "%d", tilemap->data_z[i]);
		if(i < tilemap->size_z-1){
			fprintf(outfile, " , ");
		}
	}
	fprintf(outfile, "\n");
	
	fprintf(outfile, "\ntilemap_%p_end==.-1\n", tilemap);
}


bool tilemap_equal(const tilemap_t* tilemap1, const tilemap_t* tilemap2){
	if(tilemap1->height != tilemap2->height){
		return false;
	}
	if(tilemap1->width != tilemap2->width){
		return false;
	}
	
	size_t dataLenght = sizeof(tilemap1->data[0])*tilemap1->width*tilemap2->height;
	return (memcmp(tilemap1->data, tilemap2->data, dataLenght) == 0);
}


typedef void(*vector_tilemap_foreachHandler)(tilemap_t*);
void vector_tilemap_foreach(vector_tilemap_t* vtilemap, vector_tilemap_foreachHandler handler){
	for(size_t i = 0; i < vtilemap->count; ++i){
		handler(&(vtilemap->tilemaps[i]));
	}
}


tilemap_t* vector_tilemap_findOrAdd(vector_tilemap_t* vtilemap, const tilemap_t* tilemap){
	for(size_t i = 0; i < vtilemap->count; ++i){
		tilemap_t* tilemapPAux = &(vtilemap->tilemaps[i]);
		if(tilemap_equal(tilemapPAux, tilemap)){
			printf("\nTILEMAP %p EQUAL TO %p\n", tilemap, tilemapPAux);
			printf("IGNORING %p\n\n", tilemap);
			return tilemapPAux;
		}
	}
	vtilemap->tilemaps[vtilemap->count++] = *tilemap;
	return (&vtilemap->tilemaps[vtilemap->count-1]);
}










/**********************************************
 *
 *
 *
 *
 *
 * ENTITIES
 *
 *
 *
 *
 *
 *********************************************/

typedef struct window_t{
	int x,y, width, height;
} window_t;

bool entity_isInWindow(entity_t* entity, window_t* w){
	if(entity->worldX < w->x){
		return false;
	}
	if(entity->worldY < w->y){
		return false;
	}
	if(entity->worldX >= w->x+w->width){
		return false;
	}
	if(entity->worldY >= w->y+w->height){
		return false;
	}
	return true;
}


void entity_setXYFromWindow(entity_t* entity, window_t* w){
	entity->x = (entity->worldX - w->x - (CENTER_TILEMAP_X_OFFSET*TILE_SIZE))/2;
	entity->y = (entity->worldY - w->y) - (CENTER_TILEMAP_Y_OFFSET*TILE_SIZE);
}


void entity_print(entity_t* entity){
	if(!entity){
		printf("\n NULL!!\n");
		return;
	}
	printf("\n\nName: %s\n", entity->template);
	printf("World X: %d\n", entity->worldX);
	printf("World Y: %d\n", entity->worldY);
	printf("X: %d\n", entity->x);
	printf("Y: %d\n", entity->y);
}


void entity_fprintASM(entity_t* entity){
	fprintf(outfile, "\n;;TEMPLATE\n");
	fprintf(outfile, ".globl %s\n", entity->template);
	fprintf(outfile, ".dw %s\n", entity->template);
	fprintf(outfile, ";;X\n");
	fprintf(outfile, ".db %d\n", entity->x);
	fprintf(outfile, ";;Y\n");
	fprintf(outfile, ".db %d\n", entity->y);
}

void vector_entity_add(vector_entity_t* ventity, entity_t* entity){
	ventity->entities[ventity->count++] = *entity;
}

typedef void(*foreachHandler)();

void vector_entity_foreach(vector_entity_t* ventity, foreachHandler handler){
	for(size_t i = 0; i < ventity->count; ++i){
		handler(&(ventity->entities[i]));
	}
}

void vector_entity_foreachInWindow(
		vector_entity_t* ventity,
		window_t* window,
		foreachHandler handler,
		void* params
){
	for(size_t i = 0; i < ventity->count; ++i){
		entity_t* entity = &(ventity->entities[i]);
		if(entity_isInWindow(entity, window)){
			handler(&(ventity->entities[i]), params);
		}
	}
}






/**********************************************
 *
 *
 *
 *
 *
 * MAPS
 *
 *
 *
 *
 *
 *********************************************/


void map_setProperties(map_t* map, JSON_Array *properties){
	map->timer = 0;
	strcpy(map->palette, "");
	strcpy(map->music, "");
	strcpy(map->bossName, "");
	strcpy(map->floor, "");
	map->isCheckpoint = false;
	map->continueMusic = false;
	map->continueTimer = false;
	for(size_t i =0; i < json_array_get_count(properties); ++i){
		JSON_Object* property = json_array_get_object(properties, i);
		const char* propertyName = json_object_get_string(property, "name");

		if(strcmp(propertyName, "timer")==0){
			map->timer = atoi(json_object_get_string(property, "value"));
			continue;
		}

		if(strcmp(propertyName, "palette")==0){
			strcpy(map->palette, json_object_get_string(property, "value"));
			continue;
		}

		if(strcmp(propertyName, "music")==0){
			strcpy(map->music, json_object_get_string(property, "value"));
			continue;
		}

		if(strcmp(propertyName, "bossName")==0){
			strcpy(map->bossName, json_object_get_string(property, "value"));
			continue;
		}

		if(strcmp(propertyName, "floor")==0){
			strcpy(map->floor, json_object_get_string(property, "value"));
			continue;
		}

		if(strcmp(propertyName, "isCheckpoint")==0){
			map->isCheckpoint = true;
			continue;
		}

		if(strcmp(propertyName, "continueMusic")==0){
			map->continueMusic = true;
			continue;
		}

		if(strcmp(propertyName, "continueTimer")==0){
			map->continueTimer = true;
			continue;
		}
	}

}




void map_print(map_t* map){
	printf("\n\n\n===================================================\n");
	printf("Name: %s\n", map->name);
	printf("Height: %d\n", map->height);
	printf("Width: %d\n", map->width);
	printf("X: %d\n", map->x);
	printf("Y: %d\n\n", map->y);
	printf("Is checkoint: %s\n\n", map->isCheckpoint ? "True" : "False");
	printf("Palette: _%s\n\n", map->palette);
	printf("Timer: %d\n\n", map->timer);
	printf("Continue music: %s\n\n", map->continueMusic ? "True" : "False");
	printf("Continue timer: %s\n\n", map->continueTimer ? "True" : "False");
	printf("Music: _%s\n\n", map->music);
	printf("Floor: %s\n\n", map->floor);
	printf("Boss name: %s\n\n", map->bossName);
	
	printf("PLAYER:");
	entity_print(map->player);
	
	printf("\nBOSS:");
	entity_print(map->boss);
	
	printf("\nENTITIES:");
	vector_entity_foreach(&map->entities, entity_print);


	printf("\n\nTOP TILEMAP:");
	if(map->tilemapTop){
		tilemap_printData(map->tilemapTop);
	}else{
		printf("\n NULL!!\n");
	}
	printf("=======================================================\n");
	printf("MARGIN LEFT TILEMAP:");
	if(map->tilemapMarginLeft){
		tilemap_printData(map->tilemapMarginLeft);
	}else{
		printf("\n NULL!!\n");
	}
	printf("=======================================================\n");
	printf("MARGIN RIGHT TILEMAP:");
	if(map->tilemapMarginRight){
		tilemap_printData(map->tilemapMarginRight);
	}else{
		printf("\n NULL!!\n");
	}
	printf("=======================================================\n");
	printf("CENTER TILEMAP:");
	if(map->tilemapCenter){
		tilemap_printData(map->tilemapCenter);
	}else{
		printf("\n NULL!!\n");
	}
	printf("=======================================================\n");
}


void map_fprintHeaderASM(map_t* map){
	fprintf(outfile, "\n.globl %s\n\n", map->name);
}


void map_fprintASM(map_t* map){
	fprintf(outfile, "\n%s::\n\n", map->name);
	fprintf(outfile, ";; TILEMAP CENTER\n");
	fprintf(outfile, ".dw tilemap_%p_end\n", map->tilemapCenter);
	fprintf(outfile, ";; TILEMAP TOP\n");
	fprintf(outfile, ".dw tilemap_%p_end\n", map->tilemapTop);
	fprintf(outfile, ";; TILEMAP LEFT\n");
	fprintf(outfile, ".dw tilemap_%p_end\n", map->tilemapMarginLeft);
	fprintf(outfile, ";; TILEMAP RIGHT\n");
	fprintf(outfile, ".dw tilemap_%p_end\n", map->tilemapMarginRight);
	fprintf(outfile, ";; FLAGS CONTINUE_TIMER(2), CONTINUE_MUSIC(1), IS_CHECKPOINT(0)\n");
	fprintf(
		outfile,
		".db 0b00000%d%d%d\n",
		map->continueTimer,
		map->continueMusic,
		map->isCheckpoint
	);
	fprintf(outfile, ";; PALETTE\n");
	fprintf(outfile, ".globl _%s\n", map->palette);
	fprintf(outfile, ".dw _%s\n", map->palette);
	fprintf(outfile, ";; MUSIC\n");
	if(strcmp(map->music, "")==0){
		fprintf(outfile, ".dw 0x0000\n");
	}else{
		fprintf(outfile, ".globl _%s\n", map->music);
		fprintf(outfile, ".dw _%s\n", map->music);
	}
	fprintf(outfile, ";; FLOOR\n");
	fprintf(outfile, ".globl %s\n", map->floor);
	fprintf(outfile, ".dw %s\n", map->floor);
	fprintf(outfile, ";; BOSS\n");
	fprintf(outfile, ".globl %s\n", map->bossName);
	fprintf(outfile, ".dw %s\n", map->bossName);
	fprintf(outfile, ";; TIMER\n");
	fprintf(outfile, ".dw %d\n", map->timer);
	fprintf(outfile, ";; PLAYER X\n");
	fprintf(outfile, ".db %d\n", map->player ? map->player->x : 0);
	fprintf(outfile, ";; PLAYER Y\n");
	fprintf(outfile, ".db %d\n", map->player ? map->player->y : 0);
	
	fprintf(outfile, "\n\n;; BOSS\n");
	entity_fprintASM(map->boss);

	fprintf(outfile, "\n;; ENTITIES\n");
	vector_entity_foreach(&map->entities, entity_fprintASM);

	fprintf(outfile, "\n;; END\n");
	fprintf(outfile, ".dw 0x0000\n");
}

void vector_map_add(vector_map_t* vmap, map_t* map){
	vmap->maps[vmap->count++] = *map;
}


typedef void(*vector_map_foreachHandler)(map_t*);

void vector_map_foreach(vector_map_t* vmap, vector_map_foreachHandler handler){
	for(size_t i = 0; i < vmap->count; ++i){
		handler(&(vmap->maps[i]));
	}
}
















size_t fileToData(char const* path, char *buf){
    FILE  *file;
    size_t fileSize;
    long   off_end;
 
    /* Open the file */
    file = fopen(path, "r");
    if( !file ) {
        return 0;
    }
 
    /* Seek to the end of the file */
    fseek(file, 0L, SEEK_END);
 
    /* Byte offset to the end of the file (size) */
	off_end = ftell(file);
    
	fileSize = (size_t)off_end;
 
    /* Rewind file pointer to start of file */
    rewind(file);
 
    /* Slurp file into buffer */
    fread(buf, 1, fileSize, file);
 
    /* Close the file */
    fclose(file);
 
    buf[fileSize] = '\0';
    return fileSize;
}


void parseData(char* data){
    JSON_Value *root_value;
    JSON_Array *layers;
	root_value = json_parse_string(data);
	
	if(json_value_get_type(root_value) != JSONObject){
		printf("Error, no valid file\n");
		return;
	}

	layers = json_object_get_array(json_object(root_value), "layers");

	JSON_Object* jsonTilemap = json_array_get_object(layers, 0);

	JSON_Array* jsonTilemapData = json_object_get_array(jsonTilemap, "data");
	
	globalTilemap.height = (size_t)json_object_get_number(jsonTilemap, "height");
	globalTilemap.width  = (size_t)json_object_get_number(jsonTilemap, "width");


	for(size_t i =0; i < json_array_get_count(jsonTilemapData); ++i){
		globalTilemap.data[i] = (char)json_array_get_number(jsonTilemapData, i)-TILE_INDEX_OFFSET;
	}

	JSON_Object* jsonObjectsLayer = json_array_get_object(layers, 1);
	JSON_Array* jsonObjects = json_object_get_array(jsonObjectsLayer, "objects");
	
	printf("Number of objects on tiled: %lu\n\n", json_array_get_count(jsonObjects));
	for(size_t i =0; i < json_array_get_count(jsonObjects); ++i){
		JSON_Object* tiledObject = json_array_get_object(jsonObjects, i);
		const char* typeStr = json_object_get_string(tiledObject, "type");
		if(strcmp(typeStr, "map")==0){
			map_t map;
			strcpy(map.name, json_object_get_string(tiledObject, "name"));
			map.height = json_object_get_number(tiledObject, "height");
			map.width = json_object_get_number(tiledObject, "width");
			map.x = json_object_get_number(tiledObject, "x");
			map.y = json_object_get_number(tiledObject, "y");
			JSON_Array* jsonProperties = json_object_get_array(tiledObject, "properties");
			map_setProperties(&map, jsonProperties);
			vector_map_add(&maps, &map);
			continue;
		}
		if(strcmp(typeStr, "player")==0){
			entity_t entity;
			strcpy(entity.template, json_object_get_string(tiledObject, "name"));
			entity.worldX = json_object_get_number(tiledObject, "x");
			entity.worldY = json_object_get_number(tiledObject, "y");
			vector_entity_add(&players, &entity);
		}
		if(strcmp(typeStr, "boss")==0){
			entity_t entity;
			strcpy(entity.template, json_object_get_string(tiledObject, "name"));
			entity.worldX = json_object_get_number(tiledObject, "x");
			entity.worldY = json_object_get_number(tiledObject, "y");
			vector_entity_add(&bosses, &entity);
		}
		if(strcmp(typeStr, "entity")==0){
			entity_t entity;
			strcpy(entity.template, json_object_get_string(tiledObject, "name"));
			entity.worldX = json_object_get_number(tiledObject, "x");
			entity.worldY = json_object_get_number(tiledObject, "y");
			vector_entity_add(&entities, &entity);
		}
	}
}


void map_setPlayer(entity_t* entity, map_t* map){
	map->player = entity;
}

void map_setBoss(entity_t* entity, map_t* map){
	map->boss = entity;
}


void map_addEntity(entity_t* entity, map_t* map){
	vector_entity_add(&map->entities, entity);
}

void processMap(map_t* map){
	tilemap_t tempTilemap;
	tilemap_initFromWindow(
		&globalTilemap,
		&tempTilemap,
		map->x/TILE_SIZE,
		map->y/TILE_SIZE,
		map->width/TILE_SIZE,
		map->height/TILE_SIZE
	);

	/*
	 * TOP TILEMAP
	 */
	tilemap_t tempTilemapTop;
	tilemap_initFromWindow(
			&tempTilemap,
			&tempTilemapTop,
			TOP_TILEMAP_X_OFFSET,
			TOP_TILEMAP_Y_OFFSET,
			TOP_TILEMAP_WIDTH,
			TOP_TILEMAP_HEIGHT);
	map->tilemapTop = vector_tilemap_findOrAdd(&topTilemaps, &tempTilemapTop);

	/*
	 * MARGIN LEFT TILEMAP
	 */
	tilemap_t tempTilemapMarginLeft;
	tilemap_initFromWindow(
			&tempTilemap,
			&tempTilemapMarginLeft,
			MARGIN_LEFT_TILEMAP_X_OFFSET,
			MARGIN_LEFT_TILEMAP_Y_OFFSET,
			MARGIN_TILEMAP_WIDTH,
			MARGIN_TILEMAP_HEIGHT);
	map->tilemapMarginLeft = vector_tilemap_findOrAdd(&marginTilemaps, &tempTilemapMarginLeft);

	/*
	 * MARGIN RIGHT TILEMAP
	 */
	tilemap_t tempTilemapMarginRight;
	tilemap_initFromWindow(
			&tempTilemap,
			&tempTilemapMarginRight,
			MARGIN_RIGHT_TILEMAP_X_OFFSET,
			MARGIN_RIGHT_TILEMAP_Y_OFFSET,
			MARGIN_TILEMAP_WIDTH,
			MARGIN_TILEMAP_HEIGHT);
	map->tilemapMarginRight = vector_tilemap_findOrAdd(&marginTilemaps, &tempTilemapMarginRight);


	/*
	 * CENTER TILEMAP
	 */
	tilemap_t tempTilemapCenter;
	tilemap_initFromWindow(
			&tempTilemap,
			&tempTilemapCenter,
			CENTER_TILEMAP_X_OFFSET,
			CENTER_TILEMAP_Y_OFFSET,
			CENTER_TILEMAP_WIDTH,
			CENTER_TILEMAP_HEIGHT);
	map->tilemapCenter = vector_tilemap_findOrAdd(&centerTilemaps, &tempTilemapCenter);



	/*
	 *
	 * ADDS THE CORRESPONDING PLAYER
	 *
	 */
	window_t window = {
		.x = map->x,
		.y = map->y,
		.width = map->width,
		.height = map->height
	};
	map->player = NULL;
	vector_entity_foreachInWindow(&players, &window, map_setPlayer, map);
	if(map->player){
		entity_setXYFromWindow(map->player, &window);
	}

	/*
	 *
	 * ADDS THE CORRESPONDING BOSS
	 *
	 */
	map->boss = NULL;
	vector_entity_foreachInWindow(&bosses, &window, map_setBoss, map);
	if(map->boss){
		entity_setXYFromWindow(map->boss, &window);
	}else{
		map->boss = &dummyEntity;
	}

	/*
	 *
	 * ADDS THE CORRESPONDING ENTITIES
	 *
	 */
	map->entities.count = 0;
	vector_entity_foreachInWindow(&entities, &window, entity_setXYFromWindow, &window);
	vector_entity_foreachInWindow(&entities, &window, map_addEntity, map);
}


int genASM(const char* filename){
	char filenameWithExt[50];
	sprintf(filenameWithExt, "%s.s", filename);

	if(!setOutfileFromFilename(filenameWithExt)){
		return 1;
	}

	fprintf(outfile, "\n;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n");
	fprintf(outfile, ";; TILEMAPS\n");
	fprintf(outfile, ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n");
	
	fprintf(outfile, "\n;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n");
	fprintf(outfile, ";; TILEMAPS TOP\n");
	fprintf(outfile, ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n");

	vector_tilemap_foreach(&topTilemaps, tilemap_fprintASM);
	
	fprintf(outfile, "\n;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n");
	fprintf(outfile, ";; TILEMAPS MARGINS\n");
	fprintf(outfile, ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n");
	
	vector_tilemap_foreach(&marginTilemaps, tilemap_fprintASM);
	
	fprintf(outfile, "\n;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n");
	fprintf(outfile, ";; TILEMAPS CENTER\n");
	fprintf(outfile, ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n");
	
	vector_tilemap_foreach(&centerTilemaps, tilemap_fprintASM);
	
	fprintf(outfile, "\n;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n");
	fprintf(outfile, ";; MAPS\n");
	fprintf(outfile, ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n");
	
	vector_map_foreach(&maps, map_fprintASM);
	closeOutfile();

	return 0;
}


int genHeaderASM(const char* filename){
	char filenameWithExt[50];
	sprintf(filenameWithExt, "%s.h.s", filename);

	if(!setOutfileFromFilename(filenameWithExt)){
		return 1;
	}

	vector_map_foreach(&maps, map_fprintHeaderASM);
	closeOutfile();

	return 0;
}


int main(int argc, char** argv){
	char data[500000];
	fileToData(argv[1], data);

	parseData(data);
	
	vector_map_foreach(&maps, processMap);

	vector_tilemap_foreach(&topTilemaps, tilemap_compress);
	vector_tilemap_foreach(&marginTilemaps, tilemap_compress);
	vector_tilemap_foreach(&centerTilemaps, tilemap_compress);
	
	printf("World tilemap data:\n\n");

	tilemap_printData(&globalTilemap);
	
	printf("\n\n\n\n========================================\n");
	printf("MAPS DATA");
	printf("\n========================================\n");

	vector_map_foreach(&maps, map_print);
	
	vector_entity_foreach(&players, entity_print);
	genASM(argv[2]);
	genHeaderASM(argv[2]);
}

