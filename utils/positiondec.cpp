#include <iostream>
#include <string>

using namespace std;

/*
 * CALCULATES THE TILES MEMORY POSITION USING THE CHARACTER X AND Y, 
 * AND THE INITIAL POSITION OF THE TILEMAP.
 * DECIMAL VERSION
 */
int main() {
    int playerx, playery, memoryStart;
    string memoryStr;
    
    cout << "Insert player x in decimal: " << endl;
    cin >> playerx;
    
    cout << "Insert player y in decimal: " << endl;
    cin >> playery;
    
    cout << "Insert the memory start position in hexadecimal: " << endl;
    cin >> memoryStr;
    memoryStart = std::stoul(memoryStr, nullptr, 16);
    
    int tilex = playerx / 4;
    int tiley = playery / 4;
    
    cout << "The position in the tilemap is x: " << tilex << " y: " << tiley << endl;
    
    int memoryPosition = memoryStart + tiley * 40 + tilex;
    
    char hex_string[4];
    sprintf(hex_string, "%X", memoryPosition); //convert number to hex
    cout << "The tile you are looking at is at 0x" << hex_string << endl;
}
